// import $, { isEmptyObject } from 'jquery';
$(window).scroll(function() {
    var scrollTopThreshold = 50;
    if ($(document).scrollTop() > scrollTopThreshold) {
      var fonts = '<link rel="stylesheet" href="'+window.location.origin+'/themes/growhappytheme/assets/styles/fonts.pack.css">';
      var defers = '<script defer src="'+window.location.origin+'/themes/growhappytheme/src/scripts/defer.pack.js"><\/script>';
      var fusepump = '<script defer src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js"><\/script>';
      var bolt = '<script defer src="https://cdn-akamai.mookie1.com/LB/LightningBolt.js"><\/script>';
      $('#deferred').append(fonts).append(defers).append(fusepump).append(bolt);
    }
})