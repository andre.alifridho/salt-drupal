import Swiper, { Navigation, Pagination, Autoplay } from 'swiper';

Swiper.use([Navigation, Pagination]);
// hide login banner on page scroll
$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    var offset = 5;

    if (scroll >= offset) {
        $(".container-login-register").addClass("is-hidden");
    } else {
        $(".container-login-register").removeClass("is-hidden");
    }
});

$('.lightbox-call-to-action').on('click', function (e) {
    e.preventDefault();
    var productId = $(this).data('product-id');
    var productName = $(this).data('data-product-name');
    var productRange = $(this).data('data-product-range');
    var productCategory = $(this).data('data-product-category');
    var productSubCategory = $(this).data('data-product-subcategory');
    var productVariant = $(this).data('data-product-variant');
    /*eslint-disable */
    var lightbox = new fusepump.lightbox.buynow(productId);
    /*eslint-enable */
    if (lightbox) {
        // push dataLayer
        dataLayer.push({
            'buyNowInformation': {
              'productBrand': 'Lactogrow',
              'productQuantity': '1',
              'buyNowClicked': 1,
              'productId': productId,
              'productName': productName,
              'productRange': productRange,
              'productCategory': productCategory,
              'productSubCategory': productSubCategory,
              'productVariant': productVariant
            }
          });

        lightbox.show(); // Show the lightbox
    }
    return false;
});

$('.btn-profile-menu-toggle').on('click', function () {
    $('.profile-menu-wrapper').toggleClass('is-hidden')
});

// DREAMBIG
var carouselHero = $('.lct__dream_big--hero .carousel').carousel();
var swiperTips = new Swiper('.lct__dream_big--stress_test-tips--swiper_container', {
    spaceBetween: -35,
    navigation: {
        nextEl: '.lct__dream_big--stress_test-tips--swiper_button_next',
        prevEl: '.lct__dream_big--stress_test-tips--swiper_button_prev',
    },
    pagination: {
        el: '.lct__dream_big--stress_test-tips--swiper_pagination',
        dynamicBullets: true
    },
    breakpoints: {
        768: {
            spaceBetween: -60,
        },
        992: {
            spaceBetween: -240,
        },
        1200: {
            spaceBetween: -260,
        }
    }
});

var swiperPodcast = new Swiper('.lct__dream_big--podcast-tips--swiper_container', {
    spaceBetween: -35,
    navigation: {
        nextEl: '.lct__dream_big--podcast-tips--swiper_button_next',
        prevEl: '.lct__dream_big--podcast-tips--swiper_button_prev',
    },
    pagination: {
        el: '.lct__dream_big--podcast-tips--swiper_pagination',
        dynamicBullets: true
    },
    breakpoints: {
        768: {
            // spaceBetween: -60,
            spaceBetween: 0,
            slidesPerView: 2
        },
        992: {
            // spaceBetween: -240,
            spaceBetween: 0,
            slidesPerView: 2
        },
        1200: {
            // spaceBetween: -260,
            spaceBetween: 0,
            slidesPerView: 2
        }
    }
});

var swiperPodcastSpotify = new Swiper('.lct__dream_big--podcast-spotify-swiper_container', {
    slidesPerView: 1,
    spaceBetween: 15,
    initialSlide: 1,
    centeredSlides: true,
    loop: false,
    loopAdditionalSlides: 100,
    pagination: {
        el: '.lct__dream_big--podcast-spotify-swiper_pagination',
        dynamicBullets: true
    },
    navigation: {
        nextEl: '.lct__dream_big--podcast-spotify-swiper_button_next',
        prevEl: '.lct__dream_big--podcast-spotify-swiper_button_prev',
    }
});

var swiperCerita = new Swiper('.lct__dream_big--cerita-swiper_container', {
    slidesPerView: 1,
    spaceBetween: -50,
    initialSlide: 1,
    centeredSlides: true,
    loop: true,
    loopAdditionalSlides: 100,
    pagination: {
        el: '.lct__dream_big--cerita-swiper_pagination',
        dynamicBullets: true
    },
    breakpoints: {
        769: {
            mousewheel: false,
            allowTouchMove: false,
            slidesPerView: 3,
            spaceBetween: -30
        }
    }
});

function initSwiperHappyNutri() {
    var active_swiper = 0;
    var prev_swiper = 0;
    var next_swiper = 0;
    $('.lct__dream_big--happy_nutri-swiper_slide').each(function (index) {
        if ($(this).hasClass('swiper-slide-next')) {
            next_swiper = index;
        }
        if ($(this).hasClass('swiper-slide-prev')) {
            prev_swiper = index;
        }
        if ($(this).hasClass('swiper-slide-active')) {
            active_swiper = index;
        }
    });

    $('.lct__dream_big--happy_nutri-swiper_slide').each(function (index) {
        if (index <= prev_swiper) {
            $(this).removeClass('positive');
            $(this).addClass('negative');
        } else if (index >= next_swiper) {
            $(this).removeClass('negative');
            $(this).addClass('positive');
        } else {
            $(this).removeClass('positive');
            $(this).removeClass('negative');
        }
    });
}

var swiperHappyNutri = new Swiper('.lct__dream_big--happy_nutri-swiper_container', {
    centeredSlides: true,
    touchRatio: 0,
    speed: 400,
    slidesPerView: 1,
    navigation: {
        nextEl: '.lct__dream_big--happy_nutri-swiper_button_next',
        prevEl: '.lct__dream_big--happy_nutri-swiper_button_prev',
    },
    breakpoints: {
        768: {
            spaceBetween: -320
        },
        1024: {
            spaceBetween: -440
        },
        1025: {
            spaceBetween: -500
        }
    },
    on: {
        init: initSwiperHappyNutri()
    }
});

swiperHappyNutri.on('slideChange', function () {
    initSwiperHappyNutri();
});
// END OF DREAMBIG

$(window).scroll(function () {
    var scrollTopThreshold = 500;
    if ($(document).scrollTop() > scrollTopThreshold) {
        $("#goToTopBtn").fadeIn();
    } else {
        $("#goToTopBtn").fadeOut();
    }
});

$("#goToTopBtn").on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});

$('.lct__dream_big--podcast-spotify-swiper_slide img').on('click', function(e) {
    const spotify_src = $(this).attr('spotify-src');
    $('#modalSpotify iframe').attr('src', spotify_src);
    $('#modalSpotify').modal('show');
});

$('#modalSpotify').on('hidden.bs.modal', function () {
    $('#modalSpotify iframe').attr('src', '');
});


$('.btn-play-cerita').on('click', function(e) {
    const youtub_src = $(this).attr('youtube-src');
    $('#modalYoutube iframe').attr('src', youtub_src);
    $('#modalYoutube').modal('show');
});

$('#modalYoutube').on('hidden.bs.modal', function () {
    $('#modalYoutube iframe').attr('src', '');
});