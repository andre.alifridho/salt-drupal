import Swiper from 'swiper/bundle';

$.fn.rangeslider = function(options) {
    this.each(function(i, elem){
        var obj = $(elem); // input element
        var defautValue = obj.attr("value");
        var slider_max  = (obj.attr("max"));
        var slider_min  = (obj.attr("min"));
        var slider_step = (obj.attr("step"));
        var slider_stop = slider_max / slider_step;
        var step_percentage = 100 / slider_stop;


        if(slider_stop <= 30){
            var i;
            var dots = "";
            for (i = 1; i <= slider_stop; i++){
                dots += "<div class='dot' id='"+ i +"' style='left:"+ ((step_percentage * i) - step_percentage) +"%;'></div>";
            }
        }
        else{
            var dots = "";
        }

        var sliderDots = "<span class='bar'>" + dots + "</span><span class='bar-btn'><svg><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#icon-smile'></use></svg></span>";

        obj.next().append(sliderDots);

        // obj.wrap("<span class='slider'></span>");
        // obj.after("<span class='slider-container'><span class='bar'>" + dots + "</span><span class='bar-btn'><svg><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#icon-smile'></use></svg></span></span>");

        

        // if($('span.slider').length == 0) {
        //     obj.wrap("<span class='slider'></span>");
        // }

        // if($('span.slider-container').length == 0) {
        //     obj.after("<span class='slider-container'><span class='bar'>" + dots + "</span><span class='bar-btn'><svg><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#icon-smile'></use></svg></span></span>");
        // }
        // obj.attr("oninput", "updateSlider(this)");
        obj.on('input', function(){
            updateSlider(this);
        })
        updateSlider(this);
        return obj;
    });
};


function updateSlider(passObj) {
    var obj = $(passObj);
    var value = obj.val();
    var min = obj.attr("min");
    var max = obj.attr("max");
    var range = Math.round(max - min);
    var percentage = Math.round((value - min) * 100 / range);
    var nextObj = obj.next();

    var btn = nextObj.find("span.bar-btn");

    if(value == min){
        nextObj.find("span.bar-btn").css("left", "12.5%");

    }
    else if(value == max){
        nextObj.find("span.bar-btn").css("left", "calc(" + percentage + "% - 12.5%");
    }
    else if(value == 2){
        nextObj.find("span.bar-btn").css("left", "calc(" + percentage + "% + " + btn.width()/2 + "px");
    }
    else if(value == 3){
        nextObj.find("span.bar-btn").css("left", "calc(" + percentage + "% - " + btn.width()/2 + "px");
    }

    nextObj.find(".bar").attr('data-value', value);
    nextObj.find("span.bar > span").css("width", percentage + "%");
    nextObj.find("span.bar-btn > span").text(value);
};

function changeQuestion(){
    var btnSlider = $('.btn-slider');
    var options = {
        slidesPerView: 1,
        loop: false,
        speed: 600,
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        simulateTouch: false,
        allowTouchMove: false
    };
    var stressTestSwiper = new Swiper('#quiz-swiper', options);
    var stressTestAnimationSwiper = new Swiper('#quiz-animation-swiper', options);

    stressTestSwiper.controller.control = stressTestAnimationSwiper;
    // stressTestAnimationSwiper.controller.control = stressTestSwiper;

    btnSlider.each(function(e, i){
        var $this = $(this);
        var formQuiz = $(stressTestSwiper.$el);
        var sliderLength = stressTestSwiper.slides.length;

        var $modal = $('#loginModal');

       

        $this.on('click', function(){
            console.log(e, sliderLength)
            stressTestSwiper.slideNext(600);

            if(e == (sliderLength - 1)){
              var answer = formQuiz.serializeArray(),
                nid_quiz,
                urlAjax = formQuiz.attr('action'),
                redirectTo = formQuiz.data('redirect-to'),
                nilai = new Array();

              console.log(answer)

                // nilai nya dimasukin array nanti tinggal di array_sum di be
                $.each(answer, function (key, val) {
                  if (val.name === 'answer') {
                    nilai.push(val.value)
                  } else {
                    nid_quiz = val.value
                  }
                });

                $.ajax({
                  type: "POST",
                  url: urlAjax,
                  data: {
                    nid_quiz: nid_quiz,
                    nilai: nilai
                  },
                  complete: function (res) {
                    var response = res.responseJSON,
                      data = response.data,
                      success = response.success;

                    // Modalnya muncul kalau is_login nya false
                    if (success) {
                      if (data.is_login === false) {
                        var values = data.values
                        console.log(values.nilai)
                        window.location.href = base_url + "/stress-test/quiz/result";
                      } else {
                        window.location.replace(redirectTo);
                      }
                    }
                  }
                });

                // console.log($modal);
            }
        })
    })
}

function modalInit(){
    $('.slide-form').hide();
    var modalSwiper;

    $('#loginModal')
    .on('shown.bs.modal', function(){
        var loginCTA = $('#login-cta'),
            registerCTA = $('#register-cta');

        $('.slide-form').fadeIn(200);

        modalSwiper = new Swiper('#modal-swiper', {
            slidesPerView: 1,
            autoHeight: true,
            loop: false,
            speed: 300,
            simulateTouch: false,
            allowTouchMove: false
        });

        loginCTA.on('click', function(){
            modalSwiper.slidePrev(300);
        })

        registerCTA.on('click', function(){
            modalSwiper.slideNext(300);
        })
    })
    .on('hidden.bs.modal', function(){
        $('.slide-form').hide();
        modalSwiper.destroy(true, true)
    })
}



$(function() {
    changeQuestion();
    $(".slider-input").rangeslider();
    modalInit()
    
    $("#login").click(function() {
        var error = '';
        $("#error").html(error);
        console.log("console");
        var email = $('#stresstest-login-email').val();
        var password = $('#stresstest-login-password').val();
        // console.log(email, password);
        $.post(
            base_url + "stress-test/APIlogin",
            {
                email: email,
                password: password
            },
            function(data) {
            // console.log(data.success)
            window.location.href = base_url + "/stress-test/quiz/result";
            // var html = '<strong>*</strong> Login gagal, periksa email dan password anda<br>';
            // $("#error").html(html);
            }
        ).fail(function(data) {
            // console.log("error")
            $("#error").html("");
            var html = '<div id="errormessage" class="alert alert-danger" style="margin:0"><strong>*</strong> Login gagal, periksa email dan password anda<br></div>';
            $("#error").html(html);
            // alert('woops'); // or whatever
        });
          
    });
});
