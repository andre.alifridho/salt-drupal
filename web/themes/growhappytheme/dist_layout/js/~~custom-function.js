$(document).ready(function(){

    window.fbAsyncInit = function () {
        FB.init({
            appId: '212420986182161',
            xfbml: true,
            version: 'v2.12'
        });

        FB.AppEvents.logPageView();

    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    var baseUrl = window.location.protocol + "//" + window.location.host;
    var pathArray = window.location.pathname.split( '/' );
    var getLastSecondUrl = pathArray[pathArray.length-2];
    var getLastUrl = pathArray[pathArray.length-1];
    var getSubUrl = pathArray[1];

    // set the same url from header menu to sidebar if equal with title
    $('ul.dropdown-menu > li').each(function(){
        var thisParent = $(this).find('a').text();
        var thisUrl = $(this).find('a').attr('href');
        $('#inner-side-left ul > li').each(function(){
            if(thisParent == $(this).find('a').text()){
                $(this).find('a').attr('href', thisUrl);
            }
        });

    });

    new Clipboard('.btnclip');
    setGreeting();
    getHashValue(baseUrl);

    if(getLastSecondUrl == 'guest'){
        if(getSubUrl == 'dpr'){
            baseUrl = baseUrl + '/dpr';
        }

        $('#inner-side img').each(function(){
            var getImage = $(this).attr('src');
            if($(this).closest('.profile-box').length <= 0){
                $(this).attr('src', '../' + getImage);
            }
        });

        $('#inner-side-left ul > li').each(function(){
            $(this).find('a').attr('href', baseUrl + '/guest/' + $(this).find('a').attr('href').replace('.html', ''));
        });

        $('#inner-side-left ul > li:last-child a').attr('href', baseUrl + '/daftar-rewards');

        if($('#logged-area').attr('data-hasdata') == 'ada'){
            $('#left-nav > ul').append('<li><a href="' + baseUrl + '/guest/bonus-pulsa">Bonus Rewards</a></li>');
        }
        $('#left-nav > ul').append('<li><a href="' + baseUrl + '/referensikan-teman">Referensikan Teman</a></li>\
                                        <li><a href="' + baseUrl + '/guestlogout">Logout</a></li>');

        // change reset url
        $('#left-nav > ul > li:nth-child(2) > a').attr('href', $('#submenu-profile > ul > li:nth-child(2) > a').attr('href'));

        if(getUrlParameter('error') == '1'){
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, kode unik salah atau sudah digunakan</h4>');
            $('#popupbox').modal();
            // alert('Maaf, kode unik salah atau sudah digunakan');
        }else if(getUrlParameter('error') == '0'){
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Selamat, kode unik berhasil disubmit</h4>');
            $('#popupbox').modal();
        }else if(getUrlParameter('error') == '3'){
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Maksimal submit code 10 kali dalam 1 bulan</h4>');
            $('#popupbox').modal();
        }else if(getUrlParameter('error') == '4'){
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Maksimal submit code 5 kali dalam 1 hari</h4>');
            $('#popupbox').modal();
        }else if(getUrlParameter('error') == '5'){
            //$('#popupbox .modal-body').html('<h4 style="text-align: center">Selamat Anda mendapatkan bonus rewards pulsa 25rb</h4><br><div style="text-align: center;"><span style="display: inline-block;"><a href="' + baseUrl + '/guest/klaim-pulsa" class="btn btn-warning">Klaim Sekarang</a> <a class="btn btn-warning" href="' + baseUrl + '">Klaim Nanti</a></span></div>');
            $('#popupbox .modal-header').html('<img src="/themes/artheme/dist_front/img/logo-dpc2.png" width="auto" height="95" style="margin-top:-23px;margin-bottom: -16px;" alt="">');
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Selamat Anda mendapatkan bonus rewards pulsa 10.000</h4>');
            $('#popupbox').modal();
        }else if(getUrlParameter('redeemError') == '2'){
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, point anda tidak mencukupi untuk redeem item</h4>');
            $('#popupbox').modal();
        }
        else if(getUrlParameter('redeemError') == '10'){
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, terjadi kesalahan silahkan coba kembali dalam 24 jam</h4>');
            $('#popupbox').modal();
        }
    }

    //popup Error when redeem
    if(getUrlParameter('redeemError') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, tidak ada item untuk di redeem</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('redeemError') == '0'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Selamat, redeem berhasil</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('mailstatus') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Selamat, email telah dikirim</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '11'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, email ini sudah terdaftar sebelumnya</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '12'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Kode aktivasi anda salah</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '13'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, nomor telepon ini sudah terdaftar sebelumnya</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '14'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, minimal point untuk klaim adalah 3150</h4>');
        $('#popupbox').modal();
    }

    else if(getUrlParameter('error') == '20'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Terjadi kesalahan, silahkan cek data yang anda input</h4>');
        $('#popupbox').modal();
    }
    
    if(getUrlParameter('banned') == '1'){
        $('#popupbox .modal-header').html('<img src="/themes/artheme/dist_front/img/logo-dpc2.png" width="auto" height="95" style="margin-top:-23px;margin-bottom: -16px;" alt="">');
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Member ID Anda dalam status tidak aktif. Hubungi 08001821028 (tekan 2) atau email ke dancow.parentingrewards@id.nestle.com</h4>');
        $('#popupbox').modal();
    }


    if(getUrlParameter('success') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Profile Berhasil di Update</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('login') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Link ganti password sudah dikirim ke email anda.</h4>');
        $('#popupbox').modal();
    }

    if(getUrlParameter('login') == '2'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Email atau Password anda salah</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('successRedeem')){
        $('#popupbox .modal-header').html('<img src="/themes/artheme/dist_front/img/logo-dpc2.png" width="auto" height="95" style="margin-top:-23px;margin-bottom: -16px;" alt="">');
        $('#popupbox .modal-body').html('<h4 style="text-align: center;font-weight: 300;">Selamat, Anda telah berhasil menukar DANCOW IYA BOLEH Poin.</h4><br><h4 style="text-align: center;font-weight: 300;">Ini adalah kode undian Anda</h4><br><h2 style="text-align: center;font-weight:600;margin-top: 0px;"><img src="/themes/artheme/dist_front/img/IyaBolehPoinicon.png" style="width:60px;"> '+getUrlParameter('successRedeem')+'</h2><br><h4 style="text-align: center;font-weight: 300;">Tambah terus DANCOW IYA BOLEH Poin Anda untuk memperbesar kesempatan memenangkan hadiah undian.</h4><div style="text-align: center;"><span style="display: inline-block;margin-bottom:10px;"><br><a class="btn btn-warning" href="'+baseUrl+'/guest/iyaboleh-voucher">Lihat Iya Boleh Poin</a></span></div>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('suksesponta')){
        $('#popupbox .modal-header').html('<img src="/dpr/themes/artheme/dist_front/img/logo-dpc2.png" width="auto" height="95" style="margin-top:-23px;margin-bottom: -16px;" alt="">');
        //$('#popupbox .modal-body').html('<h4>Tukarkan kode '+getUrlParameter('suksesponta')+' dg 1 DANCOW Advanced Excelnutri+ 1+ 200gr di ALFAMART. Valid sd 28 Feb 19. Syarat ketentuan berlaku</h4>');
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Terima kasih.<br>Silakan cek SMS di nomor HP terdaftar. Syarat ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('gagal') == '0'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Nomor HP yang Anda masukkan tidak sesuai dengan data PONTA. Cek kembali nomor HP Anda. Syarat ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('gagal') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Promo sudah berakhir. <br>Nantikan promo DANCOW Advanced Excelnutri+ berikutnya. Info lengkap cek <a href="https://www.parentingrewards.dancow.co.id" target="_blank">www.parentingrewards.dancow.co.id</a><br>Syarat Ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('gagal') == '2'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Data Anda sudah digunakan.<br>Nomor HP dan data member PONTA hanya bisa dimasukkan 1 (satu) kali. Syarat ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }

    var currentUrl = window.location.href;
    // alert(currentUrl);
    $('a').closest('li').not().closest('.nav-pills').removeClass('active');
    $('a').each(function() {
        if($(this).attr('href') == currentUrl){
            if($(this).closest('.dropdown').length > 0){
                $(this).closest('.dropdown').addClass('active');
            }else{
                $(this).closest('li').addClass('active');
            }
        }
    });


    $('#imgupload').change(function(){

        var uploadurl = $(this).closest('form').attr('action');
        var data = new FormData();
        data.append('image', $(this)[0].files[0]);

        $.ajax({
            url: uploadurl,
            data: data,
            cache: false,
            type: 'POST',
            processData: false,
            contentType: false,
            success: function(data){
                $('.profile-box > img').attr('src', data);
            }
        });
    });

    if($('input[name="address"]').val() == '0'){
        $('.add-address-form input[type="text"], select, .add-address-form textarea').attr('required', 'required');
    }
    $('input[name="address"]').on('click', function(){
        if($(this).val() == '0'){
            $('.add-address-form input[type="text"], select, .add-address-form textarea').attr('required', 'required');
        }
    });


    $('form#addkeranjangbelanja').submit(function(e){
        var getInput = '';
        if($(this).find('input').not('input[name="slug"]').length > 0){
            $(this).find('input').not('input[name="slug"]').each(function(){
                if($(this).val() > 0 && $(this).val().length > 0){
                    $('form#addkeranjangbelanja button[type="submit"]').attr('disabled', 'disabled');
                    getInput += 'ada';
                    // alert($(this).val());
                }
            });

            if(getInput.length <= 0){
                $('#popupbox .modal-body').html('<h4 style="text-align: center">Anda harus mengisi jumlah atau nomer</h4>');
                $('#popupbox').modal();
                e.preventDefault();
            }else if($('form#addkeranjangbelanja > #detail-rewards-middle-right').attr('data-ars') == 'na'){
                $('#popupbox .modal-body').html('<h4 style="text-align: center">Anda sudah melebihi batas maksimal penukaran poin. Silakan pilih rewards lainnya</h4>');
                $('#popupbox').modal();
                e.preventDefault();
            }
        }else{
            $('#popupbox .modal-body').html('<h4 style="text-align: center">Rewards Tidak Tersedia</h4>');
            $('#popupbox').modal();
            e.preventDefault();

        }


    });

    $('.share-twitter').on('click', function(){
        popupCenter('https://twitter.com/share?url=' + $(this).data('shareurl'), 'Share Twitter', '500', '400');
    });

    $('.share-fb').click(function (e) {
        var refferal = $(this).attr('data-refferal');
        var name = $(this).attr('data-name');
        var description = $(this).attr('data-description');
        var image = $(this).attr('data-image');

        e.preventDefault();
        FB.ui({
            method: 'share_open_graph',
            action_type: 'og.shares',
            action_properties: JSON.stringify({
                object: {
                    'og:url': refferal,
                    'og:title': name,
                    'og:description': description,
                    'og:image': image
                }
            })
        });

        return false;
    });

    $('.tempat-beli').on('change', function(){
        if($(this).val() == 'Alfamart'){
            $('.member-ponta').show();
        }else{
            $('.member-ponta').hide();
        }
    });

    function popupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }

    function checkPhysc(){
        var fisik = ['cosmolady','tiwitodz','tiwikidz','fryingpan', 'backpack', 'map', 'tupperware','sodexo','shell','elc-trike', 'elctrike','blitz', 'dpc','bigbangdancow', 'bigbanglactogrow', 'kidzania', 'seaworld','elcfisik'];
        var havePhsyc = [];
        var addClass = '';
        $('.table.table-striped tbody > tr').each(function(){
            if(jQuery.inArray($(this).data('slug'), fisik) >= 0){
                havePhsyc.push('has');
            }
        });

        if(getLastUrl == 'keranjang-step-3'){
            addClass = 'cart-active';
        }

        //cek kalo ada fisik
        if(jQuery.inArray('has', havePhsyc) < 0){
            $('li.step-2').addClass(addClass);
            $('li.step-2 div.cart-icon > i').attr('class', 'fa fa-check-square-o');
            $('li.step-2 span').text('Konfirmasi');

            $('#address-box > .blockfisik').remove();
            $('li.step-3').remove();

            $('a.changebtn').attr('href', baseUrl + '/guest/keranjang-step-3');
        }else{
            $('li.step-2 div.cart-icon > i').attr('class', 'fa fa-address-book');
            $('li.step-2 span').text('Alamat Pengiriman');
            $('#cart-steps ul').append('<li class="step-3 ' + addClass + '">\
                                            <div class="number-step">3</div>\
                                            <div class="cart-icon">\
                                                <i class="fa fa-check-square-o" aria-hidden="true"></i>\
                                            </div>\
                                            <span>Konfirmasi</span></li>');

            $('a.changebtn').attr('href', baseUrl + '/guest/keranjang-step-2');
        }
    }

    if($('.step-1.cart-active').length > 0 || $('.step-3.cart-active').length > 0){
        checkPhysc();
    }


    if($('table tbody.history-mutasi').length > 0){
        var mutasi = [];
        var getPointBalance = $('#greet span.sisapointbalance').text(); //13500;
        var historypoint = parseInt(getPointBalance);

        $('table tbody.history-mutasi > tr').each(function(){
            mutasi.push(parseInt($(this).find('td:nth-child(4)').text()));
        });

        console.log(mutasi);
        for(var i=mutasi.length;i>=0;i--){
            indexArray = i - 1;
            indexMutasi = i;
            indexElement = i - 1;

            var getMutasiType = $('table tbody.history-mutasi > tr:nth-child(' + indexMutasi + ')').find('td:nth-child(2)').text();
            if(mutasi[indexArray]){
                if(getMutasiType == 'DB'){
                    historypoint = historypoint+mutasi[indexArray];
                }else if(getMutasiType == 'CR'){
                    historypoint = historypoint-mutasi[indexArray];
                }
            }
            $('table tbody.history-mutasi > tr:nth-child(' + indexElement + ')').find('td:nth-child(5)').text(historypoint);
        }

        $('table tbody.history-mutasi > tr:last-child').find('td:nth-child(5)').text(getPointBalance);
        $('.saldoawal').text(historypoint);
    }

    if($('.btn-warning.changebtn').length > 0){
        console.log($('table.table-striped tbody > tr').length);
        if($('table.table-striped tbody > tr').length <= 1){
            $('table.table-striped tbody > tr a.changebtn').remove();
        }
    }

    $('#adddataanak input.datepicker').on('keyup change', function(e){
        if(checkKidAge($(this).val())){
            e.target.setCustomValidity("Umur Anak Minimal 1 Tahun");
        }else{
            e.target.setCustomValidity("");
        }
    });

    $('#dob').on('keyup change', function(e){
        if(checkParentAge($(this).val())){
            e.target.setCustomValidity("Umur Minimal 13 Tahun");
        }else{
            e.target.setCustomValidity("");
        }
    });

    $('#addanakbtn').on('click', function(){
        var getAnak = parseInt($('#countanak').val());
        var getAnak = getAnak + 1;
        $('#countanak').val(getAnak);
        $('#adddataanak').append('<tr>\
                                      <td width="50%">\
                                        <div class="row-register">\
                                          <span>Nama Anak<i>*</i></span>\
                                          <input name="anak[' + getAnak + '][name]" type="text" class="form-control" required>\
                                        </div>\
                                        <div class="row-register">\
                                          <span>Jenis Kelamin<i>*</i></span>\
                                          <select name="anak[' + getAnak + '][gender]" class="form-control" required>\
                                            <option value="">Pilih</option>\
                                            <option value="M">Laki-laki</option>\
                                            <option value="F">Perempuan</option>\
                                          </select>\
                                        </div>\
                                      </td>\
                                      <td width="50%">\
                                        <div class="row-register">\
                                          <span>Tanggal Lahir<i>*</i></span>\
                                          <input name="anak[' + getAnak + '][dob]" type="text" class="datepicker form-control" required>\
                                        </div>\
                                        <div class="row-register">\
                                          <span>Produk yang dikonsumsi<i>*</i></span>\
                                          <select name="anak[' + getAnak + '][product]" class="form-control" required>\
                                            <option value="">Pilih</option>\
                                            <option>DANCOW 1+</option>\
                                            <option>DANCOW 3+</option>\
                                            <option>DANCOW 5+</option>\
                                            <option>Campur</option>\
                                            <option>Others</option>\
                                          </select>\
                                        </div>\
                                        <button type="button" class="pull-right btn btn-danger btndeleteanak">Delete</button>\
                                      </td>\
                                    </tr>');

        $('.btndeleteanak').on('click', function(){
            $(this).closest('tr').remove();
        });
        $('input.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#adddataanak input.datepicker').on('keyup change', function(e){
            if(checkKidAge($(this).val())){
                e.target.setCustomValidity("Umur Anak Minimal 1 Tahun");
            }else{
                e.target.setCustomValidity("");
            }
        });
    });

    function checkKidAge(value){
        var current = new Date().getTime(),
            dateSelect = new Date(value).getTime();
            age = current - dateSelect;
            ageGet = Math.floor(age / 1000 / 60 / 60 / 24 / 365.25); // age / ms / sec / min / hour / days in a year
            if(ageGet < 1){
                return true;
            }else{
                return false;
            }
    }

    function checkParentAge(value){
        var current = new Date().getTime(),
            dateSelect = new Date(value).getTime();
            age = current - dateSelect;
            ageGet = Math.floor(age / 1000 / 60 / 60 / 24 / 365.25); // age / ms / sec / min / hour / days in a year
            if(ageGet <= 13){
                return true;
            }else{
                return false;
            }
    }

    $('.delete-keranjang').click(function(){
        var deleteurl = $(this).closest('table').attr('data-action');
        var thisVal = $(this).data('index');
        var thisPoint = parseInt($(this).data('point'));
        var thisElem = $(this);

        $.ajax({
            url: deleteurl,
            data: "id=" + thisVal,
            cache: false,
            type: 'GET',
            success: function(data){
                if(data == 'Success'){
                    thisElem.closest('tr').remove();
                    var getTotal = parseInt($('#totalpoint').text());
                    var calculate = getTotal - thisPoint;
                    $('#totalpoint').text(calculate);

                    checkPhysc();

                    if($('table.table-striped tbody > tr').length <= 1){
                        $('table.table-striped tbody > tr a.changebtn').remove();
                    }
                }
            }
        });
    })

    $('input.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });


    $('#changeviewprofile').click(function(){
        $('.view-profile').hide();
        $('.edit-profile').show();
    })

    $('#provinsiID, #kabupatenID, #kecamatanID').change(function(){
        var thisID = $(this).attr('id');
        var getBaseUrl = $(this).data('actionurl');
        var getThisVal = $(this).find(':selected').data('getid'); //$(this).val();
        if(getThisVal != "0"){
            $.post( getBaseUrl, { id : getThisVal })
                .done(function( data ) {
                    // console.log(getThisVal+' - '+data);
                    var html = '<option value="">Pilih</option>';
                    if(thisID == "kecamatanID"){
                        $.each(JSON.parse(data), function(key, item){
                            html += '<option data-kodepos="' + item.kodepos + '" data-getid="' + item.id + '" value="' + item.name + '">' + item.name + '</option>';
                        });
                    }else{
                        $.each(JSON.parse(data), function(key, item){
                            html += '<option data-getid="' + item.id + '" value="' + item.name + '">' + item.name + '</option>';
                        });
                    }
                    // console.log(html);
                    if(thisID == "provinsiID"){
                        $('#kabupatenID').html(html);
                        $('#kecamatanID, #kelurahanID').html('<option value="">Pilih</option>');
                        $('#kodepos').val('');

                    }else if(thisID == "kabupatenID"){
                        $('#kecamatanID').html(html);
                        $('#kelurahanID').html('<option value="">Pilih</option>');
                        $('#kodepos').val('');

                    }else if(thisID == "kecamatanID"){
                        $('#kelurahanID').html(html);
                        $('#kodepos').val('');

                    }

                    // $('#kelurahanID').change(data, function(){
                    //     var data = JSON.parse(data);
                    //     var getIndex = $(this).prop('selectedIndex');
                    //     console.log(data);
                    //     if(data[getIndex-2].hasOwnProperty("kodepos")){
                    //         $('#kodepos').val(data[getIndex-2].kodepos);
                    //     }
                    // });

                    $('#kelurahanID').change(function(){
                        $('#kodepos').val($(this).find(':selected').data('kodepos'));
                    });
            });

        }
    });

    $('input[name="address"]').on('click', function(){
        if($(this).val() != '0'){
            $('#btnnextorsimpan').text('SELANJUTNYA');
        }else{
            $('#btnnextorsimpan').text('SIMPAN');
        }
    });


    $('.sisapointbalance').text(addCommas($('.sisapointbalance').text()));
    $('.formatcurrency').each(function(){
        $(this).text(addCommas($(this).text()));
    });

    //modal for popup asi
    // $(window).on('load',function(){
        console.log(getCookie("nopopup"));
        if (getCookie("nopopup") != 1) {
            $('#confirmationModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    // });

    $("#btn-popup-agree").click(function(){
      $("#confirmationModal").modal("hide");
      setCookie("nopopup", 1, 1);
    })

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function setGreeting(){
        var getDate = new Date();
        var thehours = getDate.getHours(); // + ':' + getDate.getMinutes();
        var themessage;
        var morning = 'Selamat Pagi';
        var afternoon = 'Selamat Siang';
        var evening = 'Selamat Sore';
        var night = 'Selamat Malam';

        // if (thehours >= '00:00' && thehours <= '10:00') {
        //     themessage = morning;

        // } else if (thehours >= '10:01' && thehours <= '15:00') {
        //     themessage = afternoon;

        // } else if (thehours >= '15:01' && thehours <= '18:00') {
        //     themessage = evening;

        // } else if (thehours >= '18:01' && thehours <= '23:59') {
        //     themessage = night;
        // }

        if (thehours >= 0 && thehours <= 10) {
            themessage = morning;

        } else if (thehours >= 10 && thehours <= 15) {
            themessage = afternoon;

        } else if (thehours >= 15 && thehours <= 18) {
            themessage = evening;

        } else if (thehours >= 18 && thehours <= 23) {
            themessage = night;
        }

        $('#greet > strong > span').html(themessage);
    }

    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function getHashValue($url){
        var getHash = window.location.hash.substring(1);

        if(getHash){
            $('.tab-pane, .nav-tab > li').removeClass('active');
            $('#' + getHash + ', #tab-' + getHash).addClass('active');
        }
    }

    // // Create the lightbox
    // lightbox = new fusepump.lightbox.buynow(00214324);

    // var callToActionElement = document.getElementById(
    //     "lightbox-call-to-action" // The id of your call-to-action element
    // );

    // // Bind to the click event on the call-to-action element
    // callToActionElement.addEventListener("click", function() {
    //     lightbox.show(); // Show the lightbox
    // });

})

