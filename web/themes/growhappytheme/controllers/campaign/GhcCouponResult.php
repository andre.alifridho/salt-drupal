<?php

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

function growhappytheme_preprocess_page__campaign__ghc_coupon_result(&$variables)
{
  try {
    session_start();
	  $session = new Session();
	  if ($session->get('guestlogin')) {
	    $member = (object)$session->get('guestlogin');
	    $variables['guestlogin'] = $member;
		$authid = $member->id;
	  }

  $query = \Drupal::database()->select('snrv2_campaign_national_coupons', 'coupon');
			$query->fields('coupon', ['created_at', 'coupon_code']);
			$query->condition('coupon.member_id', $authid);
			$coupons = $query->execute()->fetchAll();

			foreach ($coupons as $c) {
			  $couponsArr[] = [
				  'created_at' => $c->created_at,
				  'coupon_code' => $c->coupon_code,
			  ];
			}

  $variables['coupon'] = $couponsArr;

  } catch (Exception $e) {
      $path = 'not-found';
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
  }
}

function growhappytheme_preprocess_page__campaign__nasionalpromo_topspender(&$variables)
{
  try {

  $query = \Drupal::database()->select('snrv2_natpromo_top', 'top');
			$query->fields('top', ['name', 'phone', 'timestamp']);
			$data = $query->execute()->fetchAll();

			foreach ($data as $c) {
			  $dataArr[] = [
				  'name' => $c->name,
				  'phone' => $c->phone,
          'timestamp' => $c->timestamp,
			  ];
			}

  $variables['data'] = $dataArr;

  } catch (Exception $e) {
      $path = 'not-found';
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
  }
}

function growhappytheme_preprocess_page__campaign__nasionalpromo_biweekly(&$variables)
{
  try {

  $query = \Drupal::database()->select('snrv2_natpromo_bw', 'bw');
			$query->fields('bw', ['name', 'phone', 'timestamp']);
			$data = $query->execute()->fetchAll();

			foreach ($data as $c) {
			  $dataArr[] = [
				  'name' => $c->name,
				  'phone' => $c->phone,
          'timestamp' => $c->timestamp,
			  ];
			}

  $variables['data'] = $dataArr;

  } catch (Exception $e) {
      $path = 'not-found';
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
  }
}

function growhappytheme_preprocess_page__campaign__nasionalpromo_monthly(&$variables)
{
  try {

  $query = \Drupal::database()->select('snrv2_natpromo_montly', 'montly');
			$query->fields('montly', ['name', 'phone', 'timestamp']);
			$data = $query->execute()->fetchAll();

			foreach ($data as $c) {
			  $dataArr[] = [
				  'name' => $c->name,
				  'phone' => $c->phone,
          'timestamp' => $c->timestamp,
			  ];
			}

  $variables['data'] = $dataArr;

  } catch (Exception $e) {
      $path = 'not-found';
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
  }
}
