export function init() {
    (function ($, Drupal) {

        /**
         * Recaptcha bug fix with ajax rendring form.
         */
        Drupal.behaviors.recaptcha_ajax_behaviour = {
            attach: function(context, settings) {
                if (typeof grecaptcha != "undefined") {
                    var captchas = document.getElementsByClassName('g-recaptcha');
                    var main_block = document.getElementById('block-growhappytheme-content');
                    for (var i = 0; i < captchas.length; i++) {
                        var site_key = captchas[i].getAttribute('data-sitekey');
                        if (!$(captchas[i]).html()) {
                            main_block.classList.add('captcha-rerendered');
                            grecaptcha.render(captchas[i], { 'sitekey' : site_key});
                        }
                    }
                }
            }
        }

        Drupal.behaviors.get_provinsi_kota = {
            attach(context, settings) {
                var country_select = $('select[name="country"]', context),
                    province_select = $('select[name="province"]', context),
                    city_select = $('select[name="city"]', context),
                    province_change_state = false,
                    default_province_select = province_select.html(),
                    default_city_select = province_select.html();

                $('select[name="province"]').val('Negara');

                console.log("negara");
                console.log(country_select);
                console.log(country_select.html());

                function get_provinsi(){
                    $.ajax({
                        url: window.baseUrl + 'api/area/get-provinsi',
                        method:'GET',
                        success: function(data){
                            if(data.success){
                                var data = data.data;

                                province_select.empty();
                                province_select.select2();

                                // for(var i = 0; i < data.length; i++){
                                //     province_select.append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
                                // }

                                var html = '<option value>Pilih Provinsi</option>';
        
                                for (var i = 0; i < data.length; i++) {
                                    html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                                }
        
                                province_select.html(html);
                            }
                        },
                        // complete: function(){
                        //     get_kabupaten()
                        // }
                    });
                }

                function get_kabupaten(){
                    var provinsi_selected = province_select.val();

                    $.ajax({
                        url: window.baseUrl + 'api/area/get-kabupaten',
                        data: {
                            provinsi_id: provinsi_selected
                        },
                        method:'GET',
                        success: function(data){
                            if(data.success){
                                var data = data.data;
                
                                city_select.empty();
                                city_select.select2();

                                // for(var i = 0; i < data.length; i++){
                                //     city_select.append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
                                // }

                                var html = '<option value>Pilih Kota / Kabupaten</option>';
        
                                for (var i = 0; i < data.length; i++) {
                                    html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                                }
        
                                city_select.html(html);
                            }
                        }
                    });
                
                }

                country_select.on('change', function(){
                    var $country_value = $(this).find('option:selected').val();

                    if($country_value === 'Indonesia'){
                        get_provinsi();
                        province_change_state = true;
                        
                    } else {
                        province_select.empty();
                        province_select.append(default_province_select);
                        province_change_state = false;
                    }
                })

                province_select.on('change', function(){
                    var $value = $(this).find('option:selected').val();

                    if(province_change_state){
                        get_kabupaten()
                    } else {
                        city_select.empty();
                        city_select.append(default_city_select);
                    }
                })
                
            }
        }

        Drupal.behaviors.reuse_get_provinsi_kota = {
            attach(context){
                $(context).ajaxComplete(function( event, request, settings ) {
                    if(settings.url === "/contact-us?ajax_form=1&_wrapper_format=drupal_ajax"){
                        Drupal.behaviors.get_provinsi_kota.attach(context);
                    }
                });
            }
        }
    })(jQuery, Drupal);
}

init();
