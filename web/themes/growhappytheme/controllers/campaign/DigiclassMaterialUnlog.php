<?php

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

function growhappytheme_preprocess_page__guest__campaign__digiclass_material_unlog(&$variables)
{
  try {

    session_start();
    $session = new Session();

  if ($session->get('guestlogin')) {
    $member = (object)$session->get('guestlogin');
    $variables['guestlogin'] = $member;

  $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,
    ]);

  $response_provinsi = $client->get("api/web/area/provinsi?key=".__API_KEY__);
  $get_response_provinsi = $response_provinsi->getBody();
  $provinsi = json_decode($get_response_provinsi, true);

  $query = \Drupal::database()->select('snrv2_little_digiclass_kelas', 'kelas');
	$query->fields('kelas', ['id', 'kelas']);
	$query->condition('kelas.status', 1);
	$class = $query->execute()->fetchAll();

  if (isset($_GET['msg'])) {
    $msg = $_GET['msg'];
  } else {
    $msg = '';
  }

  if (isset($_GET['status'])) {
    $status = $_GET['status'];
  } else {
    $status = '';
  }

  if (isset($_GET['success'])) {
    $success = $_GET['success'];
  } else {
    $success = '';
  }

  $variables['provinsi'] = $provinsi;
  $variables['msg'] = $msg;
  $variables['status'] = $status;
  $variables['success'] = $success;
  $variables['class'] = $class;

} else {
  session_start();
  $_SESSION['set'] = 'digiclass';
  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
  $response = new RedirectResponse($redirectto);
  $response->send();
  return;
}

  } catch (Exception $e) {
      $path = 'not-found';
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
  }
}
