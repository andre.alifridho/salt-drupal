<?php

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

function growhappytheme_preprocess(array &$variables, $articleid)
{

  date_default_timezone_set("Asia/Jakarta");

  ### BLOCK FRAME
  header("X-XSS-Protection: 1; mode=block");
  header("X-Frame-Options: DENY");
  header("X-Content-Type-Options: nosniff");

  ### SSO LOGIN AND REGISTER
  error_reporting(0);
  $query_params = array(
    'response_type' => 'code',
    'action'        => 'login',
    'client_id'     => __SSO_CLIENT_ID__,
    //'redirect_uri'  => __SSO_REDIRECT__,
    'redirect_uri'  => 'http://www.growhappy.co.id/sso/callback',
    'scope'         => 'user'
  );

  $login_url = __SSO_AUTH_URL__ . '?' . http_build_query($query_params);
  // $login_url = 'https://growhappy.co.id/apps/snrv2/apps/snrv2/api/web/sso/login';

  $query_params = array(
    'response_type' => 'code',
    'action'        => 'register',
    'client_id'     => __SSO_CLIENT_ID__,
    //'redirect_uri'  => __SSO_REDIRECT__,
    'redirect_uri'  => 'http://www.growhappy.co.id/sso/callback',
    'scope'         => 'user'
  );

  $register_url = __SSO_AUTH_URL__ . '?' . http_build_query($query_params);
  // $register_url = 'https://growhappy.co.id/apps/snrv2/apps/snrv2/api/web/sso/register';

  $query_params = array(
    'response_type' => 'code',
    'action'        => 'profile',
    'client_id'     => __SSO_CLIENT_ID__,
    //'redirect_uri'  => __SSO_REDIRECT__,
    'redirect_uri'  => 'http://www.growhappy.co.id/sso/callback',
    'scope'         => 'user'
  );

  $profile_url = __SSO_AUTH_URL__ . '?' . http_build_query($query_params);
  // $profile_url = 'https://connect.sahabatnestle.co.id/api/sso/user/users/';

  global $base_url;

  $current_path = \Drupal::service('path.current')->getPath();
  setcookie("after_login_url", $current_path, time() + 7200);
  $root_split = explode("/", $current_path);
  $root_path = $root_split[1];
  $node = \Drupal::routeMatch()->getParameter('node');
  $node_type = $node->type->entity ? $node->type->entity->label() : '';

  if ($root_path != "management") {
    if ($root_path != 'home' && $node_type == '' && $root_path != 'product' && $root_path != '' && $node_type != 'Article' && $root_path != 'happyupdates' && $root_path != 'dream-big' && $root_path != 'happyarticle' && $root_path != 'stress-test' && $root_path != 'club' && $root_path != 'club-redeem' && $root_path != 'reward-detail' && $root_path != 'login' && $root_path != 'lupa-password' && $root_path != 'join' && $root_path != 'nav-new' && $root_path != 'faq' && $root_path != 'profile' && $root_path != 'profile-edit' && $root_path != 'sample' && $root_path != '404') {
      $variables['#attached']['library'][] = 'growhappytheme/home-styling';
    }
  } else {
    $variables['#attached']['library'][] = 'growhappytheme/global-styling';
  }

  // $url_login = 'https://beta.connect.sahabatnestle.co.id/new/sso/auth?response_type=code&action=login&client_id=13_1p9g6lujri684k04k040cgwggs0c4k0sswc4skkwkkgso8s8gw&redirect_uri=http://159.65.14.249:8072/dpr_project/redirect&scope=user';
  // $url_register = 'https://beta.connect.sahabatnestle.co.id/new/sso/auth?response_type=code&action=login&client_id=13_1p9g6lujri684k04k040cgwggs0c4k0sswc4skkwkkgso8s8gw&redirect_uri=http://159.65.14.249:8072/dpr_project/redirect&scope=user';
  //
  $variables['adminSess'] = $_SESSION;

  if (isset($_SESSION['guestlogin']->id)) {
    $authid = $_SESSION['guestlogin']->id;
    // $client_tr = new Client([
    //   "base_uri" => __API_BASE__."",
    //   'verify' => false, //'proxy' => 'http://getaway:sungguhkauterlaludsu@180.235.151.172:3128'
    // ]);
    $client_tr = new Client([
      "base_uri" => __API_BASE__
    ]);
    $options_tr = [
      'headers' => [
        "X-Member-ID" => $authid
      ]
    ];
    $response_tr = $client_tr->get(__API_BASE__ . "/api/web/member", $options_tr);
    $getResponse_tr = $response_tr->getBody();

    // $method = 'GET';
    // $url = 'https://growhappy.co.id/apps/snrv2/api/web/member';
    // $headers = $options_tr;
    // $body = [];
    // $headers = json_encode($headers);
    // $body = json_encode($body);
    // $command = "bash /var/www/html/rewards_sahabatnestle_co_id/public/request-api.sh {$method} {$url} --headers=\"{$headers}\" --body=\"{$body}\"";
    // $process = new \Symfony\Component\Process\Process($command);
    // $process->run();
    // $getResponse_tr = $process->isSuccessful() ? $process->getOutput() : $process->getErrorOutput();

    $exchange_tr = json_decode($getResponse_tr, true);

    $member = $exchange_tr;
  } else {
    $member = NULL;
  }

  $variables['member'] = $member;
  //$variables['$url_login'] = $url_login;
  //$variables['$url_register'] = $url_register;
  $variables['login_url'] = $login_url;
  $variables['register_url'] = $register_url;
  $variables['profile_url'] = $profile_url;
  $variables['lasturl'] = \Drupal::service('path.current')->getPath();
  $lasturl = \Drupal::service('path.current')->getPath();
  $variables['desc'] = explode("/", $lasturl);
  $variables['head'] = end(explode("/", $lasturl));
  $variables['headtitle'] = preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $variables['head']);
  $variables['node_type'] = $variables['desc'][1];
  $variables['subnode'] = $variables['desc'][2];
  $variables['subnode_2'] = $variables['desc'][3];
  $variables['rewardname'] = $variables['desc'][2];
  $articleid = (isset($variables['desc'][2])) ? $variables['desc'][2] : 0;
  $variables['og_image'] = 'http://growhappy.co.id/images/preview.png';

  if (isset($_GET['utm_source'], $_GET['utm_medium'], $_GET['utm_campaign'])) {
    setcookie("utm_source", $_GET['utm_source'], time() + 7200);
    setcookie("utm_medium", $_GET['utm_medium'], time() + 7200);
    setcookie("utm_campaign", $_GET['utm_campaign'], time() + 7200);
  }

  // if ($variables['node_type'] == 'node') {
  //   // $client = new Client(["base_uri" => "https://snr.irisdevlab.com/apps/snrv2"]);
  //   $client = new Client([
  //     "base_uri" => __API_BASE__.""
  //   ]);
  //   $response = $client->get(__API_BASE__."/api/web/article/$articleid");
  //   $getResponse = $response->getBody();
  //   $articleData = json_decode($getResponse);
  //   $variables['article'] = $articleData;
  // }

  if ($variables['node_type'] == 'detail-rewards') {
    $slugrewards = $variables['rewardname'];
    $client = new Client(["base_uri" => __API_BASE__ . ""]);
    $client = new Client([
      "base_uri" => __API_BASE__ . ""
    ]);
    $response = $client->get(__API_BASE__ . "/api/web/rewards/$slugrewards");
    $getResponse = $response->getBody();

    // $method = 'GET';
    // $url = 'https://growhappy.co.id/apps/snrv2/api/web/rewards/'.$slugrewards;
    // $headers = [];
    // $body = [];
    // $headers = json_encode($headers);
    // $body = json_encode($body);
    // $command = "bash /var/www/html/rewards_sahabatnestle_co_id/public/request-api.sh {$method} {$url} --headers=\"{$headers}\" --body=\"{$body}\"";
    // $process = new \Symfony\Component\Process\Process($command);
    // $process->run();
    // $getResponse = $process->isSuccessful() ? $process->getOutput() : $process->getErrorOutput();

    $exchange = json_decode($getResponse);
    $variables['rewards'] = $exchange;
  }

  if ($variables['node_type'] == '99ways' && $variables['subnode'] == 'gallery_detail') {
    if (isset($variables['subnode_2'])) {
      $exp = explode('-', $variables['subnode_2']);
      if (count($exp) != 3) {
        $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . 'not_found';
        return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
      } else {
        $query = \Drupal::database()->select('snrv2_gallery_ninetynine', 'gallery');
        $query->fields('gallery', ['id', 'mother_name', 'child_name', 'raw_image', 'framed_image', 'description']);
        $query->condition('gallery.status', 1);
        $query->condition('gallery.mother_name', $exp[0]);
        $query->condition('gallery.child_name', $exp[1]);
        $query->condition('gallery.id', $exp[2]);
        $gallery_det = $query->execute()->fetchObject();
        $variables['gallery_det'] = $gallery_det;
        $variables['og_image'] = 'http://growhappy.co.id/crm' . $gallery_det->framed_image;
      }
    }
  }


  $_google = "
            if(top!=self){
                top.location.replace(document.location);
                alert('For security reasons, framing is not allowed; click OK to remove the frames.')
            }

            if (navigator.userAgent.indexOf('MSIE') != -1)
            {
                alert('Please use firefox/chrome'); window.location = 'http://www.nestle.com';
            }

             if (self === top) {
                   var antiClickjack = document.getElementById('antiClickjack');
                   antiClickjack.parentNode.removeChild(antiClickjack);
               } else {
                   top.location = self.location;
               }


        ";

  $variables['#attached']['html_head'][] = [
    [
      '#tag' => 'style',
      '#attributes' => array('id' => 'antiClickjack'),

      // Add JavaScript to the <script> tag.
      '#value' => 'body{display:none !important;}'
      // Give weight so it appears after meta tags, etc.
    ],
    // A key, to make it possible to recognize this HTML <HEAD> element when altering.
    'coba'
  ];

  $variables['#attached']['html_head'][] = [
    [
      // Add a <script> tag.
      '#tag' => 'script',
      // Add JavaScript to the <script> tag.
      '#value' => $_google,
      // Give weight so it appears after meta tags, etc.
    ],
    // A key, to make it possible to recognize this HTML <HEAD> element when altering.
    'key'
  ];
}

function growhappytheme_preprocess_page__node(&$variables)
{
  $node = \Drupal::routeMatch()->getParameter('node');
  $node_type = $node->type->entity ? $node->type->entity->label() : '';

  if ($node_type == 'Article') {
    if ($node instanceof \Drupal\node\NodeInterface) {
      $title = $node->getTitle();
      $body = $node->get('body')->getValue();
      $body = preg_replace('/\b&nbsp;\b/', ' ', $body[0]['value']);
      $image = $node->get('field_hero_image')->getValue();
      $image_alt = $image[0]['alt'];
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      } else {
        $image = $node->get('field_image')->getValue();
        if ($image) {
          $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
        }
      }
      $meta_description = $node->get('field_meta_description')->getValue();
      // $image_mobile_alt = $node->get('field_image_mobile')->getValue();
      $tag = $node->get('field_tags')->target_id != null ? \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName() : 'Article';

      $url      = \Drupal::urlGenerator()->generateFromRoute('<front>', [], ['absolute' => TRUE]);
      $url_relative  = substr(\Drupal::request()->getRequestUri(), 1);
      $url_artikel  = $url . $url_relative;
      $variables['artikel'] = [
        'title' => $title,
        'body' => $body,
        'image' => $image,
        // 'image_mobile' => $image_mobile,
        'image_alt' => $image_alt,
        // 'image_mobile_alt' => $image_mobile_alt[0]['alt'],
        'meta_description' => $meta_description,
        'tag' => $tag,
        'share_facebook' => $url_artikel,
        'share_whatsapp' => rawurlencode($title . '%0a' . $url_artikel),
      ];

      /**
       * Get Artikel Terkait
       *
       * @author Aditya Dewantara
       * @since 21 Aug 2020
       */
      $article_terkait = array();
      $query = \Drupal::entityQuery('node');
      $query->condition('status', 1);
      $query->condition('nid', $node->id(), '<>');
      $query->condition('type', 'article');
      if ($node->get('field_tags')->target_id == null) {
        $query->condition('field_tags', $node->get('field_tags')->target_id, 'IS NULL');
      } else {
        $query->condition('field_tags', $node->get('field_tags')->target_id);
      }
      $query->range(0, 3);
      $query->sort('created', 'DESC');
      $entity_ids = $query->execute();
      $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
      foreach ($articles as $art) {
        $title = $art->getTitle();
        $body = $node->get('body')->getValue();
        $image = $art->get('field_hero_image')->getValue();
        $image_alt = $image[0]['alt'];
        if ($image) {
          $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
        } else {
          $image = $art->get('field_image')->getValue();
          if ($image) {
            $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
          }
        }
        // $image_mobile = file_create_url($art->get('field_image_mobile')->entity->getFileUri());
        $meta_description = $node->get('field_meta_description')->getValue();
        // $image_mobile_alt = $art->get('field_image_mobile')->getValue();
        $tag = $art->get('field_tags')->target_id != null ? \Drupal\taxonomy\Entity\Term::load($art->get('field_tags')->target_id)->getName() : null;
        $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

        $article_terkait[] = [
          'title' => $title,
          'image' => $image,
          'body' => $body,
          'image_alt' => $image_alt,
          // 'image_mobile_alt' => $image_mobile_alt[0]['alt'],
          'meta_description' => $meta_description,
          'tag' => $tag,
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $art->id())
        ];
      }

      $variables['artikel_terkait'] = $article_terkait;
    }
  } elseif ($node_type == 'Product') {
    if ($node instanceof \Drupal\node\NodeInterface) {
      $title = $node->getTitle();
      $description = $node->get('field_dsu_product_desc')->getValue();
      $diskon = $node->get('field_diskon')->getValue();
      $fusepump_id = $node->get('field_dsu_sku')->getValue();
      $gramasi = $node->get('field_gramasi')->getValue();
      $informasi = $node->get('field_informasi_nilai_gizi')->getValue();
      $komposisi = $node->get('field_komposisi')->getValue();
      $poin = $node->get('field_poin')->getValue();
      $image = $node->get('field_image')->getValue();
      $image_file = [];
      foreach ($image as $i) {
        $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
      }
      $category = \Drupal\taxonomy\Entity\Term::load($node->get('field_dsu_category')->target_id)->getName();
      $rasa = \Drupal\taxonomy\Entity\Term::load($node->get('field_rasa')->target_id)->getName();

      $variables['produk_detail'] = [
        'title' => $title,
        'description' => $description[0]['value'],
        'diskon' => $diskon[0]['value'],
        'fusepump_id' => $fusepump_id[0]['value'],
        'gramasi' => $gramasi[0]['value'],
        'informasi' => $informasi[0]['value'],
        'komposisi' => $komposisi[0]['value'],
        'poin' => $poin[0]['value'],
        'kandungan' => $kandungan_list,
        'image' => $image_file,
        'category' => $category,
        'rasa' => $rasa
      ];

      /**
       * Get Product Same Category Detail
       *
       * @author Aditya Dewantara
       * @since 09 Nov 2020
       */
      $produk_lain = array();
      $query = \Drupal::entityQuery('node');
      $query->condition('status', 1);
      $query->condition('nid', $node->id(), '<>');
      $query->condition('field_dsu_category', $node->get('field_dsu_category')->target_id);
      $query->condition('field_rasa', $node->get('field_rasa')->target_id);
      $query->sort('field_gramasi', 'ASC');
      $entity_ids = $query->execute();
      $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
      foreach ($products as $prod) {
        $title = $prod->getTitle();
        $gramasi = $prod->get('field_gramasi')->getValue();
        $poin = $prod->get('field_poin')->getValue();

        $produk_lain[] = [
          'title' => $title,
          'poin' => $poin[0]['value'],
          'gramasi' => $gramasi[0]['value'],
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
        ];
      }

      $variables['produk_lain'] = $produk_lain;

      $product_terkait = array();
      $query = \Drupal::entityQuery('node');
      $query->condition('status', 1);
      $query->condition('nid', $node->id(), '<>');
      $query->sort('field_gramasi', 'ASC');
      $query->condition('field_rasa', $node->get('field_rasa')->target_id);
      $query->range(0, 4);
      $entity_ids = $query->execute();
      $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
      foreach ($products as $prod) {
        $title = $prod->getTitle();
        $title = str_replace(array(' Vanila', ' Madu', ' Original'), '', $title);
        $gramasi = $prod->get('field_gramasi')->getValue();
        $fusepump_id = $prod->get('field_dsu_sku')->getValue();
        $poin = $prod->get('field_poin')->getValue();
        $category = \Drupal\taxonomy\Entity\Term::load($prod->get('field_dsu_category')->target_id)->getName();
        $rasa = \Drupal\taxonomy\Entity\Term::load($prod->get('field_rasa')->target_id)->getName();
        $image = $prod->get('field_image')->getValue();
        $image_file = [];
        foreach ($image as $i) {
          $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
        }
        $image_display = $prod->get('field_image_display')->getValue();


        $product_terkait[] = [
          'title' => $title,
          'gramasi' => $gramasi[0]['value'],
          'image' => $image_file,
          'image_display' => count($image_display) > 0 ? file_create_url(\Drupal\file\Entity\File::load($image_display[0]['target_id'])->getFileUri()) : null,
          'category' => $category,
          'fusepump_id' => $fusepump_id[0]['value'],
          'rasa' => strtolower($rasa),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
        ];
      }

      $variables['produk_terkait'] = $product_terkait;
    }
  } elseif ($node_type == 'Event & Promo') {
    $event = \Drupal::routeMatch()->getParameter('node');
    if ($event instanceof \Drupal\node\NodeInterface) {
      $nid = $event->id();
      $title = $event->getTitle();
      $body = $event->get('body')->getValue();
      $hightlight = $event->get('field_highlight_event_promo')->getValue();
      $jenis = $event->get('field_jenis_event_promo')->getValue();
      $gambar = $event->get('field_gambar_event_promo')->entity->getFileUri();
      $gambar2 = file_create_url($gambar);
      $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $event->id());
      $date = $event->get('created')->getValue();
      $alt = $event->get('field_gambar_event_promo')->getValue();

      $konten1 = str_replace("<p>", "", $body[0]['value']);
      $konten2 = str_replace("</p>", "", $konten1);
      $node_event[] = array(
        'node_title' => $title,
        'node_body' => $konten2,
        'node_gambar' => $gambar2,
        'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
        'node_alias' => "" . substr($alias, 1),
        'node_gambar_alt' => $alt[0]['alt'],
        'node_date' => date('d-m-Y', $date[0]['value']),
      );

      //Jenis
      if ($jenis[0]['value'] == 'Promo') {
        $node_event[0]['node_jenis'] = 'Promo';
        $node_event[0]['node_class_jenis'] = 'promo';
      }
      if ($jenis[0]['value'] == 'Event') {
        $node_event[0]['node_jenis'] = 'Event';
        $node_event[0]['node_class_jenis'] = 'event';
      }
      //Highlight event
      if ($hightlight[0]['value'] == 1) {
        $node_event[0]['node_class_highlight'] = 'col-12 col-sm-12 col-md-8 col-lg-6 cell cell-big';
      } else {
        $node_event[0]['node_class_highlight'] = 'col-12 col-sm-6 col-md-4 col-lg-3 cell';
      }

      $variables['eventpromo'] = [
        'title' => $title,
        'body' => $konten2,
        'image' => $gambar2,
        // 'image_mobile' => $image_mobile,
        'image_alt' => $alt[0]['alt'],
        // 'image_mobile_alt' => $image_mobile_alt[0]['alt'],
        // 'meta_description' => $meta_description,
        'tag' => $jenis[0]['value'],
        // 'share_facebook' => $url_artikel,
        // 'share_whatsapp' => rawurlencode($title . '%0a' . $url_artikel),
      ];

      $variables['event_existing'] = $node_event;
    }

    //Artikel terbaru
    $node_article = array();
    $variables['artikel_terbaru'] = $node_article;
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'dsu_article');
    // $query->orderBy("created", "DESC");
    $query->range(0, 3);
    $entity_ids = $query->execute();
    $nodes_artikel = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
    $id = 0;
    foreach ($nodes_artikel as $artikel) {
      $title = $artikel->getTitle();
      $body = $artikel->get('body')->getValue();
      $hightlight = $artikel->get('field_highlight_artikel')->getValue();
      $sticky = $artikel->get('field_sticky_artikel')->getValue();
      $topik = $artikel->get('field_topik_artikel')->getValue();
      $usia = $artikel->get('field_dsu_category')->getValue();
      $gambar = $artikel->get('field_image_article')->entity->getFileUri();
      $gambar2 = file_create_url($gambar);
      $alt = $artikel->get('field_dsu_image')->getValue();
      $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $artikel->id());
      $date = $artikel->get('created')->getValue();

      $konten1 = str_replace("<p>", "", $body[0]['value']);
      $konten2 = str_replace("</p>", "", $konten1);
      $node_article[] = array(
        'node_title' => $title,
        'node_body' => $konten2,
        'node_sticky' => $sticky,
        'node_gambar' => $gambar2,
        'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
        'node_gambar_alt' => $alt[0]['alt'],
        'node_alias' => "" . substr($alias, 1),
        'node_date' => date('d-m-Y', $date[0]['value']),
      );
      //Highlight artikel
      if ($hightlight[0]['value'] == 1) {
        $node_article[$id]['node_class_highlight'] = 'col-12 col-sm-12 col-md-8 col-lg-6 cell cell-big';
      } else {
        $node_article[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-md-4 col-lg-3 cell';
      }
      //Kategori Usia
      if ($usia[0]['target_id'] == 1) {
        $node_article[$id]['node_usia'] = '1';
      }
      if ($usia[0]['target_id'] == 2) {
        $node_article[$id]['node_usia'] = '3';
      }
      if ($usia[0]['target_id'] == 3) {
        $node_article[$id]['node_usia'] = '5';
      }
      //Topik artikel
      if ($topik[0]['target_id'] == 4) {
        $node_article[$id]['node_topik'] = 'Nutrisi';
        $node_article[$id]['node_class_topik'] = 'nutrisi';
      }
      if ($topik[0]['target_id'] == 5) {
        $node_article[$id]['node_topik'] = 'Parenting';
        $node_article[$id]['node_class_topik'] = 'parenting';
      }
      if ($topik[0]['target_id'] == 6) {
        $node_article[$id]['node_topik'] = 'Stimulasi';
        $node_article[$id]['node_class_topik'] = 'stimulasi';
      }
      $id = $id + 1;
    }
    $variables['artikel_terbaru'] = $node_article;

    //Promo
    $node_promo = array();
    $variables['promo_list'] = $node_promo;
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'event_promo');
    $query->condition('field_jenis_event_promo', 'promo');
    $query->range(0, 4);
    $entity_ids = $query->execute();
    $nodes_promo = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
    $id = 0;
    foreach ($nodes_promo as $promo) {
      $gambar = $promo->get('field_gambar_event_promo')->entity->getFileUri();
      $gambar2 = file_create_url($gambar);
      $alt = $promo->get('field_gambar_event_promo')->getValue();
      $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $promo->id());

      $node_promo[] = array(
        'node_gambar' => $gambar2,
        'node_gambar_alt' => $alt[0]['alt'],
        'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
        'node_alias' => "" . substr($alias, 1),
      );
    }
    $variables['promo_list'] = $node_promo;

    //Event
    $node_event = array();
    $variables['event_list'] = $node_event;
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'event_promo');
    $query->condition('field_jenis_event_promo', 'event');
    $query->range(0, 4);
    $entity_ids = $query->execute();
    $nodes_event = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
    $id = 0;
    foreach ($nodes_event as $event) {
      $gambar = $event->get('field_gambar_event_promo')->entity->getFileUri();
      $gambar2 = file_create_url($gambar);
      $alt = $event->get('field_gambar_event_promo')->getValue();
      $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $event->id());

      $node_event[] = array(
        'node_gambar' => $gambar2,
        'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
        'node_gambar_alt' => $alt[0]['alt'],
        'node_alias' => "" . substr($alias, 1),
      );
    }
    $variables['event_list'] = $node_event;
  } elseif ($node_type == 'Faq') {

    if ($node instanceof \Drupal\node\NodeInterface) {
      $title = $node->getTitle();
      $body = $node->get('body')->getValue();
      $meta_description = $node->get('field_meta_description')->getValue();
      $cara_gabung = $node->get('field_cara_gabung')->getValue();
      $pertanyaan = $node->get('field_pertanyaan')->getValue();
      $syarat_ketentuan = $node->get('field_syarat_dan_ketentuan')->getValue();

      $list_gambar_cara_gabung = $node->get('field_image_cara_gabung')->getValue();

      foreach ($list_gambar_cara_gabung as $key => $gambar) {
        if ($gambar) {
          $list_gambar_cara_gabung[$key]['url'] = file_create_url(\Drupal\file\Entity\File::load($gambar['target_id'])->getFileUri());
        }
      }

      $list_syarat_dan_ketentuan_label = $node->getFieldDefinition('field_list_syarat_dan_ketentuan')->getFieldStorageDefinition()->getSetting('allowed_values');
      $list_syarat_dan_ketentuan = $node->get('field_list_syarat_dan_ketentuan')->getValue();
      $i = 0;
      foreach ($list_syarat_dan_ketentuan_label as $key => $sk) {
        if ($list_syarat_dan_ketentuan[$i]['value']) {
          $list_syarat_dan_ketentuan[$i]['label'] = $list_syarat_dan_ketentuan_label[$key];
        }
        $i++;
      }

      $list_pertanyaan_label = $node->getFieldDefinition('field_list_pertanyaan')->getFieldStorageDefinition()->getSetting('allowed_values');
      $list_pertanyaan = $node->get('field_list_pertanyaan')->getValue();

      $i = 0;
      foreach ($list_pertanyaan_label as $key => $sk) {
        if ($list_pertanyaan[$i]['value']) {
          $list_pertanyaan[$i]['label'] = $list_pertanyaan_label[$key];
        }
        $i++;
      }

      $variables['faq'] = [
        'title' => $title,
        'body' => $body[0]['value'],
        'meta_description' => $meta_description,
        'cara_gabung' => $cara_gabung,
        'list_gambar_cara_gabung' => $list_gambar_cara_gabung,
        'pertanyaan' => $pertanyaan,
        'syarat_ketentuan' => $syarat_ketentuan,
        'list_syarat_dan_ketentuan' => $list_syarat_dan_ketentuan,
        'list_pertanyaan' => $list_pertanyaan
      ];
      // var_dump($variables['faq']['list_syarat_dan_ketentuan']); die;
    }
  }


  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }
}

function growhappytheme_preprocess_page__product(&$variables)
{




  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $produk_all = array();
  $produk_cat = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'dsu_product');
  // $query->sort('field_gramasi', 'ASC');
  $query->sort('field_rasa', 'DESC');
  $query->sort('field_dsu_category', 'ASC');
  $query->sort('field_gramasi', 'ASC');
  // $query->range('field_gramasi', 'ASC');
  $entity_ids = $query->execute();
  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($products as $prod) {
    $title = $prod->getTitle();
    $title = str_replace(array(' Vanila', ' Madu', ' Original'), '', $title);
    $gramasi = $prod->get('field_gramasi')->getValue();
    $fusepump_id = $prod->get('field_dsu_sku')->getValue();
    $poin = $prod->get('field_poin')->getValue();
    $category = \Drupal\taxonomy\Entity\Term::load($prod->get('field_dsu_category')->target_id)->getName();
    $rasa = \Drupal\taxonomy\Entity\Term::load($prod->get('field_rasa')->target_id)->getName();
    $image = $prod->get('field_image')->getValue();
    $image_file = [];
    foreach ($image as $i) {
      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
    }
    $image_display = $prod->get('field_image_display')->getValue();

    $produk_all[] = [
      'title' => $title,
      'gramasi' => $gramasi[0]['value'],
      'image' => $image_file,
      'image_display' => count($image_display) > 0 ? file_create_url(\Drupal\file\Entity\File::load($image_display[0]['target_id'])->getFileUri()) : null,
      'category' => $category,
      'fusepump_id' => $fusepump_id[0]['value'],
      'rasa' => strtolower($rasa),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
    $produk_cat[$category][] = [
      'title' => $title,
      'gramasi' => $gramasi[0]['value'],
      'image' => $image_file,
      'image_display' => count($image_display) > 0 ? file_create_url(\Drupal\file\Entity\File::load($image_display[0]['target_id'])->getFileUri()) : null,
      'category' => $category,
      'fusepump_id' => $fusepump_id[0]['value'],
      'rasa' => strtolower($rasa),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }

  // dd($produk_cat);
  $variables['produk'] = $produk_all;
  $variables['produk_category'] = $produk_cat;
}

function growhappytheme_preprocess_page__happyarticle(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Article']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  $orGroup = $query->orConditionGroup()
    ->condition('field_tags', null, 'IS NULL')
    ->condition('field_tags', $term->id());
  $query->condition($orGroup);

  $query->range(0, 5);
  $query->sort('created', 'DESC');

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__semua(&$variables)
{
  $session = new \Symfony\Component\HttpFoundation\Session\Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Article']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  $orGroup = $query->orConditionGroup()
    ->condition('field_tags', null, 'IS NULL')
    ->condition('field_tags', $term->id());
  $query->condition($orGroup);

  // $query->range(0, 5);

  $query->sort('created', 'DESC');

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__happytummy(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Happy Tummy']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $query->condition('field_tags', $term->id());

  if ($search != null) {
    // dd($search);
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  // $query->condition('field_tags',13);

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__happytummy__1hingga3tahun(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    // ->loadByProperties(['name' => 'Happy Tummy', 'name' => '1-3 Tahun happyartikel']);
    ->loadByProperties(['name' => 'Happy Tummy']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  // dd($term->id());
  // $query->condition('field_tags',$term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  $and = $query->andConditionGroup();
  $and->condition('field_tags', 10);
  $query->condition($and);
  $and = $query->andConditionGroup();
  $and->condition('field_tags', 11);
  $query->condition($and);

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'happytummy';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__happytummy__prasekolah(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    // ->loadByProperties(['name' => 'Happy Tummy', 'name' => '1-3 Tahun happyartikel']);
    ->loadByProperties(['name' => 'Happy Tummy']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  // $query->condition('field_tags',$term->id());
  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }



  $and = $query->andConditionGroup();
  $and->condition('field_tags', 10); // staging 10 local 13
  $query->condition($and);
  $and = $query->andConditionGroup();
  $and->condition('field_tags', 12); // staging 12 local 15
  $query->condition($and);

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'happytummy';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__happyparenting(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Happy Parenting']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $query->condition('field_tags', $term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'happy parenting';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__happyparenting__1hingga3tahun(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    // ->loadByProperties(['name' => 'Happy Tummy', 'name' => '1-3 Tahun happyartikel']);
    ->loadByProperties(['name' => 'Happy Tummy']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $and = $query->andConditionGroup();
  $and->condition('field_tags', 9);
  $query->condition($and);
  $and = $query->andConditionGroup();
  $and->condition('field_tags', 11);
  $query->condition($and);
  // dd($term->id());
  // $query->condition('field_tags',$term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'happytummy';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__happyparenting__prasekolah(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    // ->loadByProperties(['name' => 'Happy Tummy', 'name' => '1-3 Tahun happyartikel']);
    ->loadByProperties(['name' => 'Happy Tummy']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $and = $query->andConditionGroup();
  $and->condition('field_tags', 9); // staging 9 local 12
  $query->condition($and);
  $and = $query->andConditionGroup();
  $and->condition('field_tags', 12); // staging 12 local 15
  $query->condition($and);
  // dd($term->id());
  // $query->condition('field_tags',$term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'happytummy';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__1hingga3tahun(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => '1-3 Tahun happyartikel']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $query->condition('field_tags', $term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = '1-3 tahun';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyarticle__prasekolah(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Pra Sekolah happyartikel']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $query->condition('field_tags', $term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  // $orGroup = $query->orConditionGroup()
  //           ->condition('field_tags', null,'IS NULL')
  //           ->condition('field_tags', $term->id());
  // $query->condition($orGroup);

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $image = $prod->get('field_hero_image')->getValue();
    $body = $prod->get('body')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'image_alt' => $image_alt,
      'body' => $body,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'pra sekolah';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyupdates(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];
  //For pagination
  $page = \Drupal::request()->get('page');
  if (!$page) {
    $page = 1;
    $a = 0;
    $b = 10;
  } else {
    if ($page > 1) {
      $a = (($page - 1) * 10);
      $b = 10;
    } else {
      $page = 1;
      $a = 0;
      $b = 10;
    }
  }

  $node_event = array();
  $variables['event_list'] = $node_event;
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'event_promo');
  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }
  $query->range($a, $b);
  $query->sort('field_highlight_event_promo', 'DESC');
  $query->sort('created', 'DESC');
  $entity_ids = $query->execute();

  $count = \Drupal::entityQuery('node');
  $count->condition('status', 1);
  $count->condition('type', 'event_promo');
  $count_id = $count->execute();
  $count = count($count_id);

  $nodes_event = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  $id = 0;
  foreach ($nodes_event as $event) {
    $title = $event->getTitle();
    $body = $event->get('body')->getValue();
    $gambar = $event->get('field_gambar_event_promo')->entity->getFileUri();
    $gambar2 = file_create_url($gambar);
    $alt = $event->get('field_gambar_event_promo')->getValue();
    $jenis = $event->get('field_jenis_event_promo')->getValue();
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $event->id());
    $konten1 = str_replace("<p>", "", $body[0]['value']);
    $konten2 = str_replace("</p>", "", $konten1);
    $hightlight = $event->get('field_highlight_event_promo')->getValue();

    $node_event[] = array(
      'node_title' => str_replace("public://", "/", $title),
      'node_gambar' => $gambar2,
      'node_gambar_alt' => $alt[0]['alt'],
      'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
      'node_body' => str_replace("public://", "/", $konten2),
      'node_alias' => "" . substr($alias, 1),
    );
    //Jenis
    if ($jenis[0]['value'] == 'Promo') {
      $node_event[$id]['node_jenis'] = 'Promo';
      $node_event[$id]['node_class_jenis'] = 'promo';
    }
    if ($jenis[0]['value'] == 'Event') {
      $node_event[$id]['node_jenis'] = 'Event';
      $node_event[$id]['node_class_jenis'] = 'event';
    }
    //Highlight
    if ($id < 2) {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-12 col-md-8 col-lg-6 cell cell-big';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    } else {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-md-4 col-lg-3 cell';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    }

    $id = $id + 1;
  }
  $variables['page_happy'] = 'all';
  $variables['eventpromo'] = $node_event;
  $variables['dpc_pages_pagination'] = ceil($count / 10);
  $variables['current_page'] = $page;
}
function growhappytheme_preprocess_page__happyupdates__events(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  // dd("lacto");
  $search = $_GET['s'];

  //For pagination
  $page = \Drupal::request()->get('page');
  if (!$page) {
    $page = 1;
    $a = 0;
    $b = 10;
  } else {
    if ($page > 1) {
      $a = (($page - 1) * 10);
      $b = 10;
    } else {
      $page = 1;
      $a = 0;
      $b = 10;
    }
  }

  $node_event = array();
  $variables['event_list'] = $node_event;
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'event_promo');
  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }
  $query->condition('field_jenis_event_promo', 'event');
  $query->range($a, $b);
  $query->sort('field_highlight_event_promo', 'DESC');
  $query->sort('created', 'DESC');
  $entity_ids = $query->execute();

  $count = \Drupal::entityQuery('node');
  $count->condition('status', 1);
  $count->condition('type', 'event_promo');
  $query->condition('field_jenis_event_promo', 'event');
  $count_id = $count->execute();
  $count = count($count_id);

  $nodes_event = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  $id = 0;
  foreach ($nodes_event as $event) {
    $title = $event->getTitle();
    $body = $event->get('body')->getValue();
    $gambar = $event->get('field_gambar_event_promo')->entity->getFileUri();
    $gambar2 = file_create_url($gambar);
    $alt = $event->get('field_gambar_event_promo')->getValue();
    $jenis = $event->get('field_jenis_event_promo')->getValue();
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $event->id());
    $konten1 = str_replace("<p>", "", $body[0]['value']);
    $konten2 = str_replace("</p>", "", $konten1);
    $hightlight = $event->get('field_highlight_event_promo')->getValue();

    $node_event[] = array(
      'node_title' => str_replace("public://", "/", $title),
      'node_gambar' => $gambar2,
      'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
      'node_gambar_alt' => $alt[0]['alt'],
      'node_body' => str_replace("public://", "/", $konten2),
      'node_alias' => "" . substr($alias, 1),
    );
    //Jenis
    if ($jenis[0]['value'] == 'Promo') {
      $node_event[$id]['node_jenis'] = 'Promo';
      $node_event[$id]['node_class_jenis'] = 'promo';
    }
    if ($jenis[0]['value'] == 'Event') {
      $node_event[$id]['node_jenis'] = 'Event';
      $node_event[$id]['node_class_jenis'] = 'event';
    }
    //Highlight
    if ($id < 2) {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-12 col-md-8 col-lg-6 cell cell-big';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    } else {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-md-4 col-lg-3 cell';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    }

    $id = $id + 1;
  }
  $variables['page_happy'] = 'events';
  $variables['eventpromo'] = $node_event;
  $variables['dpc_pages_pagination'] = ceil($count / 10);
  $variables['current_page'] = $page;
}

function growhappytheme_preprocess_page__happyupdates__promo(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];

  \Drupal::service('page_cache_kill_switch')->trigger();
  //For pagination
  $page = \Drupal::request()->get('page');
  $perpage = 10;
  if (!$page) {
    $page = 1;
    $a = 0;
    $b = $perpage;
  } else {
    if ($page > 1) {
      $a = (($page - 1) + $perpage);
      $b = $perpage * $page;
    } else {
      $page = 1;
      $a = 0;
      $b = $perpage;
    }
  }

  $node_event = array();
  $variables['event_list'] = $node_event;
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'event_promo');
  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }
  $query->condition('field_jenis_event_promo', 'promo');
  $query->range($a, $b);
  $query->sort('field_highlight_event_promo', 'DESC');
  $query->sort('created', 'DESC');
  $entity_ids = $query->execute();

  $count = \Drupal::entityQuery('node');
  $count->condition('status', 1);
  $count->condition('type', 'event_promo');
  $query->condition('field_jenis_event_promo', 'promo');
  $count_id = $count->execute();
  $count = count($count_id);

  $nodes_event = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  $id = 0;
  foreach ($nodes_event as $event) {
    $title = $event->getTitle();
    $body = $event->get('body')->getValue();
    $gambar = $event->get('field_gambar_event_promo')->entity->getFileUri();
    $gambar2 = file_create_url($gambar);
    $alt = $event->get('field_gambar_event_promo')->getValue();
    $jenis = $event->get('field_jenis_event_promo')->getValue();
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $event->id());
    $konten1 = str_replace("<p>", "", $body[0]['value']);
    $konten2 = str_replace("</p>", "", $konten1);
    $hightlight = $event->get('field_highlight_event_promo')->getValue();

    $node_event[] = array(
      'node_title' => str_replace("public://", "/", $title),
      'node_gambar' => $gambar2,
      'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
      'node_gambar_alt' => $alt[0]['alt'],
      'node_body' => str_replace("public://", "/", $konten2),
      'node_alias' => "" . substr($alias, 1),
    );
    //Jenis
    if ($jenis[0]['value'] == 'Promo') {
      $node_event[$id]['node_jenis'] = 'Promo';
      $node_event[$id]['node_class_jenis'] = 'promo';
    }
    if ($jenis[0]['value'] == 'Event') {
      $node_event[$id]['node_jenis'] = 'Event';
      $node_event[$id]['node_class_jenis'] = 'event';
    }
    //Highlight
    if ($id < 2) {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-12 col-md-8 col-lg-6 cell cell-big';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    } else {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-md-4 col-lg-3 cell';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    }

    $id = $id + 1;
  }
  $variables['page_happy'] = 'promo';
  $variables['eventpromo'] = $node_event;
  $variables['dpc_pages_pagination'] = ceil($count / 10);
  $variables['current_page'] = $page;
}


function growhappytheme_preprocess_page__happyupdates__event_promo(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];
  //For pagination
  $page = \Drupal::request()->get('page');
  if (!$page) {
    $page = 1;
    $a = 0;
    $b = 10;
  } else {
    if ($page > 1) {
      $a = (($page - 1) * 10);
      $b = 10;
    } else {
      $page = 1;
      $a = 0;
      $b = 10;
    }
  }

  $node_event = array();
  $variables['event_list'] = $node_event;
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'event_promo');
  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }
  $query->range($a, $b);
  $query->sort('field_highlight_event_promo', 'DESC');
  $query->sort('created', 'DESC');
  $entity_ids = $query->execute();

  $count = \Drupal::entityQuery('node');
  $count->condition('status', 1);
  $count->condition('type', 'event_promo');
  $count_id = $count->execute();
  $count = count($count_id);

  $nodes_event = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  $id = 0;
  foreach ($nodes_event as $event) {
    $title = $event->getTitle();
    $body = $event->get('body')->getValue();
    $gambar = $event->get('field_gambar_event_promo')->entity->getFileUri();
    $gambar2 = file_create_url($gambar);
    $alt = $event->get('field_gambar_event_promo')->getValue();
    $jenis = $event->get('field_jenis_event_promo')->getValue();
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $event->id());
    $konten1 = str_replace("<p>", "", $body[0]['value']);
    $konten2 = str_replace("</p>", "", $konten1);
    $hightlight = $event->get('field_highlight_event_promo')->getValue();

    $node_event[] = array(
      'node_title' => str_replace("public://", "/", $title),
      'node_gambar' => $gambar2,
      'node_gambar_alt' => $alt[0]['alt'],
      'node_gambar_webp' => str_replace(array('.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG'), '.webp', $gambar2),
      'node_body' => str_replace("public://", "/", $konten2),
      'node_alias' => "" . substr($alias, 1),
    );
    //Jenis
    if ($jenis[0]['value'] == 'Promo') {
      $node_event[$id]['node_jenis'] = 'Promo';
      $node_event[$id]['node_class_jenis'] = 'promo';
    }
    if ($jenis[0]['value'] == 'Event') {
      $node_event[$id]['node_jenis'] = 'Event';
      $node_event[$id]['node_class_jenis'] = 'event';
    }
    //Highlight
    if ($id < 2) {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-12 col-md-8 col-lg-6 cell cell-big';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    } else {
      //   $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-md-4 col-lg-3 cell';
      $node_event[$id]['node_class_highlight'] = 'col-12 col-sm-6 col-lg-4 cell';
    }

    $id = $id + 1;
  }
  $variables['eventpromo'] = $node_event;
  $variables['dpc_pages_pagination'] = ceil($count / 10);
  $variables['current_page'] = $page;
}
function growhappytheme_preprocess_page__happyupdates__backup(&$variables)
{
  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Article']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  $query->condition('field_tags', $term->id(), '<>');
  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $body = $prod->get('body')->getValue();
    $image = $prod->get('field_hero_image')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    $tag = $prod->get('field_tags')->target_id != null ? \Drupal\taxonomy\Entity\Term::load($prod->get('field_tags')->target_id)->getName() : null;
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'body' => $body,
      'image_alt' => $image_alt,
      'tag' => $tag,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'all';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyupdates__events__backup(&$variables)
{
  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Event']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $query->condition('field_tags', $term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $body = $prod->get('body')->getValue();
    $image = $prod->get('field_hero_image')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    $tag = $prod->get('field_tags')->target_id != null ? \Drupal\taxonomy\Entity\Term::load($prod->get('field_tags')->target_id)->getName() : null;
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'body' => $body,
      'image_alt' => $image_alt,
      'tag' => $tag,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['search_query'] = $search;
  $variables['page_happy'] = 'events';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__happyupdates__promo__backup(&$variables)
{
  $search = $_GET['s'];

  $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => 'Promo']);
  $term = reset($term);
  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'article');
  $query->condition('field_tags', $term->id());

  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }

  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $body = $prod->get('body')->getValue();
    $image = $prod->get('field_hero_image')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    $tag = $prod->get('field_tags')->target_id != null ? \Drupal\taxonomy\Entity\Term::load($prod->get('field_tags')->target_id)->getName() : null;
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $article[] = [
      'title' => $title,
      'image' => $image,
      'body' => $body,
      'image_alt' => $image_alt,
      'tag' => $tag,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }
  $variables['page_happy'] = 'promo';
  $variables['article'] = $article;
  $variables['total_article'] = count($article);
}

function growhappytheme_preprocess_page__front(&$variables)
{

  $session = new Session();

  $session->set('lacto', 'lactorevamp');
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  } else {
    $session->set('guestlogin', null);
    // dd($session->get('guestlogin'));
  }

  $product_3 = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('title', '%' . db_like('LACTOGROW 3') . '%', 'LIKE');
  $entity_ids = $query->execute();
  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);

  foreach ($products as $prod) {
    $fusepump_id = $prod->get('field_dsu_sku')->getValue();
    $image = $prod->get('field_image')->getValue();
    $image_file = [];
    foreach ($image as $i) {
      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
    }

    $product_3[] = [
      'image' => $image_file[1],
      'fusepump_id' => $fusepump_id[0]['value'],
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }

  $variables['produk_3'] = $product_3;

  $product_4 = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('title', '%' . db_like('LACTOGROW 4') . '%', 'LIKE');
  $entity_ids = $query->execute();
  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);

  foreach ($products as $prod) {
    $fusepump_id = $prod->get('field_dsu_sku')->getValue();
    $image = $prod->get('field_image')->getValue();
    $image_file = [];
    foreach ($image as $i) {
      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
    }

    $product_4[] = [
      'image' => $image_file[1],
      'fusepump_id' => $fusepump_id[0]['value'],
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }

  $variables['produk_4'] = $product_4;

  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->range(0, 5);
  $query->condition('type', 'article');
  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $body = $prod->get('body')->getValue();
    $image = $prod->get('field_hero_image')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));
    $article[] = [
      'title' => $title,
      'body' => $body,
      'image' => $image,
      'image_alt' => $image_alt,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }

  $variables['article'] = $article;

  $client = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards"]);

  $tokenUrl = $client->get("https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards");
  // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=20_5tucsud0tlay95iu87m9m7odb4s38xak1y3j5bb26m9yafl4br&client_secret=048kyvt31l4so71o752o3gs5beysoyytoq4d9tn0mevxj87f44");
  $getReward = $tokenUrl->getBody();
  $rewards = json_decode($getReward, true);

  $image = "https://www.rewards.sahabatnestle.co.id/images/rewards/";

  $variables['rewardslisting'] = $rewards;
  $variables['rewardsimage'] = $image;
}

function growhappytheme_preprocess_page__dream_big(&$variables)
{



  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $product_3 = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('title', '%' . db_like('LACTOGROW 3') . '%', 'LIKE');
  $entity_ids = $query->execute();
  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);

  foreach ($products as $product) {
    $fusepump_id = $product->get('field_dsu_sku')->getValue();
    $description = $product->get('field_dsu_product_desc')->getValue();
    $image = $product->get('field_image')->getValue();
    $image_file = [];
    foreach ($image as $i) {
      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
    }

    $product_3[] = [
      'image' => $image_file[1],
      'fusepump_id' => $fusepump_id[0]['value'],
      'description' => $description[0]['value'],
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $product->id())
    ];
  }

  $variables['produk_3'] = $product_3;

  $tips = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('title', '%' . db_like('Tips') . '%', 'LIKE');
  $entity_ids = $query->execute();
  $tips_list = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($tips_list as $tip) {
    $title = $tip->getTitle();
    $body = $tip->get('body')->getValue();
    $image = $tip->get('field_hero_image')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $tip->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    $meta_description = $tip->get('field_meta_description')->getValue();
    $tag = $tip->get('field_tags')->target_id != null ? \Drupal\taxonomy\Entity\Term::load($tip->get('field_tags')->target_id)->getName() : null;
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $tips[] = [
      'title' => $title,
      'image' => $image,
      'body' => $body,
      'image_alt' => $image_alt,
      'meta_description' => $meta_description,
      'tag' => $tag,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $tip->id())
    ];
  }
  $variables['tips'] = $tips;

  $banners = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('title', '%' . db_like('Dream Big Banner') . '%', 'LIKE');
  $entity_ids = $query->execute();
  $banners_list = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($banners_list as $banner) {
    $title = $banner->getTitle();
    $image_desktop = $banner->get('field_image_banner_desktop')->getValue();
    $image_mobile = $banner->get('field_image_banner_mobile')->getValue();
    $image_alt_desktop = $image_desktop[0]['alt'];
    $image_alt_mobile = $image_mobile[0]['alt'];
    $link_banner = $banner->get('field_link_banner')->getValue();
    if ($image_desktop) {
      $image_desktop = file_create_url(\Drupal\file\Entity\File::load($image_desktop[0]['target_id'])->getFileUri());
    } else {
      $image_desktop = $banner->get('field_image_banner_desktop')->getValue();
      if ($image_desktop) {
        $image_desktop = file_create_url(\Drupal\file\Entity\File::load($image_desktop[0]['target_id'])->getFileUri());
      }
    }

    if ($image_mobile) {
      $image_mobile = file_create_url(\Drupal\file\Entity\File::load($image_mobile[0]['target_id'])->getFileUri());
    } else {
      $image_mobile = $banner->get('field_image_banner_mobile')->getValue();
      if ($image_mobile) {
        $image_mobile = file_create_url(\Drupal\file\Entity\File::load($image_mobile[0]['target_id'])->getFileUri());
      }
    }

    $banners[] = [
      'title' => $title,
      'image_desktop' => $image_desktop,
      'image_mobile' => $image_mobile,
      'image_desktop_alt' => $image_alt_desktop,
      'image_mobile_alt' => $image_alt_mobile,
      'link' => $link_banner
    ];
  }
  $variables['banners'] = $banners;
}

function growhappytheme_preprocess_page__faq(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $search = $_GET['s'];
  $limit = $_GET['limit'] ?: 6;
  $faqs = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('type', 'faq');
  if ($search != null) {
    $query->condition('title', "%" . db_like($search) . "%", 'LIKE');
  }
  $query->range(0, intval($limit));
  $query->sort('created', 'DESC');
  $entity_ids = $query->execute();

  $faqs_list = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($faqs_list as $faq) {
    $title = $faq->getTitle();
    $body = $faq->get('body')->getValue();
    $meta_description = $faq->get('field_meta_description')->getValue();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));

    $faqs[] = [
      'title' => $title,
      'body' => $body,
      'meta_description' => $meta_description,
      'limit' => $limit ?: 6,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $faq->id())
    ];
  }
  $variables['faqs'] = $faqs;
}

function growhappytheme_preprocess_page__login(&$variables)
{


  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;

    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path();
    // dd($redirectto);
    // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
    $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
    $response->send(); // don't send the response yourself inside controller and form.
    return;
  }


  // $redirectto = "https://rewards.sahabatnestle.co.id/apps/sso/sso/auth/login?response_type=code&client_id=5_2m5v84a4i8g0cgsssocg0gkoc88c0wksww0c488ww0w4sooccs&redirect_uri=http%3A%2F%2Frewards.sahabatnestle.co.id%2Fsso%2Fcallback&scope=user";
  // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);

  $base_url = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__;

  // dd($base_url);
  // $query = \Drupal::database()->select($this->user_tbl, 'user');
  // $query->fields('user', ['id', 'username', 'email', 'created_at', 'updated_at', 'status']);
  // // $query->condition('user.status', 1);
  // $getData = $query->execute()->fetchAll();

  if (isset($_GET['failed'])) {
    $variables['data']['failed'] = $_GET['failed'];
  } else {
    $variables['data']['failed'] = '';
  }

  if (isset($_GET['reset'])) {
    $variables['data']['reset'] = $_GET['reset'];
  } else {
    $variables['data']['reset'] = '';
  }

  //echo $data;exit;
  //set variable for view
  $mergeData = (object) array(
    'reset' => $reset,
    'failed' => $failed
  );

  $element = array(
    '#theme' => 'login',
    '#data' => $mergeData,
    '#base_url' => $base_url,
  );
  return $element;
}

function growhappytheme_preprocess_page__loginprocess(&$variables)
{

//   // dd('login');
//   // dd(isset($_POST['password']));
//   $base_url = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__;

//   // print_r($_POST);
//   // exit;

//   if (isset($_POST['email'], $_POST['password'])) {

//     try {

//       $client = new Client(["base_uri" => "https://rewards.sahabatnestle.co.id/apps/sso/sso/token"]);

//       // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=" . __SSO_CLIENT_ID__ . "&client_secret=" . __SSO_SECRET__);
//       $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=14_2uw5bdjbf6eck408w8wkk8k08g884000ck4scc884kogcc8gs0&client_secret=2jc2nsgrlt0kk88k8k0gokgggkocgcgos8ws0880w0gkwk0g8k");
//       // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=20_5tucsud0tlay95iu87m9m7odb4s38xak1y3j5bb26m9yafl4br&client_secret=048kyvt31l4so71o752o3gs5beysoyytoq4d9tn0mevxj87f44");
//       $getToken = $tokenUrl->getBody();
//       $token = json_decode($getToken, true);
//     } catch (\Exception $e) {

//       $client = new Client(["base_uri" => "https://www.rewards.sahabatnestle.co.id/apps/sso/sso/token"]);

//       // $tokenUrl = $client->get("https://www.rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=" . __SSO_CLIENT_ID__ . "&client_secret=" . __SSO_SECRET__);
//       $tokenUrl = $client->get("https://www.rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=14_2uw5bdjbf6eck408w8wkk8k08g884000ck4scc884kogcc8gs0&client_secret=2jc2nsgrlt0kk88k8k0gokgggkocgcgos8ws0880w0gkwk0g8k");
//       // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=20_5tucsud0tlay95iu87m9m7odb4s38xak1y3j5bb26m9yafl4br&client_secret=048kyvt31l4so71o752o3gs5beysoyytoq4d9tn0mevxj87f44");
//       $getToken = $tokenUrl->getBody();
//       $token = json_decode($getToken, true);
//     }

//     // dd($token);

//     // print_r($token);
//     // exit;

//     if (!isset($token)) {
//       $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/login?failed=1';
//       return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//     } else {
//       // dd("token");
//     }

//     $dataPost = http_build_query(array(
//       'email' => $_POST['email'],
//       'password' => $_POST['password']
//     ));

//     $ch = curl_init();
//     curl_setopt($ch, CURLOPT_URL, 'https://rewards.sahabatnestle.co.id/apps/sso/api/v2/sso/user/login');

//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//     curl_setopt($ch, CURLOPT_POST, 1);
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPost);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//     curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
//     curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2);
//     curl_setopt($ch, CURLOPT_HTTPHEADER, [
//       sprintf('Authorization: Bearer %s', $token['access_token'])
//     ]);
//     $result = curl_exec($ch);
//     $login = json_decode($result, true);

//     if (!isset($login)) {

//       $ch = curl_init();
//       curl_setopt($ch, CURLOPT_URL, 'https://www.rewards.sahabatnestle.co.id/apps/sso/api/v2/sso/user/login');

//       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//       curl_setopt($ch, CURLOPT_POST, 1);
//       curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPost);
//       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//       curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
//       curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2);
//       curl_setopt($ch, CURLOPT_HTTPHEADER, [
//         sprintf('Authorization: Bearer %s', $token['access_token'])
//       ]);
//       $result = curl_exec($ch);
//       $login = json_decode($result, true);
//     }

//     if ($login['code'] == 200) {


//       $session = new Session();

//       try {
//         $session->set('guestlogin', $login);
//       } catch (\Symfony\Component\HttpFoundation\Session\Session $e) {
//         $session->set('guestlogin', $login);
//       }


//       // Redirect ke stress tools
//       if ($session->get('redirect_stress_test')) {

//         $user = $login['level1'];
//         $check_ibu = \Drupal::database()->select('lacto_stresstools_ibu', 'ibu')
//           ->condition('sso_id', $user['id_sso_user'])
//           ->countQuery()
//           ->execute()
//           ->fetchAssoc();

//         // Kalau hasilnya nol berarti belum ada di table lacto_stresstools_ibu, jadi di buat dulu ya, :D
//         if($check_ibu['expression'] == 0) {
//           $ibu = \Drupal::database()->insert('lacto_stresstools_ibu')
//           ->fields([
//             'sso_id' => $user['id_sso_user'],
//             'name' => $user['name'],
//             'email' => $user['email'],
//             'saved_at' => date('Y-m-d h:i:s', strtotime('now')),
//           ])
//           ->execute();
//         }

//         $session->remove('redirect_stress_test');
//         header("Location: " . $GLOBALS["base_url"] . "/stress-test/quiz/result");
//         die();
//       }

//       if ($session->get('clubredeem')) {
//         $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/club-redeem?s=' . $session->get('clubredeem');
//         $session->set('clubredeem', null);
//       } else {
//         $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path();
//       }
//       // dd($login['code']);
//       // dd($redirectto);
//       // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//       $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//       $response->send(); // don't send the response yourself inside controller and form.
//       return;
//     } else {
//     }
//   } else {

//     $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/login?failed=5';
//     return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//   }
try {
    if (isset($_POST['username'], $_POST['password'])) {
        $options = [
            'form_params' => [
                'email' => $_POST['username'],
                'password' => $_POST['password'],
            ]
        ];

        $client = new \GuzzleHttp\Client([
            'base_uri' => __API_BASE__,
            'verify' => false,
        ]);

        $response = $client->post("api/web/auth/login", $options);
        $getResponse = $response->getBody();
        $result = json_decode($getResponse, true);
        if($result['success'] == true && isset($result['member'])){

            $_SESSION['guestlogin'] = $result['member'];
            $_SESSION['timestamp'] = time();

            session_regenerate_id();
            if ($_SESSION['guestlogin']['profile_completed'] == 1) {
                if (isset($_SESSION['set'])) {
                    if ($_SESSION['set'] == 'lap') {
                        $path = '/guest/campaign/kelas-zumba-submit';
                    }elseif($_SESSION['set'] == 'loyal') {
                        $path = '/guest/campaign/kelas-zumba';
                    }elseif($_SESSION['set'] == 'digiclass') {
                        $path = '/guest/campaign/little-digiclass';
                    }
                }else{
                    $path = 'guest/profile';
                }
            }else{
                // $path = 'guest/complete-form';
                $path = 'guest/profile';
            }
        }else{
            if($result['errors'] == 'sso:user_not_enabled'){
                $path = 'login?failed=2';
            }elseif($result['errors'] == 'ghc:member_inactive'){
                $path = 'login?failed=3';
            }else{
                $path = 'login?failed=4';
            }
        }
    }else{
        $path = 'login?failed=5';
    }
} catch (\Exception $e) {
    $path = 'login?failed=1';
}

$url = $this->base_url($path);
return new TrustedRedirectResponse($url);
}

function growhappytheme_preprocess_page__guestlogoutfront(&$variables)
{

  $session = new Session();
  if ($session->get('guestlogin')) {

    $session->set('guestlogin', null);

    // Stress tools remove session
    $session->remove('quiz');
    $session->remove('anak_new');
    $session->remove('anak_add');
    $session->remove('anak_active');

    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path();
    // dd($redirectto);

    $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
    $response->send(); // don't send the response yourself inside controller and form.
    return;
  } else {
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path();
    // dd($redirectto);

    $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
    $response->send();
    return;
  }
}


function growhappytheme_preprocess_page__join(&$variables)
{
  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/pemeliharaan-sistem';
  $response = new RedirectResponse($redirectto);
  $response->send();
  return;

  $base_url = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__;

  // $query = \Drupal::database()->select($this->provinsi, 'provinsi');
  // $query->fields('provinsi', ['id', 'name']);
  // $provinsi = $query->execute()->fetchAll();

  if (isset($_GET['form'])) {
    $form = $_GET['form'];
  } else {
    $form = '';
  }

  $mergeData = (object) array(
    'form' => $form,
    'provinsi' => ""
  );
  //print_r($_SESSION['error_regis']);exit;
  //set variable for view

  $element = array(
    '#theme' => 'register',
    '#data' => $mergeData,
    '#base_url' => $base_url,
  );

  return $element;
}

function countAge($dob)
{
  $ts1 = strtotime($dob);

  $year1 = date('Y', $ts1);
  $year2 = date('Y');

  $month1 = date('m', $ts1);
  $month2 = date('m');

  $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

  return $diff;
}

function growhappytheme_preprocess_page__register_process(&$variables)
{
//   $session = new Session();
//   // dd("lacto");
//   $base_url = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__;


//   $resolve = [
//     'curl' => [
//       CURLOPT_RESOLVE => ['rewards.sahabatnestle.co.id:443:45.60.27.163']
//     ]
//   ];

//   // $name = $this->split_name($_POST['name']);

//   if (isset($_POST['name'], $_POST['dob_member'], $_POST['mobile_number'], $_POST['email'], $_POST['plain_password'])) {

//     $age = countAge($_POST['dob_member']);

//     // dd($age);

//     if ($age < 156) {
//       // $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/join?form=5';
//       // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//       $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/join?form=5';
//       $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//       $response->send(); // don't send the response yourself inside controller and form.
//       return;
//     }

//     $num = strlen($_POST['mobile_number']);
//     if ($num < 11) {
//       // $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/join?form=7';
//       // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//       $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/join?form=7';
//       $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//       $response->send(); // don't send the response yourself inside controller and form.
//       return;
//     }

//     $re = "(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,50}$)";
//     $passOne = $_POST['plain_password'];
//     if (!preg_match($re, $passOne)) {
//       // $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/join?form=6';
//       // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//       $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/join?form=6';
//       $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//       $response->send(); // don't send the response yourself inside controller and form.
//       return;
//     }

//     try {

//       $client = new Client(["base_uri" => "https://rewards.sahabatnestle.co.id/apps/sso/sso/token"]);

//       $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=14_2uw5bdjbf6eck408w8wkk8k08g884000ck4scc884kogcc8gs0&client_secret=2jc2nsgrlt0kk88k8k0gokgggkocgcgos8ws0880w0gkwk0g8k", $resolve);
//       $getToken = $tokenUrl->getBody();
//       $token = json_decode($getToken, true);
//     } catch (\Exception $e) {

//       $client = new Client(["base_uri" => "https://www.rewards.sahabatnestle.co.id/apps/sso/sso/token"]);

//       $tokenUrl = $client->get("https://www.rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=14_2uw5bdjbf6eck408w8wkk8k08g884000ck4scc884kogcc8gs0&client_secret=2jc2nsgrlt0kk88k8k0gokgggkocgcgos8ws0880w0gkwk0g8k", $resolve);
//       $getToken = $tokenUrl->getBody();
//       $token = json_decode($getToken, true);
//     }

//     // dd($token);

//     // print_r($token);
//     // exit;

//     if (!isset($token)) {
//       // $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/join?form=3';
//       // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//       $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/join?form=3';
//       $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//       $response->send(); // don't send the response yourself inside controller and form.
//       return;
//     }

//     $dataPost = http_build_query(array(
//       'name' => $_POST['name'],
//       'email' => $_POST['email'],
//       'mobile_number' => $_POST['mobile_number'],
//       'gender' => 'f',
//       'plain_password' => $_POST['plain_password'],
//       'consent' => 1,
//       'dob_member' => $_POST['dob_member'],
//       'id_city' => 123,
//       'city_name' => '-',
//       'phone_number_2' => $_POST['mobile_number'],
//       'child_name_1' => '-',
//       'dob_child_1' => '1970-01-01',
//       'child_name_2' => '-',
//       'dob_child_2' => '1970-01-01',
//       'child_name_3' => '-',
//       'dob_child_3' => '1970-01-01',
//       'child_name_4' => '-',
//       'dob_child_4' => '1970-01-01',
//       'child_name_5' => '-',
//       'dob_child_5' => '1970-01-01',
//       'is_send_email' => 0
//     ));

//     $ch = curl_init();
//     curl_setopt($ch, CURLOPT_URL, 'https://rewards.sahabatnestle.co.id/apps/sso/api/v2/sso/user/userl12bulk');

//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//     curl_setopt($ch, CURLOPT_POST, 1);
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPost);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//     curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
//     curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2);
//     curl_setopt($ch, CURLOPT_HTTPHEADER, [
//       sprintf('Authorization: Bearer %s', $token['access_token'])
//     ]);
//     $result = curl_exec($ch);
//     $regist = json_decode($result, true);

//     // dd($regist);

//     if (!isset($regist)) {
//       $ch = curl_init();
//       curl_setopt($ch, CURLOPT_URL, 'https://www.rewards.sahabatnestle.co.id/apps/sso/api/v2/sso/user/userl12bulk');

//       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//       curl_setopt($ch, CURLOPT_POST, 1);
//       curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPost);
//       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//       curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
//       curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2);
//       curl_setopt($ch, CURLOPT_HTTPHEADER, [
//         sprintf('Authorization: Bearer %s', $token['access_token'])
//       ]);
//       $result = curl_exec($ch);
//       $regist = json_decode($result, true);
//     }

//     // echo "<pre>";
//     // print_r($regist);
//     // exit;

//     if (isset($_GET['refferal'])) {
//       $reff = $_GET['refferal'];
//     } else {
//       $reff = '';
//     }

//     if ($regist['code'] == 200) {

//       $urlSendEmail = "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/services/send-email";

//       $template_id = 554;
//       $param_for_template = array(
//         'FIRSTNAME' => $_POST['name'],
//         'ACTIVATION_LINK' => $regist['data_level_1']['activation_link'],
//       );

//       $client_tr = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/"]);
//       $options_tr = [
//         'headers' => [
//           "Accept" => 'application/json'
//         ],
//         'json' => [
//           "name" => '',
//           "email" => $_POST['email'],
//           "template_id" => $template_id,
//           "parameters" => $param_for_template
//         ]
//       ];
//       $response_tr = $client_tr->post($urlSendEmail, $options_tr);
//       $getResponse_tr = $response_tr->getBody();
//       $exchange_tr = json_decode($getResponse_tr, true);

//       // Redirect ke stress tools
//       if ($session->get('redirect_stress_test')) {
//         $session->remove('redirect_stress_test');
//         header("Location: " . $GLOBALS["base_url"] . "/stress-test/quiz/result");
//         die();
//       }

//       $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/register-success';
//       $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//       $response->send(); // don't send the response yourself inside controller and form.
//       return;
//     } else {
//       if ($regist['enabled'] == 0) {

//         $urlSendEmail = "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/services/send-email";

//         $template_id = 554;
//         $param_for_template = array(
//           'FIRSTNAME' => $_POST['name'],
//           'ACTIVATION_LINK' => $regist['activation_link'],
//         );

//         $client_tr = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/"]);
//         $options_tr = [
//           'headers' => [
//             "Accept" => 'application/json'
//           ],
//           'json' => [
//             "name" => '',
//             "email" => $_POST['email'],
//             "template_id" => $template_id,
//             "parameters" => $param_for_template
//           ]
//         ];
//         $response_tr = $client_tr->post($urlSendEmail, $options_tr);
//         $getResponse_tr = $response_tr->getBody();
//         $exchange_tr = json_decode($getResponse_tr, true);

//         // Redirect ke stress tools
//         if ($session->get('redirect_stress_test')) {
//           $session->remove('redirect_stress_test');
//           header("Location: " . $GLOBALS["base_url"] . "/stress-test/quiz/result");
//           die();
//         }

//         // $redirectto = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/register-success?form=10';
//         $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/register-success';
//         $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//         $response->send(); // don't send the response yourself inside controller and form.
//         return;
//         // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//       } else if ($regist['enabled'] == 1) {
//         $dataPost = http_build_query(array(
//           'email' => $_POST['email'],
//           'is_send_email' => 0
//         ));


//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_URL, 'https://rewards.sahabatnestle.co.id/apps/sso/api/v2/sso/user/forgotpassword');

//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//         curl_setopt($ch, CURLOPT_POST, 1);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPost);
//         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//         curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
//         curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, [
//           sprintf('Authorization: Bearer %s', $token['access_token'])
//         ]);
//         $result = curl_exec($ch);
//         $reset = json_decode($result, true);

//         if (!isset($reset)) {
//           $ch = curl_init();
//           curl_setopt($ch, CURLOPT_URL, 'https://www.rewards.sahabatnestle.co.id/apps/sso/api/v2/sso/user/forgotpassword');

//           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//           curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//           curl_setopt($ch, CURLOPT_POST, 1);
//           curl_setopt($ch, CURLOPT_POSTFIELDS, $dataPost);
//           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//           curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//           curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
//           curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2);
//           curl_setopt($ch, CURLOPT_HTTPHEADER, [
//             sprintf('Authorization: Bearer %s', $token['access_token'])
//           ]);
//           $result = curl_exec($ch);
//           $reset = json_decode($result, true);
//         }

//         // echo "<pre>";
//         // print_r($reset);
//         // exit;

//         if ($reset['code'] == 200) {

//           // $query = \Drupal::database()->update($this->member);
//           // $query->fields([
//           //     'sso_confirmation_token' => $reset['confirmation_token'],
//           // ]);
//           // $query->condition('sso_uid', $reset['id_user_sso']);
//           // $query->execute();

//           // echo "<pre>";
//           // print_r($reset);
//           // exit;

//           $urlSendEmail = "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/services/send-email";

//           // $link = "https://www.growhappy.co.id/ reset-password-confirmation?token=".$reset['confirmation_token'];
//           $link = "https://www.growhappy.co.id/reset-password-confirmation?token=" . $reset['confirmation_token'] . "&email=" . $_POST['email'];

//           $template_id = 561;
//           $param_for_template = array(
//             'LINK' => $link,
//           );

//           $client_tr = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/"]);
//           $options_tr = [
//             'headers' => [
//               "Accept" => 'application/json'
//             ],
//             'json' => [
//               "name" => '',
//               "email" => $_POST['email'],
//               "template_id" => $template_id,
//               "parameters" => $param_for_template
//             ]
//           ];
//           $response_tr = $client_tr->post($urlSendEmail, $options_tr);
//           $getResponse_tr = $response_tr->getBody();
//           $exchange_tr = json_decode($getResponse_tr, true);
//         }

//         // Redirect ke stress tools
//         if ($session->get('redirect_stress_test')) {
//           $session->remove('redirect_stress_test');
//           header("Location: " . $GLOBALS["base_url"] . "/stress-test/quiz/result");
//           die();
//         }

//         // $redirectto = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/register-success?form=11';
//         $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/register-success';
//         $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//         $response->send(); // don't send the response yourself inside controller and form.
//         return;
//       } else {
//         // $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/join?form=4';
//         // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//         $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/join?form=4';
//         $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//         $response->send(); // don't send the response yourself inside controller and form.
//         return;
//       }
//     }
//   } else {

//     dd($_POST['name'] . $_POST['dob_member'] . $_POST['mobile_number'] . $_POST['email'] . $_POST['plain_password']);
//     // $redirectto = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . '/join?form=0';
//     // return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectto);
//     $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/join?form=0';
//     $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
//     $response->send(); // don't send the response yourself inside controller and form.
//     return;
//   }
            try {
				if (isset($_POST['name'], $_POST['dob_member'], $_POST['mobile_number'], $_POST['email'], $_POST['plain_password'], $_POST['terms'], $_POST['news_letter'])) {
					$options = [
						'form_params' => [
							'email' => $_POST['email'],
							'name' => $_POST['name'],
							'phone' => $_POST['mobile_number'],
							'dob' => $_POST['dob_member'],
							'password' => $_POST['plain_password'],
						]
					];

					$client = new \GuzzleHttp\Client([
						'base_uri' => __API_BASE__,
						'verify' => false,
					]);

					$response = $client->post("api/web/auth/register", $options);
					$getResponse = $response->getBody();
					$result = json_decode($getResponse, true);
					if($result['success'] == true){
						$path = 'join-success';
					}else{
						if(in_array($result['errors'], ['sso:email_exists','email_exists','phone_exists'])){
							$path = 'join?form=2';
						}elseif($result['errors']['password']){
							$path = 'join?form=6';
						}elseif($result['errors']['phone']){
							$path = 'join?form=7';
						}elseif($result['errors']['dob']){
							$path = 'join?form=5';
						}else{
							$path = 'join?form=4';
						}
					}
				}else{
					$path = 'join?form=0';
				}
			} catch (\Exception $e) {
				$path = 'join?form=3';
			}

			//$url = $GLOBALS["base_url"].$path;
			$redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
         $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
         $response->send(); // don't send the response yourself inside controller and form.
         return;
}

function growhappytheme_preprocess_page__club(&$variables)
{

  $client = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards"]);

  $tokenUrl = $client->get("https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards");
  // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=20_5tucsud0tlay95iu87m9m7odb4s38xak1y3j5bb26m9yafl4br&client_secret=048kyvt31l4so71o752o3gs5beysoyytoq4d9tn0mevxj87f44");
  $getReward = $tokenUrl->getBody();
  $rewards = json_decode($getReward, true);

  $image = "https://www.rewards.sahabatnestle.co.id/images/rewards/";

  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $variables['rewards'] = $rewards;
  $variables['rewardsimage'] = $image;

  $product_3 = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('title', '%' . db_like('LACTOGROW 3') . '%', 'LIKE');
  $entity_ids = $query->execute();
  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);

  foreach ($products as $prod) {
    $fusepump_id = $prod->get('field_dsu_sku')->getValue();
    $image = $prod->get('field_image')->getValue();
    $image_file = [];
    foreach ($image as $i) {
      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
    }

    $product_3[] = [
      'image' => $image_file[1],
      'fusepump_id' => $fusepump_id[0]['value'],
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }

  $variables['produk_3'] = $product_3;

  $product_4 = array();
  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->condition('title', '%' . db_like('LACTOGROW 4') . '%', 'LIKE');
  $entity_ids = $query->execute();
  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);

  foreach ($products as $prod) {
    $fusepump_id = $prod->get('field_dsu_sku')->getValue();
    $image = $prod->get('field_image')->getValue();
    $image_file = [];
    foreach ($image as $i) {
      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
    }

    $product_4[] = [
      'image' => $image_file[1],
      'fusepump_id' => $fusepump_id[0]['value'],
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }

  $variables['produk_4'] = $product_4;
}

function growhappytheme_preprocess_page__reward_detail(&$variables)
{




  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
  }

  $slug = $_GET['s'];

  $client = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards"]);

  $tokenUrl = $client->get("https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards" . "/" . $slug);
  // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=20_5tucsud0tlay95iu87m9m7odb4s38xak1y3j5bb26m9yafl4br&client_secret=048kyvt31l4so71o752o3gs5beysoyytoq4d9tn0mevxj87f44");
  $getReward = $tokenUrl->getBody();
  $rewards = json_decode($getReward, true);

  $client = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards"]);

  $tokenUrl = $client->get("https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards");
  // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=20_5tucsud0tlay95iu87m9m7odb4s38xak1y3j5bb26m9yafl4br&client_secret=048kyvt31l4so71o752o3gs5beysoyytoq4d9tn0mevxj87f44");
  $getRewardlisting = $tokenUrl->getBody();
  $rewardslisting = json_decode($getRewardlisting, true);

  // dd($rewards);

  $image = "https://www.rewards.sahabatnestle.co.id/images/rewards/";

  $variables['rewards'] = $rewards;
  $variables['rewardslisting'] = $rewardslisting;
  $variables['rewardsimage'] = $image;
}

function growhappytheme_preprocess_page__profile(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
    $variables['user_lacto'] = $variables['guestlogin']['level1'];
    $client = new Client([
      "base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/"
    ]);

    $response = $client->get("api/web/retails");
    $retails = $response->getBody();
    $variables['retails'] = json_decode($retails, true);
    // dd($variables['user_lacto']);


    $request = $_POST;
    if (is_array($request) && isset($request['mobile_number'], $request['email'])) {
      $client = new Client([
        "base_uri" => "https://rewards.sahabatnestle.co.id/apps/sso/"
      ]);

      $res_tokens = $client->request('GET', 'sso/token', [
        'query' => [
          'grant_type' => 'client_credentials',

          // DEV, comment kalau udah pake yg dari settings.php
          'client_id' => '14_2uw5bdjbf6eck408w8wkk8k08g884000ck4scc884kogcc8gs0',
          'client_secret' => '2jc2nsgrlt0kk88k8k0gokgggkocgcgos8ws0880w0gkwk0g8k',

          // dari settings.php, uncomment kalau udah dilive
          // 'client_id' => __SSO_CLIENT_ID__,
          // 'client_secret' => __SSO_SECRET__,
        ]
      ]);
      $token = $res_tokens->getBody();
      $token = json_decode($token, true);

      /* DATA SSO lvl1 */
      $user_lacto = $variables['guestlogin']['level1'];
      /* Nanti diup lagi soalnya data SSO ga adata First, Middle, Last name */
      $request["name"] = $user_lacto['name'];
      $request["gender"] = $user_lacto['gender'];

      $response = $client->request('POST', 'api/v2/sso/user/userl1update', [
        'form_params' => $request,
        'headers' => [
          'Authorization' => 'Bearer ' . $token['access_token']
        ]
      ]);

      $result = $response->getBody();
      $result = json_decode($result, true);
      /* $result */
      if ($result['code'] != 200) {
        dd(false);
      }
    }
  } else {
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
    $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
    $response->send(); // don't send the response yourself inside controller and form.
    return;
  }
}

function growhappytheme_preprocess_page__profile_edit(&$variables)
{
  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/pemeliharaan-sistem';
  $response = new RedirectResponse($redirectto);
  $response->send();
  return;

  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;
    $variables['user_lacto'] = $variables['guestlogin']['level1'];

    $client = new Client([
      "base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/"
    ]);

    $response = $client->get("api/web/retails");
    $retails = $response->getBody();
    $variables['retails'] = json_decode($retails, true);
    // dd($variables['user_lacto']);
  } else {
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
    $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
    $response->send(); // don't send the response yourself inside controller and form.
    return;
  }
}

function growhappytheme_preprocess_page__club_redeem(&$variables)
{
  $session = new Session();
  if ($session->get('guestlogin')) {
    $exchange = $session->get('guestlogin');
    $variables['guestlogin'] = $exchange;



    $slug = $_GET['s'];


    $client = new Client(["base_uri" => "https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards"]);

    $tokenUrl = $client->get("https://loyalty.app.master-7rqtwti-mdsspx7hgy47g.au.platformsh.site/api/web/rewards" . "/" . $slug);
    // $tokenUrl = $client->get("https://rewards.sahabatnestle.co.id/apps/sso/sso/token?grant_type=client_credentials&client_id=20_5tucsud0tlay95iu87m9m7odb4s38xak1y3j5bb26m9yafl4br&client_secret=048kyvt31l4so71o752o3gs5beysoyytoq4d9tn0mevxj87f44");
    $getReward = $tokenUrl->getBody();
    $rewards = json_decode($getReward, true);

    $variables['rewards'] = $rewards;

    $image = "https://www.rewards.sahabatnestle.co.id/images/rewards/";

    $variables['rewardsimage'] = $image;
  } else {
    $session->set('clubredeem', $_GET['s']);
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
    $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
    $response->send(); // don't send the response yourself inside controller and form.
    return;
  }
}





/* STRESS TEST TOOLS */
function growhappytheme_preprocess_page__stress_test__profile(&$variables)
{
  $session = new Session();

  // $user = $session->get('guestlogin');
  // if (!$user) {
  //   header("Location: " . $GLOBALS["base_url"] . "/login");
  //   die();
  // }

  $listanak = \Drupal::database()->select('lacto_stresstools_anak', 'anak')
    ->fields('anak')
    ->execute()
    ->fetchAll();

  // dd($listanak);

  $variables['listanak'] = $listanak;
}

function growhappytheme_preprocess_page__stress_test__profile__process(&$variables)
{
  $session = new Session();

  $request = Request::createFromGlobals();
  $post = $request->request->all();

  dd($post);

  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/stress-test/quiz';
  $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
  $response->send(); // don't send the response yourself inside controller and form.
  return;
}

function growhappytheme_preprocess_page__stress_test__profile__new__process(&$variables)
{
  $session = new Session();

  $request = Request::createFromGlobals();
  $post = $request->request->all();

  $post['umur'] = str_replace('tahun', '', $post['age_options']);
  $post['created_at'] = date('Y-m-d h:i:s', strtotime('now'));

  unset($post['age_options']);
  $session->set('anak_new', $post);

  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/stress-test/quiz';
  $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
  $response->send(); // don't send the response yourself inside controller and form.
  return;
}

function growhappytheme_preprocess_page__stress_test__profile__add__process(&$variables)
{
  $session = new Session();

  $request = Request::createFromGlobals();
  $post = $request->request->all();

  $post['umur'] = str_replace('tahun', '', $post['age_options']);
  $post['created_at'] = date('Y-m-d h:i:s', strtotime('now'));

  unset($post['age_options']);
  $session->set('anak_add', $post);

  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/stress-test/quiz';
  $response = new Symfony\Component\HttpFoundation\RedirectResponse($redirectto);
  $response->send(); // don't send the response yourself inside controller and form.
  return;
}

function growhappytheme_preprocess_page__stress_test__profile__history(&$variables)
{

}

function growhappytheme_preprocess_page__stress_test__quiz(&$variables)
{
  $session = new Session();
  if (!$session->get('redirect_stress_test')) {
    $session->set('redirect_stress_test', true);
  }

  // $user = $session->get('guestlogin');
  // if (!$user) {
  //   header("Location: " . $GLOBALS["base_url"] . "/stress-test/profile/new");
  //   die();
  // }

  $anak = null;
  if ($session->get('anak_add')) {
    // Add Anak
    $anak = $session->get('anak_add');
  } else if ($session->get('anak_new')) {
    // New Anak
    $anak = $session->get('anak_new');
  }

  $query = \Drupal::entityQuery('node');
  $query->condition('type', 'stress_test_quiz');
  $query->condition('field_umur', $anak['umur']);
  $query->sort('created', 'ASC');
  $entity_ids = $query->execute();

  $my_quiz = \Drupal::entityTypeManager()
    ->getStorage('node')
    ->loadMultiple($entity_ids);

  $nid_quiz = [];
  $list_quiz = [];
  foreach ($my_quiz as $quiz) {
    $title = $quiz->getTitle();
    $deskripsi = $quiz->get('body')->getValue()[0];

    $source_animasi = [];
    $animations = $quiz->get('field_source_animasi')->getValue();
    foreach($animations as $animasi) {
      $source_animasi[] = [
        'path' => file_create_url(\Drupal\file\Entity\File::load($animasi['target_id'])->getFileUri()),
        'alt' => $animasi['description'] == '' ? 'Alternative Video '. $title : $animasi['description'],
      ];
    }

    $nid_quiz[] = $quiz->id();
    $list_quiz[] = [
      'title' => $title,
      'deskripsi' => $deskripsi['value'],
      'source_animasi' => $source_animasi,
    ];
  }

  if (!$list_quiz) {
    header("Location: " . $GLOBALS["base_url"] . "/stress-test");
    die();
  }

  $nid_quiz = json_encode($nid_quiz);

  $variables['nid_quiz'] = $nid_quiz;
  $variables['list_quiz'] = $list_quiz;
}

function growhappytheme_preprocess_page__stress_test__quiz__result(&$variables)
{
  $session = new Session();

  $quiz = $session->get('quiz');
  if(!$quiz) {
    header("Location: " . $GLOBALS["base_url"] . "/stress-test");
    die();
  }

  // Assign Data Quiz Ke Fields
  $fields = $quiz;
  $fields['history_at'] = date('Y-m-d h:i:s', strtotime('now'));

  $user = $session->get('guestlogin');
  if($user){
    $user = $user['level1'];
    $fields['sso_id'] = $user['id_sso_user'];

    $anak = null;
    if($session->get('anak_active')){
      $anak = $session->get('anak_active');
      $fields['anak_id'] = $anak['id'];
    } else {
      if ($session->get('anak_add')) {
        // Add Anak
        $anak = $session->get('anak_add');
        $session->remove('anak_add');
      } else if ($session->get('anak_new')) {
        // New Anak
        $anak = $session->get('anak_new');
        $session->remove('anak_new');
      }

      if(isset($anak['button'])) unset($anak['button']);
      $anak['ibu_id'] = $user['id_sso_user'];

      /* Kalau Butuh Condition ngecek anak yang sama  */
      $getAnak = \Drupal::database()->select('lacto_stresstools_anak', 'anak')
        ->condition('ibu_id', $user['id_sso_user'])
        ->condition('nama_anak', $anak['nama_anak']);

      $check_anak = $getAnak->countQuery()
        ->execute()
        ->fetchAssoc();
      if($check_anak['expression'] == 0){
        $stresstools_anak = \Drupal::database()->insert('lacto_stresstools_anak');
        $stresstools_anak = $stresstools_anak->fields($anak);
        $anak_id = $stresstools_anak->execute();

        // Ini Dapeting ID anak kalau udah di insert
        $fields['anak_id'] = $anak_id;
      } else {
        $getAnak = $getAnak->fields('anak')->execute()->fetchAssoc();
        $fields['anak_id'] = $getAnak['id'];
      }
      /* End Kalau Butuh Condition ngecek anak yang sama  */

    }
  }

  $stresstools_history = \Drupal::database()->insert('lacto_stresstools_history');
  $stresstools_history = $stresstools_history->fields($fields);
  $stresstools_history->execute();

  $fields['anak'] = \Drupal::database()->select('lacto_stresstools_anak', 'anak')
    ->fields('anak')
    ->condition('id', $fields['anak_id'])
    ->execute()
    ->fetchAssoc();

  // Data Dummy
  // $result = [
  //   'nama_anak' => 'dummy dari datatabase',
  //   'umur' => '1-3',
  //   'nilai' => 8,
  // ];

  $variables['result'] = $fields;

  $article = array();

  $query = \Drupal::entityQuery('node');
  $query->condition('status', 1);
  $query->range(0, 5);
  $query->condition('type', 'article');
  $query->sort('created', 'DESC');
  $entity_ids = $query->execute();
  $articles = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);
  foreach ($articles as $prod) {
    $title = $prod->getTitle();
    $body = $prod->get('body')->getValue();
    $image = $prod->get('field_hero_image')->getValue();
    $image_alt = $image[0]['alt'];
    if ($image) {
      $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
    } else {
      $image = $prod->get('field_image')->getValue();
      if ($image) {
        $image = file_create_url(\Drupal\file\Entity\File::load($image[0]['target_id'])->getFileUri());
      }
    }
    // $tags = \Drupal\taxonomy\Entity\Term::load($node->get('field_tags')->target_id)->getName();
    $body = str_replace("&nbsp;", " ", strip_tags($body[0]['value']));
    $article[] = [
      'title' => $title,
      'body' => $body,
      'image' => $image,
      'image_alt' => $image_alt,
      'rasa' => $rasa,
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $prod->id())
    ];
  }

  $variables['article'] = $article;
}
