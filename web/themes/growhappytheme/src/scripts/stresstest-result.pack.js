// import Swiper from 'swiper/bundle';

var pageInit = (function(){
    function showRewardsModal() {
      if (window.once_poin) {
        setTimeout(function(){
            $('#rewardsModal').modal('show')
        }, 500)

        setTimeout(function(){
            $('#rewardsModal').modal('hide')
        }, 5000)

        $('#rewardsModal').on('hidden.bs.modal', function (event) {
            $('#rewards-notification').slideDown(200);
        })
      }
    }

    function setScore(){
        var $result = $('#result'),
            $data_age = $('#result').data('age'),
            $indicator = $result.find('.indicator');

        var $elem = $('#score-bar');

        ($data_age === '1-3') ? $elem.attr('aria-valuemax', '24') : $elem.attr('aria-valuemax', '28');

        var $val_now = $elem.attr('aria-valuenow'),
            $val_max = $elem.attr('aria-valuemax');

        switch ($data_age) {
            case '1-3':
                ($val_now <= 10) ? $result.attr('data-normal', 'true') : $result.attr('data-normal', 'false')
                break;

            default:
                ($val_now <= 11) ? $result.attr('data-normal', 'true') : $result.attr('data-normal', 'false')
                break;
        }

        $elem.css({
            'width': (($val_now / $val_max) * 100) + '%'
        })

        $indicator.css({
            'left': (($val_now / $val_max) * 100) + '%'
        })
    }

    function setArticleSlider(){
        var stressTestSwiper = new Swiper('#stresstest-article', {
            // centeredSlides: true,
            slidesPerView: 1.15,
            spaceBetween: 16,
            slidesOffsetBefore: 15,
            // initialSlide: 2,
            loop: false,
            speed: 600,
            // autoplay: {
            //     delay: 6000
            // },
            // navigation: {
            //     nextEl: '.swiper-button-next',
            //     prevEl: '.swiper-button-prev',
            // },

            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },

            breakpoints: {
                768: {
                    centeredSlides: false,
                    slidesPerView: 2,
                    spaceBetween: 16,
                    initialSlide: 1,
                    slidesOffsetBefore: 0,
                },

                992: {
                    slidesPerView: 3,
                    slidesOffsetBefore: 0,
                },
            },
        });
    }

    function init(){
        showRewardsModal()
        setScore()
        setArticleSlider()
    }

    return {
        init: init
    };

})();

// var swiperStressArticles = new Swiper('.swiper-stresstest-articles', {
//     // centeredSlides: true,
//     slidesPerView: 1,
//     // initialSlide: 2,
//     loop: true,
//     // navigation: {
//     //   nextEl: '.swiper-button-next.article',
//     //   prevEl: '.swiper-button-prev.article',
//     // },
//     // breakpoints: {
//     //   768: {
//     //     slidesPerView: 3,
//     //     spaceBetween: 10,
//     //     loopAdditionalSlides: 3,
//     //   },
//     // },
//     // autoplay: {
//     //   delay: 7000
//     // }
// });


pageInit.init()
