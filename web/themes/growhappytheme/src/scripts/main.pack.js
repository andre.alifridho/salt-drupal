import $, { isEmptyObject } from 'jquery';
import 'popper.js';
import 'bootstrap';
// import 'scrollpos-styler';
// lazy img & bg
document.addEventListener("DOMContentLoaded", function() {
  var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
  var lazyBgs = [].slice.call(document.querySelectorAll("[data-bg]"));
  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.classList.remove("lazy");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });
    let lazyBgObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyBg = entry.target;
          lazyBg.style.backgroundImage = "url('" + lazyBg.dataset.bg + "')";
          lazyBgObserver.unobserve(lazyBg);
        }
      });
    });
    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
    lazyBgs.forEach(function(lazyBg) {
      lazyBgObserver.observe(lazyBg);
    });
  } else {
    lazyImages.forEach((image) => {
      document.addEventListener('scroll', () => {
        if  (image.pageYOffset == window.scrollY) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.classList.remove("lazy");
        }
      })
    });
    lazyBgs.forEach((image) => {
      document.addEventListener('scroll', () => {
        if  (image.pageYOffset == window.scrollY) {
          let lazyBg = entry.target;
          lazyBg.style.backgroundImage = "url('" + lazyBg.dataset.bg + "')";
        }
      })
    })
  }
});

window.$ = $;
// $('.carousel').on('slide.bs.carousel', function() {
//     $('.carousel').carousel('pause');
// })
import Swiper, { Navigation, Pagination, Autoplay }  from 'swiper';

Swiper.use([Navigation, Pagination]);


// hide login banner on page scroll
$(window).scroll(function(){
  var scroll = $(window).scrollTop();
  var offset = 5;

  if (scroll >= offset) {
      $(".container-login-register").addClass("is-hidden");
      $(".lct__faq--detail-inner_wrapper").removeClass("pt-5");
    } else {
      $(".lct__faq--detail-inner_wrapper").addClass("pt-5");
    $(".container-login-register").removeClass("is-hidden");
  }
});

$('.btn-profile-menu-toggle').on('click', function() {
  $('.profile-menu-wrapper').toggleClass('is-hidden')
});



var swiperArticles = new Swiper('.swiper-articles', {
    centeredSlides: true,
    initialSlide: 2,
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next.article',
      prevEl: '.swiper-button-prev.article',
    },
    loopAdditionalSlides: 1,
    breakpoints: {
      768: {
        slidesPerView: 3,
        spaceBetween: 10,
        loopAdditionalSlides: 3,
      },
    },
    autoplay: {
      delay: 7000
    }
});

var swiperBenefitPoints = new Swiper('.swiper-benefit-points', {
    slidesPerView: 1,
    loopAdditionalSlides: 1,
    speed: 300,
    loop: true,
    autoHeight: true,
    // pagination: {
    //   el: '.swiper-benefit-points-pagination',
    //   type: 'bullets',
    //   clickable: true,
    //   dynamicBullets: true
    // },

    breakpoints: {
      // when window width is >= 768px
      768: {
        pagination: false,
        slidesPerView: 3,
        slidesPerColumn: 6,
        loop: false,
        autoHeight: false,
        draggable: false
      }
    }
});

var swiperHowTo = new Swiper('.swiper-howto', {
    slidesPerView: 1,
    spaceBetween: 30,
    speed: 300,
    loop: false,
    autoHeight: true,
    pagination: {
      el: '.swiper-pagination.howto'
    },
    navigation: {
      nextEl: '.swiper-button-next.howto',
      prevEl: '.swiper-button-prev.howto',
    },
    breakpoints: {
    992: {
      slidesPerView: 3,
      spaceBetween: 15,
    }
  }
});

var swiperHadiah = new Swiper('.swiper-hadiah', {
    observer: true,
    observeParents: true,
    slidesPerView: 1,
    spaceBetween: 30,
    speed: 300,
    loop: false,
    autoHeight: true,
    pagination: {
      el: '.swiper-pagination.hadiah'
    },
    navigation: {
      nextEl: '.swiper-button-next.hadiah',
      prevEl: '.swiper-button-prev.hadiah',
    },
    breakpoints: {
      992: {
        slidesPerView: 3,
        spaceBetween: 15,
      }
    }
});


var swiperRewards1 = new Swiper('.swiper-rewards', {
  loop: true,
  pagination: {
    el: '.swiper-pagination.reward',
  },
  navigation: {
    nextEl: '.swiper-button-next.reward',
    prevEl: '.swiper-button-prev.reward',
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 10,
    },
    992: {
      slidesPerView: 5,
      spaceBetween: 10,
    }
  },
  autoplay: {
    delay: 7000
  }
});

var swiperRewards2 = new Swiper('.swiper-rewards-happy-parents', {
  loop: true,
  pagination: {
    el: '.swiper-pagination.reward-happy-parents',
  },
  navigation: {
    nextEl: '.swiper-button-next.reward-happy-parents',
    prevEl: '.swiper-button-prev.reward-happy-parents',
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 10,
    },
    992: {
      slidesPerView: 5,
      spaceBetween: 10,
    }
  },
  autoplay: {
    delay: 7000
  }
});

var swiperRewards3 = new Swiper('.swiper-rewards-happy-kids', {
  loop: true,
  pagination: {
    el: '.swiper-pagination.reward-happy-kids',
  },
  navigation: {
    nextEl: '.swiper-button-next.reward-happy-kids',
    prevEl: '.swiper-button-prev.reward-happy-kids',
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 10,
    },
    992: {
      slidesPerView: 5,
      spaceBetween: 10,
    }
  },
  autoplay: {
    delay: 7000
  }
});

$('a[data-toggle="pill"]').on('shown.bs.tab', function (event) {
  swiperRewards1.update();
  swiperRewards2.update();
  swiperRewards3.update();
});

var swiperQuiz = new Swiper('.swiper-quiz', {
  initialSlide: 1,
  loop: true,
  speed: 400,
  pagination: {
    el: '.swiper-pagination.quiz',
  },
  navigation: {
    nextEl: '.swiper-button-next.quiz',
    prevEl: '.swiper-button-prev.quiz',
  },
  autoplay: {
    delay: 5000,
  }
});

var carouselProduct = $('#slideProduct').carousel({
  interval: 5000,
  cycle: true
});

var handled=false;//global variable

carouselProduct.bind('slide.bs.carousel', function (e) {
    var current=$(e.target).find('.carousel-item.active');
    var indx=$(current).index();
    if((indx+2)>$('.carousel-indicators li').length)
        indx=-1
     if(!handled)
     {
        $('.carousel-indicators li').removeClass('active')
        $('.carousel-indicators li:nth-child('+(indx+2)+')').addClass('active');
     }
     else
     {
        handled=!handled;//if handled=true make it back to false to work normally.
     }
});

$(".carousel-indicators li").on('click',function(){
   //Click event for indicators
   $(this).addClass('active').siblings().removeClass('active');
   //remove siblings active class and add it to current clicked item
   handled=true; //set global variable to true to identify whether indicator changing was handled or not.
});

$('.lightbox-call-to-action').on('click', function (e) {
  e.preventDefault();
  var productId = $(this).data('product-id');
  var productName = $(this).data('data-product-name');
  var productRange = $(this).data('data-product-range');
  var productCategory = $(this).data('data-product-category');
  var productSubCategory = $(this).data('data-product-subcategory');
  var productVariant = $(this).data('data-product-variant');
  /*eslint-disable */
  var lightbox = new fusepump.lightbox.buynow(productId);
  /*eslint-enable */
  if (lightbox) {
      // push dataLayer
      dataLayer.push({
        'buyNowInformation': {
          'productBrand': 'Lactogrow',
          'productQuantity': '1',
          'buyNowClicked': 1,
          'productId': productId,
          'productName': productName,
          'productRange': productRange,
          'productCategory': productCategory,
          'productSubCategory': productSubCategory,
          'productVariant': productVariant
        }
      });
      lightbox.show(); // Show the lightbox
  }
  return false;
});


var swiperProductCards = new Swiper('.swiper-product-cards', {
  loop: false,
  slidesPerView: 1,
  speed: 400,
  autoplay: true,
  autoplay: {
    delay: 5000,
  },

  breakpoints: {
    // when window width is >= 768
    768: {
      pagination: false,
      slidesPerView: 2,
      loop: false,
      autoHeight: false,
      draggable: false,
      touchRatio: 0,
      autoplay: false,
      spaceBetween: 15,
    }
  },

  on: {
    init: function () {
      var swiperProducts = new Swiper('.swiper-products', {
        loop: false,
        centeredSlides: true,
        slidesPerView: 1,
        speed: 400,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        autoplay: {
          delay: 5000,
        },
        // effect: 'fade',
        // fadeEffect: {
        //   crossFade: true
        // },
        touchRatio: 0
      });
    },
  }
});




var swiperFaqCaraGabung = new Swiper('.lct__faq--detail-cara_gabung--swiper_container', {
  slidesPerView: 1,
  // spaceBetween: -50,
  initialSlide: 1,
  centeredSlides: true,
  loop: false,
  loopAdditionalSlides: 100,
  pagination: {
      el: '.lct__faq--detail-cara_gabung--swiper_pagination',
      dynamicBullets: true
  },
  navigation: {
    nextEl: '.lct__faq--detail-cara_gabung--swiper_button_next',
    prevEl: '.lct__faq--detail-cara_gabung--swiper_button_prev',
},
  breakpoints: {
      768: {
          // loop: true,
          slidesPerView: 2,
          mousewheel: true,
          allowTouchMove: true,
          draggable: true,
          slidesPerView: 3,
          spaceBetween: 0
      }
  }
});



$('.swiper-product-cards .swiper-slide').on('click', function() {
  swiperProductCards.slideTo($(this).index());
})



// ASI MODAL
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var dpc = getCookie("lactoasimodalalert");
  if (dpc != "lactoasimodalactive") {
    return false;
  } else {
    return true;
  }
}

// club-redeem modal
// $('#redeemModalNotif').modal('show');

// suppress modal & third party scripts after user scroll

$(window).scroll(function() {
  var scrollTopThreshold = 50;
  if(!checkCookie() && $(document).scrollTop() > scrollTopThreshold){
    $('#asiModal').modal('show');
    $('#asiModal .btn-primary').on('click', function () {
      setCookie('lactoasimodalalert', 'lactoasimodalactive', 30);
    })
  }
  // if ($(document).scrollTop() > scrollTopThreshold) {
  //   var fonts = '<link rel="stylesheet" href="'+window.location.origin+'/themes/growhappytheme/assets/styles/fonts.pack.css">';
  //   var defers = '<script defer src="'+window.location.origin+'/themes/growhappytheme/src/scripts/defer.pack.js"><\/script>';
  //   var fusepump = '<script defer src="https://brand-ecommerce-assets.fusepump.com/bootstraper/bootstraper.js"><\/script>';
  //   var bolt = '<script defer src="https://cdn-akamai.mookie1.com/LB/LightningBolt.js"><\/script>';
  //   $('#deferred').append(fonts).append(defers).append(fusepump).append(bolt);
  // }
})

// back to top
// $(window).scroll(function() {
//   var scrollTopThreshold = 500;
//   if ($(document).scrollTop() > scrollTopThreshold) {
//     $("#goToTopBtn").fadeIn();
//   } else {
//     $("#goToTopBtn").fadeOut();
//   }
// });
// $("#goToTopBtn").on('click', function(e) {
//   e.preventDefault();
//   $('html, body').animate({scrollTop:0}, '300');
// });


//check reward wrapper height on Club page on mobile viewport
if (($('.article-wrapper').height() > 900) && ($(window).width() < 768)) {
  console.log($('.article-wrapper').height());
  $('.article-wrapper').addClass('article-wrapper-snippet');
  $('.expand-article-toggle').removeClass('d-none');
  $('.pagination-section').addClass('d-none');

}


$('.expand-article-toggle-wrapper').on('click', '.expand-article-toggle', function() {
  $('.article-wrapper').removeClass('article-wrapper-snippet');
  $('.expand-article-toggle').addClass('d-none');
  $('.pagination-section').removeClass('d-none');

})
