<?php
/**
 * @param  mixed $variables
 * @return void
 * @author Edo SuryaUtama
 * @since 15 April 2021
 */
function growhappytheme_preprocess_page__success_activation(&$variables)
{
  $data = array();
  foreach ( $_GET as $key => $value ){
    $data[$key]=$value;
  }
  $variables['errorCode'] = $data['errorCode'];
  $variables['errorCode'] = (isset($data['errorCode'])) ? $data['errorCode'] : 0;

  switch($variables['errorCode']) {
    case "403002":
      $variables['messageTitle'] = 'Tautan Kadaluarsa';
      $variables['message'] = 'Tautan verifikasi sudah kadaluarsa. Silakan sign in ke akun Happy Parents, jadi kami dapat mengirimkan tautan verifikasi yang baru.';
      break;

    default:
      $variables['messageTitle'] = 'Aktivasi Akun Berhasil!';
      $variables['message'] = 'Selamat akun Happy Parents sudah berhasil diaktivasi dan Anda telah tergabung menjadi anggota Grow Happy Club.';
  }
}