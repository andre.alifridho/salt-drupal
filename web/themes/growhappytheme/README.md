# Growhappy Theme

Make sure you are in this directory,

`cd docroot/themes/growhappytheme`

## Setup

`npm i`

## Development

`npm start`

## Production

`npm run production`
