const mix               = require('laravel-mix')
const ImageminPlugin    = require('imagemin-webpack-plugin').default
const CopyWebpackPlugin = require('copy-webpack-plugin')
const imageminMozjpeg   = require('imagemin-mozjpeg')

function mix_scss_files(folder) {
	let fs = require('fs');
	var relative_path = "src/styles" + folder;
    var paths = fs.readdirSync(relative_path);

	for (var i = 0; i < paths.length; i++) {
		if (paths[i].indexOf('.pack.scss') > 0 && paths[i].charAt(0) != '_') {
            var file_path = relative_path + paths[i];

            console.log(file_path);

            mix
            .sass(file_path, 'assets/styles' + folder)
            .options({
                processCssUrls: false,
                autoprefixer: {
                    options: {
                        browsers: [
                            'last 10 versions',
                            'ie 10-11'
                        ]
                    }
                }
            })
		}
	}
}

function mix_js_files(folder) {
	let fs = require('fs');
	var relative_path = "src/scripts" + folder;
    var paths = fs.readdirSync(relative_path);

	for (var i = 0; i < paths.length; i++) {
		if (paths[i].indexOf('.pack.js') > 0) {
            var file_path = relative_path + paths[i];

            console.log(file_path);

			mix.js(file_path, 'assets/scripts' + folder);
		}
	}
}

// mix_scss_files('/global/')
mix_scss_files('/')
mix_js_files('/')

mix
.webpackConfig({
	externals: {
		$: 'jquery',
		Drupal: 'Drupal',
		drupalSettings: 'drupalSettings',
	},
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/i,
				use: [
					{
						loader: 'sass-loader',
						options: {
							sassOptions: {
								includePaths: [
									'node_modules'
								]
							},
						},
					},
				],
			},
		]
	}
})
.setPublicPath('assets')
.setResourceRoot('../')
.copyDirectory('src/images', 'assets/images')
.copyDirectory('src/videos', 'assets/videos')
.copyDirectory('src/fonts', 'assets/fonts')

.browserSync({
	target: 'http://lacto.lndo.site/docroot', // http://localhost/{{YOUR_MILO_LIGHTNEST_FOLDER}}/docroot
	publicPath: '/themes/growhappytheme',
	proxy: 'http://lacto.lndo.site',
	files: '../src/*',
	ghostMode: false,
	notify: true,
	open: false
})

// Only in production
if(mix.inProduction()) {
	mix.webpackConfig({
		plugins: [
			new CopyWebpackPlugin({
				patterns: [{
					from: 'src/images',
					to: 'images',
					toType: 'dir'
				}]
			}),
			new ImageminPlugin({
				test: /\.(jpe?g|png|gif|svg)$/i,
				pngquant: {
					quality: '50-60'
				},
				plugins: [
					imageminMozjpeg({
						quality: 50,
						maxMemory: 1000 * 512
					})
				]
			})
		],
	});
}
