<?php
	use GuzzleHttp\Client;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Session\Session;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\HttpFoundation\RedirectResponse;

	function growhappytheme_preprocess_page__club_redeem(&$variables)
	{
		$session = new Session();
		try {
			if ($session->get('guestlogin')) {
			    $member = (Object)$session->get('guestlogin');
			    $slug = $_GET['s'];
			    $reward_item_id = $_GET['i'];

			    $client = new \GuzzleHttp\Client([
		            'base_uri' => __API_BASE__,
		            'verify' => false,
		            'proxy' => __API_PROXY__,
		        ]);

		        $headers = [
					'headers' => [
						"X-Member-ID" => $member->id
					]
				];

				//member detail
		        $response_member = $client->get("api/web/member?key=".__API_KEY__, $headers);
		        $get_response_member = $response_member->getBody();
		        $get_response_member = json_decode($get_response_member, true);

		        //reward detail
		        $response_reward = $client->get("api/web/rewards/".$slug."?key=".__API_KEY__);
		       	$get_response_reward = $response_reward->getBody();
		       	$reward = json_decode($get_response_reward);

		       	//reward item
		       	$response_reward_item = $client->get("api/web/rewards/items/".$reward_item_id."?key=".__API_KEY__);
		       	$get_response_reward_item = $response_reward_item->getBody();
		       	$reward_item = json_decode($get_response_reward_item);

		       	//member address
		       	$response_address = $client->get("api/web/address?key=".__API_KEY__, $headers);
				$get_response_address = $response_address->getBody();
				$addresses = json_decode($get_response_address, true);

				//province list
				$response_provinsi = $client->get("api/web/area/provinsi?key=".__API_KEY__);
				$get_response_provinsi = $response_provinsi->getBody();
				$provinsi = json_decode($get_response_provinsi, true);

			    $image_path = \Drupal::request()->getSchemeAndHttpHost() . base_path()."images/rewards/";

			    $variables['point'] = $get_response_member['point'];
			    $variables['guestlogin'] = $member;
			    $variables['addresses'] = $addresses;
			    $variables['reward'] = $reward;
			    $variables['item'] = $reward_item;
			    $variables['provinsi'] = $provinsi;
			    $variables['rewardsimage'] = $image_path;

			  } else {
					$session->set('s', $_GET['s']);
			    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
			    $response = new RedirectResponse($redirectto);
			    $response->send(); // don't send the response yourself inside controller and form.
			    return;
			  }
		} catch (Exception $e) {
			$path = 'not-found';
			$redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
            $response = new RedirectResponse($redirectto);
            $response->send();
		}
	}
