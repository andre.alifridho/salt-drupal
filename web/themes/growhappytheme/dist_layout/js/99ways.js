jQuery(document).ready(function($){
	$("#nnways_keyword").on('keyup', function (e) {
	    if (e.keyCode === 13) {
	        var keyword = $(this).val();
	        var cate = $("#nnways_category").val();
	        $(location).attr('href','https://growhappy.co.id/99ways/article?view=all&srch=' + keyword + '&cate=' + cate);
	    }
	});

	$("#nnways_category").change(function() {
	    var cate = $(this).val();
	    var keyword = $("#nnways_keyword").val();
	    $(location).attr('href','https://growhappy.co.id/99ways/article?view=all&srch=' + keyword + '&cate=' + cate);
	});

	$('.nnways_previewImg').bind("click" , function () {
        $('#filefoto').click();
    });

    function readURL(input) {
    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#nnways_previewImg').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#filefoto").change(function(){
	    readURL(this);
	});
});

function showGalleryNinetyNineDetail(){
	$('#nnways_modal').modal();
}