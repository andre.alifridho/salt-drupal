<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Drupal\user\Entity\User;
use Gigya\PHP\GSRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

function growhappytheme_preprocess_page__resetprocess(&$variables)
{
  try {
    if (isset($_POST['email']) || $_POST['email'] != '') {
      $options = [
        'form_params' => [
          'email' => $_POST['email'],
        ]
      ];

      $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,

      ]);
      // $client = new \GuzzleHttp\Client([
      // 	'base_uri' => 'https://loyalty.app.test-t6dnbai-mdsspx7hgy47g.au.platformsh.site',
      // 	'verify' => false,
      // ]);

      $response = $client->post("api/web/auth/forgot-password", $options);
      $getResponse = $response->getBody();
      $result = json_decode($getResponse, true);
      if ($result['success'] == true) {
        $path = 'lupa-password?form=1';
      } elseif ($result['errors'] == 'sso:user_not_found') {
        $path = 'lupa-password?form=3';
      } elseif ($_POST['email'] == '') {
        $path = 'lupa-password?form=0';
      } else {
        $path = 'lupa-password?form=2';
      }
    } else {
      $path = 'lupa-password?form=0';
    }
  } catch (\Exception $e) {
    $path = 'lupa-password?form=4';
  }

  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
  $response = new RedirectResponse($redirectto);
  $response->send();
}

function growhappytheme_preprocess_page__loginprocess(&$variables)
{
  try {
    if (isset($_POST['email'], $_POST['password'])) {
      $options = [
        'form_params' => [
          'email' => $_POST['email'],
          'password' => $_POST['password'],
        ]
      ];

      $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,

      ]);

      $response = $client->post("api/web/auth/login", $options);
      $getResponse = $response->getBody();
      $result = json_decode($getResponse, true);
      if ($result['success'] == true && isset($result['member'])) {

        session_start();
        $session = new Session();
        $session->set('guestlogin', $result['member']);
        $_SESSION['timestamp'] = time();

        if ($session->get('guestlogin')['profile_completed'] == 1) {
          if (isset($_SESSION['set'])) {
            if ($_SESSION['set'] == 'lap') {
              $path = '/guest/campaign/kelas-zumba-submit';
            } elseif ($_SESSION['set'] == 'loyal') {
              $path = '/guest/campaign/kelas-zumba';
            } elseif ($_SESSION['set'] == 'digiclass') {
              $path = '/guest/campaign/little-digiclass';
            }
          } else {
            $path = 'profile';
          }
        } else {
          $path = 'profile-edit';
        }

        $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
      } else {
        if ($result['errors'] == 'sso:user_not_enabled') {
          $path = 'login?failed=2';
        } elseif ($result['errors'] == 'ghc:member_inactive') {
          $path = 'login?failed=3';
        } else {
          $path = 'login?failed=4';
        }
      }
    } else {
      $path = 'login?failed=5';
    }
  } catch (\Exception $e) {
    $path = 'login?failed=1';
  }

  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
  $response = new RedirectResponse($redirectto);
  $response->send();
}

function growhappytheme_preprocess_page__profile(&$variables)
{
  session_start();
  $session = new Session();

  $uid = null;
  // $account = \Drupal::currentUser();
  // if ($account->isAuthenticated() && !isset($_SESSION['user_sso_uid'])) {
  //   $userdata_drupal = getUserLacto();
  //   $uid = $userdata_drupal['uuid'][0]['value']; // parameter diganti
  //   // dd($uid);
  //   $userdata = getMe($uid);
  //   // dd($userdata);
  //
  //   $variables['user_ibu'] = $userdata['profile'];
  //   // $variables['user_anak'] = $userdata['data']['child'];
  //   // $_SESSION['user_uid'] = $uid;
  //   $variables['user_data'] = getGigyaIris($uid);
  //   // dd(getGigyaData($uid));
  //   // $session->set('guestlogin', getGigyaIris($uid));
  //   $_SESSION['timestamp'] = time();
  // }

  if ($session->get('guestlogin')) {
    try {
      $userdata_drupal = getUserLacto();
      $uid = $userdata_drupal['uuid'][0]['value']; // parameter diganti
      $userdata = getMe($uid);
      $variables['user_data'] = getGigyaIris($uid);

      $member = (object)$session->get('guestlogin');
      $variables['guestlogin'] = $member;

      $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,

      ]);

      $headers = [
        'headers' => [
          "X-Member-ID" => $member->id
        ]
      ];

      //retail list
      $response_retails = $client->get("api/web/retails?key=".__API_KEY__);
      $retails = $response_retails->getBody();
      $variables['retails'] = json_decode($retails, true);

      //reward list
      $response_rewards = $client->get("api/web/rewards?key=".__API_KEY__);
      $rewards = $response_rewards->getBody();
      $variables['rewards'] = json_decode($rewards, true);

      //transaction log
      $response_transaction_logs = $client->get("api/web/transactions?key=".__API_KEY__, $headers);
      $get_response_transaction_logs = $response_transaction_logs->getBody();
      $get_response_transaction_logs = json_decode($get_response_transaction_logs, true);
      $variables['transaction_logs'] = $get_response_transaction_logs['data'];

      //transaction redeem log
      $response_transaction_redeem_logs = $client->get("api/web/transactions?type=redeem&key=".__API_KEY__, $headers);
      $get_response_transaction_redeem_logs = $response_transaction_redeem_logs->getBody();
      $get_response_transaction_redeem_logs = json_decode($get_response_transaction_redeem_logs, true);
      $variables['transaction_redeem_logs'] = $get_response_transaction_redeem_logs['data'];

      //transaction submit log
      $response_transaction_submit_logs = $client->get("api/web/transactions?type=submit&key=".__API_KEY__, $headers);
      $get_response_transaction_submit_logs = $response_transaction_submit_logs->getBody();
      $get_response_transaction_submit_logs = json_decode($get_response_transaction_submit_logs, true);
      $variables['transaction_submit_logs'] = $get_response_transaction_submit_logs['data'];

      //member detail
      $response_member = $client->get("api/web/member?key=".__API_KEY__, $headers);
      $get_response_member = $response_member->getBody();
      $get_response_member = json_decode($get_response_member, true);
      $variables['point'] = $get_response_member['point'];

      $image_path = \Drupal::request()->getSchemeAndHttpHost() . base_path() . "images/rewards/";
      $variables['rewardsimage'] = $image_path;

    } catch (Exception $e) {
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . 'guestlogoutfront';
      $response = new RedirectResponse($redirectto);
      $response->send();
      return;
    }

  } else {
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . 'guestlogoutfront';
    $response = new RedirectResponse($redirectto);
    $response->send();
    return;
  }
}

function growhappytheme_preprocess_page__profile_edit(&$variables)
{
  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/pemeliharaan-sistem';
  $response = new RedirectResponse($redirectto);
  $response->send();
  return;

  session_start();
  $session = new Session();
  if ($session->get('guestlogin')) {
    $member = (object)$session->get('guestlogin');
    $variables['user_data'] = $member;
    $variables['guestlogin'] = $member;

    // $client = new \GuzzleHttp\Client([
    //   'base_uri' => __API_BASE__,
    //   'verify' => false,
    // ]);

    // $headers = [
    //   'headers' => [
    //     "X-Member-ID" => $member->id
    //   ]
    // ];

    // //retail list
    // $response = $client->get("api/web/retails");
    // $retails = $response->getBody();
    // $variables['retails'] = json_decode($retails, true);

    // //member detail
    // $response_member = $client->get("api/web/member", $headers);
    // $get_response_member = $response_member->getBody();
    // $get_response_member = json_decode($get_response_member, true);
    // $variables['childrens'] = $get_response_member['children'];
    // $variables['point'] = $get_response_member['point'];
  } else {
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
    $response = new RedirectResponse($redirectto);
    $response->send();
    return;
  }
}

function growhappytheme_preprocess_page__profile_referral(&$variables)
{
  session_start();
  $session = new Session();
  if ($session->get('guestlogin')) {
    $member = (object)$session->get('guestlogin');
    $variables['guestlogin'] = $member;

    $client = new \GuzzleHttp\Client([
      'base_uri' => __API_BASE__,
      'verify' => false,

    ]);

    $headers = [
      'headers' => [
        "X-Member-ID" => $member->id
      ]
    ];

    //member detail
    $response_member = $client->get("api/web/member", $headers);
    $get_response_member = $response_member->getBody();
    $get_response_member = json_decode($get_response_member, true);
    $variables['point'] = $get_response_member['point'];
  } else {
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
    $response = new RedirectResponse($redirectto);
    $response->send();
    return;
  }
}

function growhappytheme_preprocess_page__guestlogoutfront(&$variables)
{
  session_destroy();
  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path();
  $response = new RedirectResponse($redirectto);
  $response->send(); // don't send the response yourself inside controller and form.
  return;
}

function growhappytheme_preprocess_page__activation(&$variables)
{
  try {
    if (isset($_GET['token'])) {
      $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,

      ]);

      $response = $client->get("api/web/auth/register/confirm?token=" . $_GET['token']);
      $getResponse = $response->getBody();
      $result = json_decode($getResponse, true);
      if ($result['success'] == true || $result['errors'] == 'sso:already_enabled') {
        $result = '12';
      } else {
        if ($result['errors'] == 'sso:invalid_token' || $result['errors']['token']) {
          $result = '13';
        } else {
          $result = '10';
        }
      }
    } else {
      $result = '404';
    }
  } catch (\Exception $e) {
    $result = '14';
  }

  $variables['result'] = $result;
}

//ciam profile
function growhappytheme_preprocess_page__ciamprofile(&$variables)
{
  session_start();
  $session = new Session();

  $uid = null;
  // $account = \Drupal::currentUser();
  // if ($account->isAuthenticated() && !isset($_SESSION['user_sso_uid'])) {
  //   $userdata_drupal = getUserLacto();
  //   $uid = $userdata_drupal['uuid'][0]['value']; // parameter diganti
  //   // dd($uid);
  //   $userdata = getMe($uid);
  //   // dd($userdata);
  //
  //   $variables['user_ibu'] = $userdata['profile'];
  //   // $variables['user_anak'] = $userdata['data']['child'];
  //   // $_SESSION['user_uid'] = $uid;
  //   $variables['user_data'] = getGigyaIris($uid);
  //   // dd(getGigyaData($uid));
  //   // $session->set('guestlogin', getGigyaIris($uid));
  //   $_SESSION['timestamp'] = time();
  // }

  if ($session->get('guestlogin')) {

    try {

      $userdata_drupal = getUserLacto();
      $uid = $userdata_drupal['uuid'][0]['value']; // parameter diganti
      $userdata = getMe($uid);
      $variables['user_data'] = getGigyaIris($uid);

      $member = (object)$session->get('guestlogin');
      $variables['guestlogin'] = $member;

      $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,

      ]);

      $headers = [
        'headers' => [
          "X-Member-ID" => $member->id
        ]
      ];

      //retail list
      $response_retails = $client->get("api/web/retails");
      $retails = $response_retails->getBody();
      $variables['retails'] = json_decode($retails, true);

      //reward list
      $response_rewards = $client->get("api/web/rewards");
      $rewards = $response_rewards->getBody();
      $variables['rewards'] = json_decode($rewards, true);

      //transaction log
      $response_transaction_logs = $client->get("api/web/transactions", $headers);
      $get_response_transaction_logs = $response_transaction_logs->getBody();
      $get_response_transaction_logs = json_decode($get_response_transaction_logs, true);
      $variables['transaction_logs'] = $get_response_transaction_logs['data'];

      //transaction redeem log
      $response_transaction_redeem_logs = $client->get("api/web/transactions?type=redeem", $headers);
      $get_response_transaction_redeem_logs = $response_transaction_redeem_logs->getBody();
      $get_response_transaction_redeem_logs = json_decode($get_response_transaction_redeem_logs, true);
      $variables['transaction_redeem_logs'] = $get_response_transaction_redeem_logs['data'];

      //transaction submit log
      $response_transaction_submit_logs = $client->get("api/web/transactions?type=submit", $headers);
      $get_response_transaction_submit_logs = $response_transaction_submit_logs->getBody();
      $get_response_transaction_submit_logs = json_decode($get_response_transaction_submit_logs, true);
      $variables['transaction_submit_logs'] = $get_response_transaction_submit_logs['data'];

      //member detail
      $response_member = $client->get("api/web/member", $headers);
      $get_response_member = $response_member->getBody();
      $get_response_member = json_decode($get_response_member, true);
      $variables['point'] = $get_response_member['point'];

      $image_path = \Drupal::request()->getSchemeAndHttpHost() . base_path() . "images/rewards/";
      $variables['rewardsimage'] = $image_path;

    } catch (Exception $e) {
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/guestlogoutfront';
      $response = new RedirectResponse($redirectto);
      $response->send();
      return;
    }

  } else {
    $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
    $response = new RedirectResponse($redirectto);
    $response->send();
    return;
  }
}

function growhappytheme_preprocess_page__ciam_auth_process(&$variables)
{
  $user = getUserLacto();
  if($user){
    $uid = $user['uuid'][0]['value'];
    $member_ciam = getGigyaIris($uid);

    if($member_ciam){
      $client = new \GuzzleHttp\Client([
          'base_uri' => __API_BASE__,
          'verify' => false,
      ]);

      $options = [
        'json' => $member_ciam,
      ];

      $endpoint = "api/web/auth?key=".__API_KEY__;

      try {
        $response = $client->post($endpoint, $options);
        $getResponse = $response->getBody();
        $result = json_decode($getResponse, true);

        //api log
        $log  = "[".date("Y-m-d H:i:s")."] - ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                "Success: `POST ".__API_BASE__.'/'.$endpoint."` resulted in a `".$response->getStatusCode()." Success`".PHP_EOL;

        session_start();
        $session = new Session();
        $session->set('guestlogin', $result);

        if ($session->get('guestlogin')) {

          $member = (object)$session->get('guestlogin');
          $variables['guestlogin'] = $member;

       //    $endpointEn = "api/web/campaign/encryptMemberCode";
       //    $optionsEn = [
    			// 	'form_params' => [
       //        "member_code" => $member->identity
    			// 	]
    			// ];
       //    $responseEn = $client->post($endpointEn, $optionsEn);
       //    $getResponseEn = $responseEn->getBody();
       //    $resultEn = json_decode($getResponseEn, true);

     } else {
       $log_member  = "[".date("Y-m-d H:i:s")."] - ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
               "Member Data: ".$member_ciam.PHP_EOL;

       //append to log file
       file_put_contents('./sites/default/files/private/logs/log_member_'.date("Y-m-d").'.log', $log_member, FILE_APPEND);
     }

        if (isset($_SESSION['set'])) {
          if ($_SESSION['set'] == 'lap') {
            $variables['path']= '/guest/campaign/kelas-zumba-submit';
          } elseif ($_SESSION['set'] == 'loyal') {
            $variables['path']= '/guest/campaign/kelas-zumba';
          } elseif ($_SESSION['set'] == 'digiclass') {
            $variables['path']= '/guest/campaign/little-digiclass';
          } elseif ($_SESSION['set'] == 'subscription') {
            $variables['subs'] = 1;
            $variables['path'] = 'https://growhappysubscription.com?member_code='.$resultEn['data'];
          }
        } elseif ($session->get('s')) {
          $variables['path']= 'reward-detail?s='.$session->get('s');
        } else {
          $variables['path'] = 'profile';
        }

      } catch (RequestException | GuzzleHttp\Exception\ClientException $e) {

        $response = $e->getResponse();
        $get_response = (string) $response->getBody();

        //api log
        $log  = "[".date("Y-m-d H:i:s")."] - ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                "Error: ".$e."Data: ".$options.PHP_EOL;

        $variables['result'] = json_decode($get_response, true);

        echo '<script language="javascript">';
        echo 'console.log("auth fail")';
        echo '</script>';
        $variables['path'] = 'login-failed';
      } catch (Exception $e) {

        $response = $e->getResponse();
        $get_response = (string) $response->getBody();

        //api log
        $log  = "[".date("Y-m-d H:i:s")."] - ".$_SERVER['REMOTE_ADDR'].PHP_EOL.
                "Error: ".$e.PHP_EOL;

        $variables['result'] = json_decode($get_response, true);

        echo '<script language="javascript">';
        echo 'console.log("auth fail")';
        echo '</script>';
        $variables['path'] = 'login-failed';
      }
    }

    //append to log file
    file_put_contents('./sites/default/files/private/logs/log_api_'.date("Y-m-d").'.log', $log, FILE_APPEND);

  } else {
    $variables['path'] = 'login-failed';
  }

}

function growhappytheme_preprocess_page__validasi(&$variables){
    try {
      $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,
      ]);

      $response = $client->get("api/web/verify-phone?key=".__API_KEY__."&phone=".$_GET['phone']."&signature=".$_GET['signature']);
      $get_response = $response->getBody();
    } catch (\GuzzleHttp\Exception\ClientException $e) {
      $response = $e->getResponse();
      $get_response = (string) $response->getBody();
    }

    $variables['result'] = json_decode($get_response);
  }
