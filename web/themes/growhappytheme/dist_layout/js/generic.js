jQuery(document).ready(function($){
    //Navbar menu
    $(".collapse.navbar-collapse").on("hide.bs.collapse", function() {
        $(".navbar-toggle").html('<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>');

        $("#header-profile-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-profile.png">');

        $("#header-search-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-search.png">');

        $(".navbar-brand").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/lacto-brand-white.png">');

        $("#affix").removeClass('navbar-extend');
        $("#search-bar").removeClass('navbar-search');
    });

    $(".collapse.navbar-collapse").on("show.bs.collapse", function() {
        $(".navbar-toggle").html('<span class="sr-only">Toggle navigation</span><i class="navbar-dropdown-logo icon-close" aria-hidden="true">X</i>');

        $("#header-profile-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-profile.png" style="display: none;">');

        $("#header-search-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-search.png" style="display: none;">');

        $(".navbar-brand").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/lacto-brand-white.png" style="display: none;">');

        $("#affix").addClass('navbar-extend');
        $("#search-bar").addClass('navbar-search');
    });

    $(window).resize(function() {
        var win = $(this);
        if (win.height() >= 768) {
            $(".navbar-toggle").html('<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>');

            $("#header-profile-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-profile.png">');

            $("#header-search-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-search.png">');

            $(".navbar-brand").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/lacto-brand-white.png">');

            $("#affix").removeClass('navbar-extend');
            $("#search-bar").removeClass('navbar-search');
        }else{
            $(".navbar-toggle").html('<span class="sr-only">Toggle navigation</span><i class="navbar-dropdown-logo icon-close" aria-hidden="true">X</i>');

            $("#header-profile-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-profile.png" style="display: none;">');

            $("#header-search-button").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/banner-search.png" style="display: none;">');

            $(".navbar-brand").html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/lacto-brand-white.png" style="display: none;">');

            $("#affix").addClass('navbar-extend');
            $("#search-bar").addClass('navbar-search');
        }
    });

    //Autocomplete search
    var availableTags = [];

    $.get('https://rewards.sahabatnestle.co.id/apps/snrv2/api/web/article',
        function(data){
            $.each(data, function(key, value){
                availableTags[key] = { value: 'https://'+location.hostname+value.url_alias, label:value.title};
            })
        },
        'json'
    );
    $("#search-bar").autocomplete({
        source: availableTags,
        select: function( event, ui ) {
            window.location.href = ui.item.value;
        }
    });

    //Loading Button
    $('#loading').on('click', function() {
      var $this = $(this);
        $this.button('loading');
      setTimeout(function() {
         $this.button('reset');
     }, 15000);
    });

    if(getUrlParameter('profileError') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Lengkapi data anak terlebih dahulu!</h4>');
        $('#popupbox').modal();
        // alert('Maaf, kode unik salah atau sudah digunakan');
    }

    //popup Error when redeem
    if(getUrlParameter('redeemError') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, redeem gagal</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('redeemError') == '0'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Selamat, redeem berhasil</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('mailstatus') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Selamat, email telah dikirim</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('redeemError') == '10'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, terjadi kesalahan silahkan coba kembali dalam 24 jam</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '11'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, email ini sudah terdaftar sebelumnya</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '12'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Kode aktivasi anda salah</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '13'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, nomor telepon ini sudah terdaftar sebelumnya</h4>');
        $('#popupbox').modal();
    }else if(getUrlParameter('error') == '14'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Maaf, minimal point untuk klaim adalah 3150</h4>');
        $('#popupbox').modal();
    }

    if(getUrlParameter('otp') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Kode OTP Salah atau sudah kadaluarsa</h4>');
        $('#popupbox').modal();
    }

    else if(getUrlParameter('error') == '20'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Terjadi kesalahan, silahkan cek data yang anda input</h4>');
        $('#popupbox').modal();
    }

    if(getUrlParameter('banned') == '1'){
        $('#popupbox .modal-header').html('<img src="https://rewards.sahabatnestle.co.id/themes/dancowtheme/dist_layout/img/images/lacto-brand-white.png" width="180" height="auto" alt="">');
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Member ID Anda dalam status tidak aktif. Hubungi 08001821028 (tekan 2) atau email ke info.rewards@id.nestle.com</h4>');
        $('#popupbox').modal();
    }


    if(getUrlParameter('success') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Profile Berhasil di Update</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('login') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Link ganti password sudah dikirim ke email anda.</h4>');
        $('#popupbox').modal();
    }

    if(getUrlParameter('login') == '2'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Email atau Password anda salah</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('successRedeem')){
        $('#popupbox .modal-header').html('<img src="/themes/dancowtheme/dist_layout/img/images/lacto-brand-white.png" width="auto" height="70" style="margin-top:-23px;margin-bottom: -16px;" alt="">');
        $('#popupbox .modal-body').html('<h4 style="text-align: center;font-weight: 300;">Selamat, Anda telah berhasil menukar DANCOW IYA BOLEH Poin.</h4><br><h4 style="text-align: center;font-weight: 300;">Ini adalah kode undian Anda</h4><br><h2 style="text-align: center;font-weight:600;margin-top: 0px;"><img src="/themes/artheme/dist_front/img/IyaBolehPoinicon.png" style="width:60px;"> '+getUrlParameter('successRedeem')+'</h2><br><h4 style="text-align: center;font-weight: 300;">Tambah terus DANCOW IYA BOLEH Poin Anda untuk memperbesar kesempatan memenangkan hadiah undian.</h4><div style="text-align: center;"><span style="display: inline-block;margin-bottom:10px;"><br><a class="btn btn-warning" href="'+baseUrl+'/guest/iyaboleh-voucher">Lihat Iya Boleh Poin</a></span></div>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('suksesponta')){
        $('#popupbox .modal-header').html('<img src="/themes/dancowtheme/dist_layout/img/images/lacto-brand-white.png" width="auto" height="70" style="margin-top:-23px;margin-bottom: -16px;" alt="">');
        //$('#popupbox .modal-body').html('<h4>Tukarkan kode '+getUrlParameter('suksesponta')+' dg 1 DANCOW Advanced Excelnutri+ 1+ 200gr di ALFAMART. Valid sd 28 Feb 19. Syarat ketentuan berlaku</h4>');
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Terima kasih.<br>Silakan cek SMS di nomor HP terdaftar. Syarat ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('gagal') == '0'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Nomor HP yang Anda masukkan tidak sesuai dengan data PONTA. Cek kembali nomor HP Anda. Syarat ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('gagal') == '1'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Promo sudah berakhir. <br>Nantikan promo DANCOW Advanced Excelnutri+ berikutnya. Info lengkap cek <a href="https://www.parentingrewards.dancow.co.id" target="_blank">www.parentingrewards.dancow.co.id</a><br>Syarat Ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }
    if(getUrlParameter('gagal') == '2'){
        $('#popupbox .modal-body').html('<h4 style="text-align: center">Data Anda sudah digunakan.<br>Nomor HP dan data member PONTA hanya bisa dimasukkan 1 (satu) kali. Syarat ketentuan berlaku</h4>');
        $('#popupbox').modal();
    }

    if (getUrlParameter('verifikasi') == '1'){
      $('#popupbox .modal-body').html('<h4 style="text-align: center">Verifikasi nomor telepon anda untuk mengakses halaman</h4>');
      $('#popupbox').modal();
    } else if (getUrlParameter('verifikasi') == '2'){
      $('#popupbox .modal-body').html('<h4 style="text-align: center">Lengkapi profil anda untuk mengakses halaman</h4>');
      $('#popupbox').modal();
    }

    if (getUrlParameter('kontak') == '0') {
      $('#popupbox .modal-body').html('<h4 style="text-align: center">Terima kasih atas pesan yang Anda kirimkan. Mohon menunggu kami akan membalas pesan Anda pada hari dan jam kerja: Senin – Jumat, pukul 08.00 – 17.00 WIB.</h4>');
      $('#popupbox').modal();
    } else if (getUrlParameter('kontak') == '1'){
      $('#popupbox .modal-body').html('<h4 style="text-align: center">Email gagal terkirim, pesan minimal 10 karakter</h4>');
      $('#popupbox').modal();
    }

    var currentUrl = window.location.href;
    // alert(currentUrl);
    $('a').closest('li').not().closest('.nav-pills').removeClass('active');
    /*$('a').each(function() {
        if($(this).attr('href') == currentUrl){
            if($(this).closest('.dropdown').length > 0){
                $(this).closest('.dropdown').addClass('active');
            }else{
                $(this).closest('li').addClass('active');
            }
        }
    });*/

    $('.share-twitter').on('click', function(){
        popupCenter('https://twitter.com/share?url=' + $(this).data('shareurl'), 'Share Twitter', '500', '400');
    });

    $('.share-fb').click(function (e) {
        var refferal = $(this).attr('data-refferal');
        var name = $(this).attr('data-name');
        var description = $(this).attr('data-description');
        var image = $(this).attr('data-image');

        e.preventDefault();
        FB.ui({
            method: 'share_open_graph',
            action_type: 'og.shares',
            action_properties: JSON.stringify({
                object: {
                    'og:url': refferal,
                    'og:title': name,
                    'og:description': description,
                    'og:image': image
                }
            })
        });

        return false;
    });

    function popupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }

    if($('table tbody.history-mutasi').length > 0){
        var mutasi = [];
        var getPointBalance = $('#greet span.sisapointbalance').text(); //13500;
        var historypoint = parseInt(getPointBalance);

        $('table tbody.history-mutasi > tr').each(function(){
            mutasi.push(parseInt($(this).find('td:nth-child(4)').text()));
        });

        console.log(mutasi);
        for(var i=mutasi.length;i>=0;i--){
            indexArray = i - 1;
            indexMutasi = i;
            indexElement = i - 1;

            var getMutasiType = $('table tbody.history-mutasi > tr:nth-child(' + indexMutasi + ')').find('td:nth-child(2)').text();
            if(mutasi[indexArray]){
                if(getMutasiType == 'DB'){
                    historypoint = historypoint+mutasi[indexArray];
                }else if(getMutasiType == 'CR'){
                    historypoint = historypoint-mutasi[indexArray];
                }
            }
            $('table tbody.history-mutasi > tr:nth-child(' + indexElement + ')').find('td:nth-child(5)').text(historypoint);
        }

        $('table tbody.history-mutasi > tr:last-child').find('td:nth-child(5)').text(getPointBalance);
        $('.saldoawal').text(historypoint);
    }

    if($('.btn-warning.changebtn').length > 0){
        console.log($('table.table-striped tbody > tr').length);
        if($('table.table-striped tbody > tr').length <= 1){
            $('table.table-striped tbody > tr a.changebtn').remove();
        }
    }

    $('input.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });


    $('.sisapointbalance').text(addCommas($('.sisapointbalance').text()));
    $('.formatcurrency').each(function(){
        $(this).text(addCommas($(this).text()));
    });

    //modal for popup asi
    // $(window).on('load',function(){


    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function setGreeting(){
        var getDate = new Date();
        var thehours = getDate.getHours(); // + ':' + getDate.getMinutes();
        var themessage;
        var morning = 'Selamat Pagi';
        var afternoon = 'Selamat Siang';
        var evening = 'Selamat Sore';
        var night = 'Selamat Malam';

        if (thehours >= 0 && thehours <= 10) {
            themessage = morning;
        } else if (thehours >= 10 && thehours <= 15) {
            themessage = afternoon;
        } else if (thehours >= 15 && thehours <= 18) {
            themessage = evening;
        } else if (thehours >= 18 && thehours <= 23) {
            themessage = night;
        }

        $('#greet > strong > span').html(themessage);
    }

    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function getHashValue($url){
        var getHash = window.location.hash.substring(1);

        if(getHash){
            $('.tab-pane, .nav-tab > li').removeClass('active');
            $('#' + getHash + ', #tab-' + getHash).addClass('active');
        }
    }

    // // Create the lightbox
    // lightbox = new fusepump.lightbox.buynow(00214324);

    // var callToActionElement = document.getElementById(
    //     "lightbox-call-to-action" // The id of your call-to-action element
    // );

    // // Bind to the click event on the call-to-action element
    // callToActionElement.addEventListener("click", function() {
    //     lightbox.show(); // Show the lightbox
    // });

});