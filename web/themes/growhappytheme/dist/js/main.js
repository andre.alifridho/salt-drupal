/*####################################*/
/*###### Custom JS Functions #########*/
/*###### Author : Aris Haryanto ######*/
/*####################################*/
 
$(document).ready(function() {
    
    // $('.timepicker').timepicker();

    function createQuery(thisElement){
        var query = '';
        var mergeQuery = '';
        var condition = '';
        
        var field = thisElement.closest('.block-sub-filter').find('.field').val();
        var operation = thisElement.closest('.block-sub-filter').find('.operation').val();
        var value = thisElement.closest('.block-sub-filter').find('.value').val();
        var btnsub = thisElement.closest('.block-sub-filter').find('.btnsub').val();
        var condition = thisElement.closest('.block-sub-filter').find('.condition').val();
        
        query = ' '+condition;
        
        if(btnsub == "()"){
            query += ' ('+field+operation+'"'+value+'"'+')';
        }else if(btnsub == "("){
            query += ' ('+field+operation+'"'+value+'"';
        }else if(btnsub == ")"){
            query += ' '+field+operation+'"'+value+'"'+')';
        }else{
            query += ' '+field+operation+'"'+value+'"';
        }
//        alert(query);
        thisElement.closest('.block-sub-filter').find('.savequery').text(query);
        $('.savequery').each(function(){
            mergeQuery += $(this).val(); 
        });
        $('#textquery').text(mergeQuery);
    }
    function addField(getCount){
        $('.block-filtering').append('<div class="block-sub-filter">\
                <div class="form-group" style="max-width: 80px;">\
                    <br>\
                    <select name="query[query_'+getCount+'][condition]" class="condition'+getCount+' condition form-control">\
                        <option>AND</option>\
                        <option>OR</option>\
                    </select>\
                </div>\
                <div class="form-inline">\
                    <div class="form-group">\
                        <select name="query[query_'+getCount+'][btnsub]" class="btnsub'+getCount+' btnsub form-control">\
                            <option></option>\
                            <option>()</option>\
                            <option>(</option>\
                            <option>)</option>\
                        </select>\
                    </div>\
                    <div class="form-group">\
                        <select name="query[query_'+getCount+'][field]" class="field'+getCount+' field form-control">\
                        </select>\
                    </div>\
                    <div class="form-group">\
                        <select name="query[query_'+getCount+'][operation]" class="operation'+getCount+' operation form-control">\
                            <option>=</option>\
                            <option>>=</option>\
                            <option><=</option>\
                            <option>></option>\
                            <option><</option>\
                            <option>!=</option>\
                        </select>\
                    </div>\
                    <div class="form-group">\
                        <input name="query[query_'+getCount+'][value]" type="text" class="value'+getCount+' value form-control">\
                    </div>\
                    <div class="form-group">\
                        <a class="btnremove btn btn-danger">Remove</a>\
                    </div>\
                    <textarea name="query[query_'+getCount+'][savequery]" class="savequery'+getCount+' savequery" style="display: none"></textarea>\
                </div>\
            </div>\
               ');

        $('.field'+getCount).html($('.getfield').html());

        $('.btnremove').on('click', function(){
            $(this).closest('.block-sub-filter').remove();
            createQuery($(this));
        });
        
        $('.condition').on('change', function(){
            createQuery($(this));
        });
        $('.operation').on('change', function(){
            createQuery($(this));
        });
        $('.value').on('keyup', function(){
            createQuery($(this));
        });
        $('.field').on('change', function(){
            createQuery($(this));
        });
        $('.btnsub').on('change', function(){
            createQuery($(this));
        });
    }
    
	$('.addfield').on('click', function(){
        var getCount = parseInt($(this).attr('data-count')); 
        getCount = getCount+1;
        addField(getCount);
        $(this).attr('data-count', getCount);
    });
    
    $('.condition').on('change', function(){
        createQuery($(this));
    });
    $('.operation').on('change', function(){
        createQuery($(this));
    });
    $('.value').on('keyup', function(){
        createQuery($(this));
    });
    $('.field').on('change', function(){
        createQuery($(this));
    });
    $('.btnsub').on('change', function(){
        createQuery($(this));
    });

    //NOTES STATUS
    notes($('#statuscall'));
    $('#statuscall').change(function(){
        notes($(this));
    });
    function notes(thisElement){
        if(thisElement.val() == 3 || 
            thisElement.val() == 4 || 
            thisElement.val() == 5){
            $('#callnote').show();
        }else{
            $('#callnote').hide();
        }
    }

});