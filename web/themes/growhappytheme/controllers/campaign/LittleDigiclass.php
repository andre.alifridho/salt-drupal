<?php

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

function growhappytheme_preprocess_page__guest__campaign__little_digiclass(&$variables)
{

  try {
  session_start();
  $session = new Session();

if ($session->get('guestlogin')) {
  $member = (object)$session->get('guestlogin');
  $variables['guestlogin'] = $member;

  if (isset($_GET['msg'])) {
    $msg = $_GET['msg'];
  } else {
    $msg = '';
  }

  if (isset($_GET['status'])) {
    $status = $_GET['status'];
  } else {
    $status = '';
  }

  if (isset($_GET['success'])) {
    $success = $_GET['success'];
  } else {
    $success = '';
  }

  $variables['msg'] = $msg;
  $variables['status'] = $status;
  $variables['success'] = $success;

} else {
  session_start();
  $_SESSION['set'] = 'digiclass';
  $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . '/login';
  $response = new RedirectResponse($redirectto);
  $response->send();
  return;
}

  } catch (Exception $e) {
      $path = 'not-found';
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
  }
}
