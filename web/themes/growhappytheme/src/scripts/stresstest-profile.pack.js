var changeProfile = (function(){
    function profileSelect(){
        var $profileActive = $('#profile-active');
        var $profile = $('.profile-inactive'),
          urlAjax = $profileActive.data('action');

        $profile.on('click', function(e){
            var value = e.currentTarget.innerHTML;
            console.log(value);

            $profileActive.children('span').text('Loading..');

            // DIEGO TODO:
            $.ajax({
              type: "POST",
              url: urlAjax,
              data: {
                nama_anak: value
              },
              complete: function (res) {
                var response = res.responseJSON,
                  data = response.data,
                  success = response.success;

                if (success) {
                  $profileActive.children('span').text(value);
                }
              }
            });
            // REFRESH PAGE DAN GANTI PROFILE SAMA KAYAK LOGIC NUTRITODS PROFILE
        })
    }

    function init(){
        profileSelect()
    }



    return {
        init: init
    };

})();

changeProfile.init()
