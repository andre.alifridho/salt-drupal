function lactoCollapseCaretChanger() {
    jQuery(document).ready(function() {

        //  Collapse 1 caret
        $(".lactoCollapse-1").on("hide.bs.collapse", function() {
            $(".lactoCollapse-1-caret").removeClass("fa-chevron-up");
            $(".lactoCollapse-1-caret").addClass("fa-chevron-down");

            $(".lactoCollapse-1-title").removeClass("lacto-collapse-opened");
            $(".lactoCollapse-1-caret-container").removeClass("lacto-collapse-opened");
        });
        $(".lactoCollapse-1").on("show.bs.collapse", function() {
            $(".lactoCollapse-1-caret").removeClass("fa-chevron-down");
            $(".lactoCollapse-1-caret").addClass("fa-chevron-up");

            $(".lactoCollapse-1-title").addClass("lacto-collapse-opened");
            $(".lactoCollapse-1-caret-container").addClass("lacto-collapse-opened");

        });

        //  Collapse 2 caret
        $(".lactoCollapse-2").on("hide.bs.collapse", function() {
            $(".lactoCollapse-2-caret").removeClass("fa-chevron-up");
            $(".lactoCollapse-2-caret").addClass("fa-chevron-down");

            $(".lactoCollapse-2-title").removeClass("lacto-collapse-opened");
            $(".lactoCollapse-2-caret-container").removeClass("lacto-collapse-opened");
        });
        $(".lactoCollapse-2").on("show.bs.collapse", function() {
            $(".lactoCollapse-2-caret").removeClass("fa-chevron-down");
            $(".lactoCollapse-2-caret").addClass("fa-chevron-up");

            $(".lactoCollapse-2-title").addClass("lacto-collapse-opened");
            $(".lactoCollapse-2-caret-container").addClass("lacto-collapse-opened");
        });

        //  Collapse 3 caret
        $(".lactoCollapse-3").on("hide.bs.collapse", function() {
            $(".lactoCollapse-3-caret").removeClass("fa-chevron-up");
            $(".lactoCollapse-3-caret").addClass("fa-chevron-down");

            $(".lactoCollapse-3-title").removeClass("lacto-collapse-opened");
            $(".lactoCollapse-3-caret-container").removeClass("lacto-collapse-opened");
        });
        $(".lactoCollapse-3").on("show.bs.collapse", function() {
            $(".lactoCollapse-3-caret").removeClass("fa-chevron-down");
            $(".lactoCollapse-3-caret").addClass("fa-chevron-up");

            $(".lactoCollapse-3-title").addClass("lacto-collapse-opened");
            $(".lactoCollapse-3-caret-container").addClass("lacto-collapse-opened");
        });

        //  Collapse 4 caret
        $(".lactoCollapse-4").on("hide.bs.collapse", function() {
            $(".lactoCollapse-4-caret").removeClass("fa-chevron-up");
            $(".lactoCollapse-4-caret").addClass("fa-chevron-down");

            $(".lactoCollapse-4-title").removeClass("lacto-collapse-opened");
            $(".lactoCollapse-4-caret-container").removeClass("lacto-collapse-opened");
        });
        $(".lactoCollapse-4").on("show.bs.collapse", function() {
            $(".lactoCollapse-4-caret").removeClass("fa-chevron-down");
            $(".lactoCollapse-4-caret").addClass("fa-chevron-up");

            $(".lactoCollapse-4-title").addClass("lacto-collapse-opened");
            $(".lactoCollapse-4-caret-container").addClass("lacto-collapse-opened");
        });

        //  Collapse 5 caret
        $(".lactoCollapse-5").on("hide.bs.collapse", function() {
            $(".lactoCollapse-5-caret").removeClass("fa-chevron-up");
            $(".lactoCollapse-5-caret").addClass("fa-chevron-down");

            $(".lactoCollapse-5-title").removeClass("lacto-collapse-opened");
            $(".lactoCollapse-5-caret-container").removeClass("lacto-collapse-opened");
        });
        $(".lactoCollapse-5").on("show.bs.collapse", function() {
            $(".lactoCollapse-5-caret").removeClass("fa-chevron-down");
            $(".lactoCollapse-5-caret").addClass("fa-chevron-up");

            $(".lactoCollapse-5-title").addClass("lacto-collapse-opened");
            $(".lactoCollapse-5-caret-container").addClass("lacto-collapse-opened");
        });

        //  Collapse 6 caret
        $(".lactoCollapse-6").on("hide.bs.collapse", function() {
            $(".lactoCollapse-6-caret").removeClass("fa-chevron-up");
            $(".lactoCollapse-6-caret").addClass("fa-chevron-down");

            $(".lactoCollapse-6-title").removeClass("lacto-collapse-opened");
            $(".lactoCollapse-6-caret-container").removeClass("lacto-collapse-opened");
        });
        $(".lactoCollapse-6").on("show.bs.collapse", function() {
            $(".lactoCollapse-6-caret").removeClass("fa-chevron-down");
            $(".lactoCollapse-6-caret").addClass("fa-chevron-up");

            $(".lactoCollapse-6-title").addClass("lacto-collapse-opened");
            $(".lactoCollapse-6-caret-container").addClass("lacto-collapse-opened");
        });

    });
}
window.onload = lactoCollapseCaretChanger();
//
// jQuery(document).ready(function() {
//     (function($) {
//         $('.owl-carousel').owlCarousel();
//     })(jQuery);
// });
//
// if (self == top) {
//     var theBody = document.getElementsByTagName('body')[0];
//     theBody.style.display = "block";
// } else {
//     top.location = self.location;
// }
