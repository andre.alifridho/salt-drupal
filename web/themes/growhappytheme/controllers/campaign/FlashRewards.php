<?php

 use GuzzleHttp\Client;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Session\Session;
 use Symfony\Component\HttpFoundation\Response;
 use Symfony\Component\HttpFoundation\RedirectResponse;

function growhappytheme_preprocess_page__campaign__flashrewards(&$variables)
{
  try {

  $client = new \GuzzleHttp\Client([
        'base_uri' => __API_BASE__,
        'verify' => false,
    ]);

  $response_provinsi = $client->get("api/web/area/provinsi?key=".__API_KEY__);
  $get_response_provinsi = $response_provinsi->getBody();
  $provinsi = json_decode($get_response_provinsi, true);

  if (isset($_GET['msg'])) {
    $msg = $_GET['msg'];
  } else {
    $msg = '';
  }

  if (isset($_GET['status'])) {
    $status = $_GET['status'];
  } else {
    $status = '';
  }

  $variables['provinsi'] = $provinsi;
  $variables['msg'] = $msg;
  $variables['status'] = $status;



  } catch (Exception $e) {
      $path = 'not-found';
      $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
        $response = new RedirectResponse($redirectto);
        $response->send();
  }
}
