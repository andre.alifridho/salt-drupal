if (inputdata) {
  $("#modalSample").modal("show");
}

$("#provinsi").on("change", function() {
  var id = $(this).val();
  console.log(id);
  $("#kota").html('<option value="">Mencari Kota...</option>');
  $.get(
    base_url + "lacto/kota/get",
    {
      id: id
    },
    function(data) {
      var html = '<option value="">-- Pilih Kota --</option>';
      for (let i = 0; i < data.data.length; i++) {
        html +=
          '<option value="' +
          data.data[i].id +
          '">' +
          data.data[i].nama +
          "</option>";
      }
      $("#kota").html(html);
    }
  );
});
