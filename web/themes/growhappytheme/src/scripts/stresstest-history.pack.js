var changeMonth = (function(){
    function monthSelect(){
      var $monthActive = $('#month-active'),
          urlRedirect = $monthActive.data('redirect-to');
        var $month = $('.month-inactive');

        $month.on('click', function(e){
            // var value = e.currentTarget.innerHTML;
            var value = $(this).data('value');
            // console.log(value);

            $monthActive.children('span').text('Loading..');

            // DIEGO TODO:
            // REFRESH PAGE DAN GANTI BULAN
            setTimeout(function () {
              window.location.replace(urlRedirect+'?bulan='+value);
              // window.location.reload();
            }, 500);
        })
    }

    function init(){
        monthSelect()
    }



    return {
        init: init
    };

})();

var setScore = (function(){
    function init(){
        var $results = $('.data-result-row');

        $results.each(function(e){
            var $this = $(this),
                $data_age = $this.data('age'),
                $score_bar = $this.find('.score-bar'),
                $progress_icon = $this.find('.progress-icon');

            ($data_age === '1-3') ? $score_bar.attr('aria-valuemax', '24') : $score_bar.attr('aria-valuemax', '28');

            var $val_now = $score_bar.attr('aria-valuenow'),
                $val_max = $score_bar.attr('aria-valuemax');

            switch ($data_age) {
                case '1-3':
                    if($val_now <= 10){
                        $this.attr('data-normal', 'true')
                        $progress_icon.find('svg use').attr('xlink:href', '#icon-smile');
                    } else {
                        $this.attr('data-normal', 'false')
                        $progress_icon.find('svg use').attr('xlink:href', '#icon-sad');
                    }

                    break;

                default:
                    if($val_now <= 11){
                        $this.attr('data-normal', 'true')
                        $progress_icon.find('svg use').attr('xlink:href', '#icon-smile');
                    } else {
                        $this.attr('data-normal', 'false')
                        $progress_icon.find('svg use').attr('xlink:href', '#icon-sad');
                    }

                    break;
            }

            setTimeout(function(){
                $score_bar.css({
                    'width': (($val_now / $val_max) * 100) + '%'
                })

                $progress_icon.css({
                    'left': (($val_now / $val_max) * 100) + '%'
                })
            }, 600)


        })
    }



    return {
        init: init
    };

})();

changeMonth.init()
setScore.init()
