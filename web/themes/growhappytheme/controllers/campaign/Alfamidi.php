<?php

 use GuzzleHttp\Client;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Session\Session;
 use Symfony\Component\HttpFoundation\Response;
 use Symfony\Component\HttpFoundation\RedirectResponse;

function growhappytheme_preprocess_page__campaign__alfamidi(&$variables)
{
  session_start();
  $session = new Session();

    try {

      $client = new \GuzzleHttp\Client([
            'base_uri' => __API_BASE__,
            'verify' => false,
        ]);

      if ($session->get('guestlogin')) {

        $member = (object)$session->get('guestlogin');
        $variables['guestlogin'] = $member;

        $headers = [
          'headers' => [
            "X-Member-ID" => $member->id
          ]
        ];

        $historyUrl = $client->get("api/web/campaign/alfamidi/history?key=".__API_KEY__, $headers);
    	  $getHistory = $historyUrl->getBody();
    	  $history = json_decode($getHistory, true);
        $variables['history'] = $history;

        $treeUrl = $client->get("api/web/campaign/alfamidi/tree?key=".__API_KEY__, $headers);
    	  $getTree= $treeUrl->getBody();
    	  $tree = json_decode($getTree, true);
        $variables['tree'] = $tree;

        $notifUrl = $client->get("api/web/campaign/alfamidi/dynamic?key=".__API_KEY__, $headers);
    	  $getNotif= $notifUrl->getBody();
    	  $notif = json_decode($getNotif, true);
        $variables['notif'] = $notif;

      }

      $tokenUrl = $client->get("api/web/rewards?key=".__API_KEY__);
  	  $getReward = $tokenUrl->getBody();
  	  $rewards = json_decode($getReward, true);

  		$filterBy1 = 159;
  		$filterBy2 = 161;
      $filterBy3 = 160;
      $filterBy4 = 162;
      $filterBy5 = 154;

  		$rewardFiltered = array_filter($rewards, function ($var) use ( $filterBy1, $filterBy2, $filterBy3, $filterBy4, $filterBy5 ) {
  			 return ($var['id'] == $filterBy1 || $var['id'] == $filterBy2 || $var['id'] == $filterBy3 || $var['id'] == $filterBy4 || $var['id'] == $filterBy5 );
  		 });


  	  $image = \Drupal::request()->getSchemeAndHttpHost() . base_path() . "images/rewards/";

  	  $variables['rewards'] = $rewardFiltered;
  	  $variables['rewardsimage'] = $image;



    } catch (Exception $e) {
        $path = 'not-found';
        $redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
          $response = new RedirectResponse($redirectto);
          $response->send();
    }

}
