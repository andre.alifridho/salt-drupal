jQuery(document).ready(function($){

});

function loginPopup(id){
	$('#hampers_modal_login').modal();	
}

function popupNotAllowed(){
	var msg = 'Happy Parents tidak memenuhi persyaratan untuk mengambil hadiah ini.';
	$('#hampers_popup_message').html(msg);
	$('#hampers_modal_message').modal();	
}

function popupHabis(){
	var msg = 'Happy Parents sudah pernah mengambil hadiah ini.';
	$('#hampers_popup_message').html(msg);
	$('#hampers_modal_message').modal();	
}


/*
0 = tidak berhak
1 = sukses
2 = unknown request
3 = belum login
4 = belum memenuhi syarat claim/kurang submit
5 = sudah pernah claim
6 = data tidak lengkap
7 = error
*/
function claimHampers1(memberid){
	var baseUrl = window.location.protocol + "//" + window.location.host;
	var url = baseUrl + '/happy_hampers/claim/1';
	var submit_url = baseUrl + '/guest/submit-kode';
	$.ajax({
	  method: "POST",
	  url: url,
	  dataType: 'json',
	  data: {}
	})
	.done(function( msg ) {
		if(msg.status == 1){
		    $('#hampers_popup_reward_name').html(msg.rewardname);
	    	$('#hampers_success_image').attr('src', msg.rewardimg);
			$('#hampers_modal_success').modal();
		}else if(msg.status == 0){		    
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 2){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 3){
		    $('#hampers_modal_login').modal();
		}else if(msg.status == 4){
			$('#button_action').html('<a href="'+submit_url+'" target="_blank"><button class="hampers_btn_4" style="width:90%;">Submit</button></a>');
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 5){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 6){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else{
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();	
		}
	});
}

function claimHampers2(memberid){
	var baseUrl = window.location.protocol + "//" + window.location.host;
	var url = baseUrl + '/happy_hampers/claim/2';
	var submit_url = baseUrl + '/guest/submit-kode';
	$.ajax({
	  method: "POST",
	  url: url,
	  dataType: 'json',
	  data: {}
	})
	.done(function( msg ) {
		if(msg.status == 1){
		    $('#hampers_popup_reward_name').html(msg.rewardname);
	    	$('#hampers_success_image').attr('src', msg.rewardimg);
			$('#hampers_modal_success').modal();
		}else if(msg.status == 0){		    
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 2){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 3){
		    $('#hampers_modal_login').modal();
		}else if(msg.status == 4){
		    $('#button_action').html('<a href="'+submit_url+'" target="_blank"><button class="hampers_btn_4" style="width:90%;">Submit</button></a>');
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 5){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 6){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else{
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();	
		}
	});
}

function claimHampers3(memberid){
	var baseUrl = window.location.protocol + "//" + window.location.host;
	var url = baseUrl + '/happy_hampers/claim/3';
	var submit_url = baseUrl + '/guest/submit-kode';
	var address = $('#inputAddress').val();
	$.ajax({
	  method: "POST",
	  url: url,
	  dataType: 'json',
	  data: { inputAddress: address }
	})
	.done(function( msg ) {
		if(msg.status == 1){
		    $('#hampers_popup_reward_name').html(msg.rewardname);
	    	$('#hampers_success_image').attr('src', msg.rewardimg);
			$('#hampers_modal_success').modal();
		}else if(msg.status == 0){		    
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 2){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 3){
		    $('#hampers_modal_login').modal();
		}else if(msg.status == 4){
		    $('#button_action').html('<a href="'+submit_url+'" target="_blank"><button class="hampers_btn_4" style="width:90%;">Submit</button></a>');
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 5){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 6){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else{
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();	
		}
	});
}

function claimHampers4(memberid){
	var baseUrl = window.location.protocol + "//" + window.location.host;
	var url = baseUrl + '/happy_hampers/claim/4';
	var submit_url = baseUrl + '/guest/submit-kode';
	var address = $('#inputAddress').val();
	$.ajax({
	  method: "POST",
	  url: url,
	  dataType: 'json',
	  data: { inputAddress: address }
	})
	.done(function( msg ) {
		if(msg.status == 1){
		    $('#hampers_popup_reward_name').html(msg.rewardname);
	    	$('#hampers_success_image').attr('src', msg.rewardimg);
			$('#hampers_modal_success').modal();
		}else if(msg.status == 0){		    
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 2){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 3){
		    $('#hampers_modal_login').modal();
		}else if(msg.status == 4){
		    $('#button_action').html('<a href="'+submit_url+'" target="_blank"><button class="hampers_btn_4" style="width:90%;">Submit</button></a>');
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 5){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else if(msg.status == 6){
		    $('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();
		}else{
			$('#hampers_popup_message').html(msg.message);
			$('#hampers_modal_message').modal();	
		}
	});
}