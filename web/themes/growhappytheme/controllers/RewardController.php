<?php
	use GuzzleHttp\Client;
	use Symfony\Component\HttpFoundation\Request;
	use Drupal\Core\Routing\TrustedRedirectResponse;
	use Symfony\Component\HttpFoundation\RedirectResponse;
	use Symfony\Component\HttpFoundation\Session\Session;


	use Drupal\Core\Database\Database;

	function growhappytheme_preprocess_page__reward_detail(&$variables)
	{
		session_start();
		$session = new Session();
		if ($session->get('guestlogin')) {
			$member = (object)$session->get('guestlogin');
			$variables['guestlogin'] = $member;
		}
		try {
			if($_GET['s']){
				$slug = $_GET['s'];

				$client = new \GuzzleHttp\Client([
		           'base_uri' => __API_BASE__,
		           'verify' => false,
		           'proxy' => __API_PROXY__,
		       	]);

		       	$response_rewards = $client->get("api/web/rewards?key=".__API_KEY__);
		       	$get_response_rewards = $response_rewards->getBody();
		       	$rewards = json_decode($get_response_rewards, true);

		       	$response_reward = $client->get("api/web/rewards/".$slug."?key=".__API_KEY__);
		       	$get_response_reward = $response_reward->getBody();
		       	$reward = json_decode($get_response_reward);

			  	$image_path = \Drupal::request()->getSchemeAndHttpHost() . base_path()."images/rewards/";

			  	$variables['reward'] = $reward;
			  	$variables['rewardslisting'] = $rewards;
			  	$variables['rewardsimage'] = $image_path;
			  	$variables['api_url'] = __API_BASE__;
			}else{
				$path = 'not-found';
				$redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
	            $response = new RedirectResponse($redirectto);
	            $response->send();
			}
		} catch (Exception $e) {
			$path = 'not-found';
			$redirectto = \Drupal::request()->getSchemeAndHttpHost() . base_path() . $path;
            $response = new RedirectResponse($redirectto);
            $response->send();
		}
	}
	function growhappytheme_preprocess_page__getrewards(&$variables)
	{
		session_start();
		$session = new Session();
		if ($session->get('guestlogin')) {
			$member = (object)$session->get('guestlogin');
			$variables['guestlogin'] = $member;
		}
		$variables['test_rewards'] = 'test rewards';

		$pointStart = $_POST['pointStart'];
		$pointEnd = $_POST['pointEnd'];
		$sortBy = $_POST['sortBy'];
		$category = $_POST['category'];
		$keyword = $_POST['keyword'];

		$variables['pointStart'] = $pointStart;
		$variables['pointEnd'] = $pointEnd;
		$variables['sortBy'] = $sortBy;
		$variables['categoryName'] = $category;
		$variables['keyword'] = $keyword;

		if ($pointStart == '' && $pointEnd == '' && $sortBy == '' && $category == '' && $keyword == '') {
			$client = new \GuzzleHttp\Client([
			'base_uri' => __API_BASE__,
			'verify' => false,
			'proxy' => __API_PROXY__,
			]);
		} else {
			$temp = '';
			if ($pointStart != '') {
			$temp .= "pointStart=" . $pointStart;
			}
			if ($pointEnd != '') {
			$temp .= "pointEnd=" . $pointEnd;
			}
			if ($sortBy != '') {
			$temp .= "&sortBy=" . $sortBy;
			}
			if ($category != '') {
			$temp .= "&category=" . $category;
			}
			if ($keyword != '') {
			$temp .= "&keyword=" . $keyword;
			}
			$client = new \GuzzleHttp\Client([
			'base_uri' => __API_BASE__,
			'verify' => false,
			'proxy' => __API_PROXY__,
			]);
		}
		$tokenUrl = $client->get("api/web/rewards?key=".__API_KEY__."&" . $temp);
		$getReward = $tokenUrl->getBody();
		$rewards = json_decode($getReward, true);

		$image = \Drupal::request()->getSchemeAndHttpHost() . base_path() . "images/rewards/";

		$variables['rewards'] = $rewards;
		$variables['rewardsimage'] = $image;
	}
	function growhappytheme_preprocess_page__club(&$variables)
	{
	  session_start();
	  $session = new Session();
	  if ($session->get('guestlogin')) {
	    $member = (object)$session->get('guestlogin');
	    $variables['guestlogin'] = $member;
	  }

	  $query = \Drupal::entityQuery('node');
	$query->condition('status', 1);
	$query->condition('type','banner_club_page');
	// $query->sort('field_order','ASC');

	$ids = $query->execute();
	$list_banner = array();

	if(count($ids) > 0) {
		$banner = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($ids);

		foreach ($banner as $art) {
			$title = $art->getTitle();

			$desktop_image =  file_create_url($art->get('field_desktop_image')->entity->getFileUri());
			$desktop_image_alt = $art->get('field_desktop_image')->getValue();
			$mobile_image = $art->get('field_mobile_image')->entity ? file_create_url($art->get('field_mobile_image')->entity->getFileUri()) : '';
			$mobile_image_alt = $art->get('field_mobile_image')->getValue();
			$url = $art->get('field_url')->getValue();
			// $order= $art->get('field_order')->getValue();

			$list_banner[] = [
				'title' => $title,
				'desktop_image' => $desktop_image,
				'desktop_image_alt' => $desktop_image_alt[0]['alt'],
				'mobile_image' => $mobile_image,
				'mobile_image_alt' => $mobile_image_alt[0]['alt'],
				'mobile_image_webp' => $mobile_image_webp,
				'url' => $url[0]['value'],
				'order' => $order[0]['value'] - 1,
			];
			// dd($list_banner);
		}
	}

  $variables['list_banner'] = $list_banner;
//   dd($list_banner);

	  $pointEnd = $_GET['pointEnd'];
	  $sortBy = $_GET['sortBy'];
	  $category = $_GET['category'];
	  $keyword = $_GET['keyword'];

	  $variables['pointEnd'] = $pointEnd;
	  $variables['sortBy'] = $sortBy;
	  $variables['categoryName'] = $category;
	  $variables['keyword'] = $keyword;

	  if ($pointEnd == '' && $sortBy == '' && $category == '' && $keyword == '') {
	    $client = new \GuzzleHttp\Client([
	      'base_uri' => __API_BASE__,
	      'verify' => false,
	      'proxy' => __API_PROXY__,
	    ]);
	  } else {
	    $temp = '';
	    if ($pointEnd != '') {
	      $temp .= "&pointEnd=" . $pointEnd;
	    }
	    if ($sortBy != '') {
	      $temp .= "&sortBy=" . $sortBy;
	    }
	    if ($category != '') {
	      $temp .= "&category=" . $category;
	    }
	    if ($keyword != '') {
	      $temp .= "&keyword=" . $keyword;
	    }
	    $client = new \GuzzleHttp\Client([
	      'base_uri' => __API_BASE__,
	      'verify' => false,
	      'proxy' => __API_PROXY__,
	    ]);
	  }

	  $tokenUrl = $client->get("api/web/rewards?key=".__API_KEY__. $temp);
	  $getReward = $tokenUrl->getBody();
	  $rewards = json_decode($getReward, true);

		$filterBy1 = 159;
		$filterBy2 = 161;
		$filterBy3 = 160;
		$filterBy4 = 162;
		$filterBy5 = 154;

		$rewardFiltered = array_filter($rewards, function ($var) use ( $filterBy1, $filterBy2, $filterBy3, $filterBy4, $filterBy5 ) {
			 return ($var['id'] !== $filterBy1 && $var['id'] !== $filterBy2 && $var['id'] !== $filterBy3 && $var['id'] !== $filterBy4 && $var['id'] !== $filterBy5 );
		 });

	  $image = \Drupal::request()->getSchemeAndHttpHost() . base_path() . "images/rewards/";

	  $variables['rewards'] = $rewardFiltered;
	  $variables['rewardsimage'] = $image;

	  $product_3 = array();
	  $query = \Drupal::entityQuery('node');
	  $query->condition('status', 1);
	  $query->condition('title', '%' . Database::getConnection()->escapeLike('LACTOGROW 3') . '%', 'LIKE');
	  $entity_ids = $query->execute();
	  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);

	  foreach ($products as $prod) {
	    $fusepump_id = $prod->get('field_dsu_sku')->getValue();
	    $image = $prod->get('field_image')->getValue();
	    $image_file = [];
	    foreach ($image as $i) {
	      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
	    }

	    $product_3[] = [
	      'image' => $image_file[1],
	      'fusepump_id' => $fusepump_id[0]['value'],
	      'url' => \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $prod->id())
	    ];
	  }

	  $variables['produk_3'] = $product_3;

	  $product_4 = array();
	  $query = \Drupal::entityQuery('node');
	  $query->condition('status', 1);
	  $query->condition('title', '%' . Database::getConnection()->escapeLike('LACTOGROW 4') . '%', 'LIKE');
	  $entity_ids = $query->execute();
	  $products = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($entity_ids);

	  foreach ($products as $prod) {
	    $fusepump_id = $prod->get('field_dsu_sku')->getValue();
	    $image = $prod->get('field_image')->getValue();
	    $image_file = [];
	    foreach ($image as $i) {
	      $image_file[] = ['image' => file_create_url(\Drupal\file\Entity\File::load($i['target_id'])->getFileUri()), 'alt' => $i['alt']];
	    }

	    $product_4[] = [
	      'image' => $image_file[1],
	      'fusepump_id' => $fusepump_id[0]['value'],
	      'url' => \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $prod->id())
	    ];
	  }

	  $variables['produk_4'] = $product_4;

	  $client = new \GuzzleHttp\Client([
	    'base_uri' => __API_BASE__,
	    'verify' => false,
	    'proxy' => __API_PROXY__,
	  ]);

	  //reward list
	  $response_rewards = $client->get("api/web/rewards?key=".__API_KEY__);
	  $get_response_rewards = $response_rewards->getBody();
	  $rewards = json_decode($get_response_rewards, true);

	  //reward list spesial
	  $response_rewards_spesial = $client->get("api/web/rewards?key=".__API_KEY__."&category=Spesial");
	  $get_response_rewards_spesial = $response_rewards_spesial->getBody();
	  $rewards_spesial = json_decode($get_response_rewards_spesial, true);

	  //reward categories
	  $response_categories = $client->get("api/web/rewards/categories?key=".__API_KEY__);
	  $get_response_categories = $response_categories->getBody();
	  $categories = json_decode($get_response_categories, true);

	  $image = \Drupal::request()->getSchemeAndHttpHost() . base_path() . "images/rewards/";

	  $variables['rewardslisting'] = $rewards;
	  $variables['rewardslistingspesial'] = $rewards_spesial;
	  $variables['rewardsimage'] = $image;
	  $variables['rewardscategories'] = $categories;
	}
