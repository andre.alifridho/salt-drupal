var createProfile = (function(){
    function ageSelect(){
        var $radio = $('input[type="radio"]');

        $radio.on('change', function(){
            var $this = $(this);

            if($this.is(':checked')){
                $radio.not($this).parent().addClass('is-not-checked')
                $this.parent().removeClass('is-not-checked')
            }
        })
    }

    function init(){
        ageSelect()
    }



    return {
        init: init
    };

})();

createProfile.init()